<?php
date_default_timezone_set("Africa/Lagos");

include_once("../../config/dbconnections.php");
// //include("../Connections/courier4_pdo.php");
include_once("../api_functions.php");
include_once("../../config/constants.php");


        $data = json_decode(file_get_contents('php://input'), true);
        $request_id=$data["request_id"];  
  
//print_r($data);
      $now=date("Y-m-d H:i:s");
      
      if(is_null($request_id)||empty($request_id)){
           $date = new DateTime();
           $time = $date->format('U');
           $request_id = $time.rand(1000,9999); 
      }
     $description = "Product ".$request_id." payment";
     
      // This is the PayStack apk key (current is demo key)
      //$payment_api_key = "pk_test_1b7843591ba341ec2bafba83ff3b2e42bd92cab5";
     //$payment_api_key = "pk_test_17f4ad565522a38470dd3a50d4aa5bfd37986e9e";
     
    //$payment_api_key = "pk_test_7f29f2a33d16e9badc1a51678b234b91ec387172";
   $payment_api_key = "pk_live_ec0029097cd244cbb35f18fb79840fc4db072922";
      
      
      
      $trans_mode = "live";
     
     
          
            $current_date = date("Y-m-d H:i:s"); 
            $client_name = $data['client_name'];
            $mobile_number = $data['mobile_number'];
            $client_email = $data['client_email'];
            $package_type = $data['packageType'];
            $amount = $data['amount'];
             
            
            $currency = "NGN";

                  

         
            global $conn;
              try {
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $query_insert = $conn->prepare("INSERT INTO `payment_requests` (`Payment_Api_Key`,`Trans_Currency`,`Service_Request_Channel`,`payment_service_requestID`, `Service_Description`, `Service_Amount`,`Actual_ServiceAmount`,`Service_CustomerName`,`Service_CustomerTel`,`Service_CustomerEmail`,`Payment_Type`, `Payment_NetworkType`, `Payment_MobilePhoneNo`, `Payment_Amount`,`Payment_ServiceCharge`, `Payment_TotalAmount`,`DateTime_PaymentRequested`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
                    $query_insert->execute(array($payment_api_key,$currency,"web",$request_id,$description,$amount,$amount,$client_name,$mobile_number,$client_email,$trans_mode,$trans_mode,$mobile_number,$amount,"0",$amount,$current_date));
                    $insert_id=$conn->lastInsertId();
                 } catch (PDOException $e) {
                   echo $e->getMessage();
                 }
        
                try {
                     $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                     $query_rsodest = $conn->prepare("select Service_RequestID,Service_RequestID,Service_Amount from payment_requests where Payment_RequestID=? LIMIT 1");
                     $query_rsodest->execute(array($insert_id));
                     $totalRows_rsodest = $query_rsodest->rowCount();
                     $row_rsodest_all = $query_rsodest->fetch(PDO::FETCH_ASSOC);
                     $Service_RequestID=$row_rsodest_all['Service_RequestID'];
                     $InvoiceID=$row_rsodest_all['Service_RequestID'];
                     $amount = $row_rsodest_all['Service_Amount'];
                  }catch (PDOException $e) {
                   echo $e->getMessage();
                  }

                     
                    if(!is_null($InvoiceID)||!empty($InvoiceID)){

                      $payment_url = BASE_URL."backend/payment/checkout_page.php?refid=".$InvoiceID; 
                      //header('Location: '.$payment_url);
                     // exit;  
                      //$payment_url = returnUrl('payment/demo')."checkout_page.php?refid=".$InvoiceID.";   
                      $pay_fields = array(
                      "resp_code" =>"000",
                      "resp_desc" => "Request successfully received for processing",
                      "request_id" => $request_id,
                      "invoice_id" => $InvoiceID,
                      "redirect_url" => $payment_url
                      );

                      echo json_encode($pay_fields);

                    }
                  
              
                    $conn=null;  
                  
          
                  //}
    
           


?>