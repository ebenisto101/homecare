<?php
date_default_timezone_set("Africa/Lagos");
include_once("../../config/dbconnections.php");
include_once("../../config/constants.php");
include_once("../api_functions.php");
include_once("../../functions/functions.php");

$data = json_decode(file_get_contents('php://input'), true);
$ipaddress = getenv("REMOTE_ADDR"); 

global $conn,$conn4;



//{"method":"","ext_ref":"09CB324B7D5D411EFE31CC7048698908","request_id":"12345","trans_ref":"GP11694644820210216I","trans_status":200,"trans_id":"dc773eqw.1613483891"}

$Service_RequestID=$data["ext_ref"];
$requestID=$data["request_id"];
$trans_ref=$data["trans_ref"];
$trans_status=$data["trans_status"];
$message = $data['message'];
$now=date("Y-m-d H:i:s");



if(isset($data["request_id"])){
    $request_id=$data["request_id"];

if(isset($data["trans_id"])){
$trans_id=$data["trans_id"];
}else{$trans_id='';}


$callback_response = $data;

if($trans_status=="success"){
    
$service_status="approved";
$order_status="paid";
$delivery_status="pending";


   try {
             $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
             //update payment_request
             $query_insert = $conn->prepare("UPDATE `payment_requests` SET `Payment_NetworkTransID`=?,`Payment_Done_IP`=?, `Payment_StatusCode` = ?,`Payment_StatusDescription` = ?,`DateTime_PaymentDone` =?,`transRefId`=? WHERE `Service_RequestID`= ?;");
             $query_insert->execute(array($trans_id,NULL,$trans_status,$message,$now,$trans_ref,$Service_RequestID));

          }catch (PDOException $e) {
           echo "p_req upd ".$e->getMessage();
          }

//make payment

          $getclientData = $conn->prepare("SELECT a.*,b.id as patientID,b.card_number,b.surname,b.firstname,b.othernames,b.tel,b.email from clientvisits a left join client_information b on a.client_id=b.id where a.process_claim_no=?");
          $getclientData->execute(array($request_id));
          $rowClientData = $getclientData->fetch(PDO::FETCH_ASSOC);


          $insertPaymentQuery = "INSERT INTO `payments` (`patient_id`, `patient_no`, `receiptno`, `date_added`, `date_added_short`,`cashier_id`, `cashier_name`,`currency`,`amount`, `process_claim_no`, `total_bill`,`payment_type`) VALUES (?,?,?,NOW(),CURDATE(),?,?,?,?,?,?,?)";
        $insertPaymentData = array($rowClientData['patientID'], $rowClientData['card_number'], $trans_id, "1", "Online", CURRENCY, $rowClientData['packagePrice'], $request_id, $rowClientData['packagePrice'], "SinglePayment");

        $insertPayment = InsertData($insertPaymentQuery, $insertPaymentData);

        if($insertPayment>0){
            $paymentDetailsQuery = "INSERT INTO payment_details(`patient_id`, `patient_no`, `receiptno`, `date_added`, `date_added_short`,`cashier_id`, `cashier_name`,`currency`,`amount`, `process_claim_no`, `total_bill`,`payment_type`,`pay_by`,`bank`,`transaction_id`)VALUES(?,?,?,NOW(),CURDATE(),?,?,?,?,?,?,?,?,?,?)";
                $paymentDetailsData = array($rowClientData['patientID'], $rowClientData['card_number'], $trans_id, "1", "Online", CURRENCY, $rowClientData['packagePrice'], $request_id, $rowClientData['packagePrice'], "SinglePayment", "Online", "Paystack", $Service_RequestID);

                $paymentDetails = InsertData($paymentDetailsQuery, $paymentDetailsData);

                if($paymentDetails>0){
                    $updateClaimsQuery = "update claims_details set current_location=?, payment_status=?,receipt_number=?,cashier_user=?,date_of_payment=NOW(),action_status=?,validated=?,date_validated=NOW() WHERE process_claim_no=?";
                $updateClaimsData = array("Laboratory", "Paid", $trans_id, "1", "Sent To Laboratory", "1", $request_id);

                $updateClaims = UpdateData($updateClaimsQuery, $updateClaimsData);
                }




        }

             

echo json_encode($callback_response);
exit;





/*
//send a payment receipt sms and email
 $query_rsodest = $conn->prepare("SELECT * FROM payment_requests WHERE Service_RequestID=? LIMIT 1");
 $query_rsodest->execute(array($Service_RequestID));
 $record=$query_rsodest->fetch(PDO::FETCH_ASSOC);
 $total_amount= $record['Payment_Amount'];
 $pay_method= $record['Payment_Type'];
 $trans_id= $record['Payment_NetworkTransID'];
 $mobile_number= $record['Payment_MobilePhoneNo'];
 
 $text_msg = "You have successfully paid for your Vaccination with ID:".$requestID.". Payment transaction ID:".$trans_id.". Thank you.";
 
 //sendSmsApi($mobile_number,$text_msg);
 
 //sendSendApi($mobile_number,$text_msg,"V-Tracker");
 //compose_payment_receipt_email($requestID,$user_id,$Service_RequestID,$trans_id,$total_amount,$row,$pay_method);


//echo json_encode($data);
*/





}


if($trans_status=="paylater"){
    
$service_status="approved";
$order_status="owing";
$delivery_status="pending";

//inputTestData("call back inputs","123456");

$pay_fields = array(
    "resp_code" =>"000",
    "resp_desc" => "Request successfully received for processing",
    "request_id" => $request_id,
    );
                    



             

echo json_encode($callback_response);
exit;










}




}







