<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RX-Pay</title>
</head>
<body>
<!-- <script type="text/javascript" src="https://demo.api.glade.ng/checkout.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
<script src="https://js.paystack.co/v1/inline.js"></script> 
<script>

function sendPayCallback(request_id,data){
    
    let api_url="https://mobile.lagosvaccine.com/payment/callback.php";
    //let api_url="https://rxhealthbeta.com/mobileapi/rxapi_live/payment/demo/callback.php";
    
      $.ajax({
                  type: "POST",
                  url: api_url,
                  //data: JSON.stringify({"method": "","ext_ref": data.customer_txnref,"request_id":request_id,"trans_ref":data.txnRef,"trans_status":data.status,"trans_id":data.cardToken}),
                  data: JSON.stringify({"method": "","ext_ref": data.reference,"request_id":request_id,"trans_ref":data.trxref,"trans_status":data.status,"trans_id":data.transaction}),
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function(data){
                   //var json = JSON.parse(data);
                    console.log('HK:',data)  
                    //alert(data.trans_ref);
                    var myJSON = JSON.stringify(data);
                    //document.write(""+myJSON);
                    window.location.replace("https://mobile.lagosvaccine.com/payment/checkout_close.php?id="+data.request_id+"&tid="+data.trans_id);
                    
                  ;},
                  failure: function(errMsg) {
                    alert(errMsg);
                  }   
                    
               
            });
    
 } 


          //let email = "ekdedume@gmail.com";
          //let othernames = "Kwaku";
          //let surname = "Jimmy";
          let request_id = "<?php echo $_GET['refid'];?>";
         // let apk_key = "pk_test_1b7843591ba341ec2bafba83ff3b2e42bd92cab5";
          
         // let api_url="https://rxhealthbeta.com/mobileapi/rxapi_live/index.php";
          let api_url="https://mobile.lagosvaccine.com/index.php";
          let api_key="42353d5c33b45b0a8246b9bf0cd46820e516e3e4";
          let guid="D6C8A1C7-9A43-11E9-99B4-002590DD14B1";
          let user="web";
          let passcode="05b7de33ae3d105f25016eff01d687d9ef155636";
          
        
         function payWithPaystack() {
            
               $.ajax({
                    type: "POST",
                    url: api_url, 
                    data: JSON.stringify({"method": "GET_PAY_TOKEN","api_key": api_key,"guid": guid,"user": user,"passcode": passcode,"ref_id":"<?php echo $_GET['refid'];?>"}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data){
                          var myJSON = JSON.stringify(data);
                          console.log('GET_PAY_TOKEN:',myJSON)
                          let email = data.Service_CustomerEmail;
                          let request_id = data.payment_service_requestID;
                          let amount = data.Service_Amount * 100;
                          let public_api_key = data.Payment_Api_Key;
                          let client_name = data.Service_CustomerName;
                          let client_mobile = data.Service_CustomerTel;
                          let pay_request_id = data.Service_RequestID;
                          let trans_currency = data.Trans_Currency;
                          if(client_name!==null){
                          let nameArray = client_name.split(" ",1);
                          let firstname = nameArray[0];
                          let othernames = nameArray[1]; 
                          }else{
                          let firstname = " ";
                          let othernames = " ";  
                          }
                          
                          var handler = PaystackPop.setup({
                            key: public_api_key, // Replace with your public key
                            email: email,
                            amount: amount, // the amount value is multiplied by 100 to convert to the lowest currency unit
                            currency: trans_currency,    // Use GHS for Ghana Cedis or USD for US Dollars
                            ref: pay_request_id, // Replace with a reference you generated
                            channels:['card', 'bank', 'ussd', 'qr', 'mobile_money', 'bank_transfer'],
                            callback: function(response) {
                               console.log('CB:', response)
                               console.log('CB:', response.status)
                               var myJSON = JSON.stringify(response);
                               //alert('Payment response: ' + myJSON); 
                              
                              if(response.status=="success"){
                                  ///////////////////
                                  //alert('Payment response: ' + myJSON);
                                  sendPayCallback(request_id,response);
                             
                               }
                              // Make an AJAX call to your server with the reference to verify the transaction
                              
                            },
                            onClose: function() {
                              alert('Transaction was not completed, window closed.');
                               // window.open('','_parent','');
                               //window.close();
                            },
                          });
                          handler.openIframe();  
                                      
                
                    ;},
                    failure: function(errMsg) {
                        alert(errMsg);
                    }
                });  
             
              
        }
        
        
        payWithPaystack();
        
            

    
</script>
</body>
</html>
