<?php
date_default_timezone_set("Africa/Accra");
include("../../Connections/courier_pdo.php");
include("../../Connections/courier4_pdo.php");

$data = json_decode(file_get_contents('php://input'), true);
$ipaddress = getenv("REMOTE_ADDR"); 

global $conn,$conn4;


function sendSendApi($phone_number,$msg){
   $data = array(
                    "merchant_id" => "GPC15301763942018",
                    "merchant_key" => "b7e5488ecc6471234a49491aa62b837f",
                    "recipient_number" => $phone_number,
                    "sender_id" => "V-TRACKER",
                    "sms_content" => $msg
               );
              
                $url="https://rxquickpay.com/gppay/sendSms.php";
                $ch = curl_init( $url );
                $payload = json_encode($data,true);
                curl_setopt( $ch, CURLOPT_POSTFIELDS,$payload);
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt($ch, CURLOPT_TIMEOUT,30);
                   # Send request.
                $result = curl_exec($ch);
                curl_close($ch);
                $result=json_decode($result,true);  
    
}

function isUserAvailableInTracker($user_id){
global $conn4;
           $query_rsodest = $conn4->prepare("SELECT user_id,email from users where user_id=? LIMIT 1");
           $query_rsodest->execute(array($user_id));
           $rowCount = $query_rsodest->rowCount();
           $user = $query_rsodest->fetch();
            if ($rowCount>0)
            {
                return true;
            } else {
                return false;
            }
}


//{"method":"","ext_ref":"09CB324B7D5D411EFE31CC7048698908","request_id":"12345","trans_ref":"GP11694644820210216I","trans_status":200,"trans_id":"dc773eqw.1613483891"}

/*if(!isIpWhitelisted($ipaddress))
{
    exit();
} */

//echo $data;

//echo json_encode($data);

$Service_RequestID=$data["ext_ref"];
$requestID=$data["request_id"];
$trans_ref=$data["trans_ref"];
$trans_status=$data["trans_status"];
$now=date("Y-m-d H:i:s");
$message=$data["message"];



if(isset($data["request_id"])){

if(isset($data["trans_id"])){
$trans_id=$data["trans_id"];
}else{$trans_id='';}



if($trans_status=="success"){
    
$service_status="approved";
$order_status="paid";
$delivery_status="pending";


//update payment_request
$query_insert = $conn->prepare("UPDATE `payment_request_test` SET `Payment_NetworkTransID`=?,`Payment_Done_IP`=?, `Payment_StatusCode` = ?,`Payment_StatusDescription` = ?,`DateTime_PaymentDone` =?,`transRefId`=? WHERE `Service_RequestID`= ?;");
$query_insert->execute(array($trans_id,$ipaddress,$trans_status,$message,$now,$trans_ref,$Service_RequestID));

$query_insert = $conn->prepare("UPDATE `vaccination_bookings` SET `payment_status`=? WHERE `booking_request_id`= ?;");
$query_insert->execute(array("paid",$requestID));

//send a payment receipt sms and email
 $query_rsodest = $conn->prepare("SELECT * FROM payment_request_test WHERE Service_RequestID=? LIMIT 1");
 $query_rsodest->execute(array($Service_RequestID));
 $record=$query_rsodest->fetch(PDO::FETCH_ASSOC);
 $total_amount= $record['Payment_Amount'];
 $pay_method= $record['Payment_Type'];
 $trans_id= $record['Payment_NetworkTransID'];
 $mobile_number= $record['Payment_MobilePhoneNo'];
 
 $text_msg = "You have successfully paid for your Vaccination with ID:".$requestID.". Payment transaction ID:".$trans_id.". Thank you.";
 
 sendSendApi($mobile_number,$text_msg);
 //compose_payment_receipt_email($requestID,$user_id,$Service_RequestID,$trans_id,$total_amount,$row,$pay_method);


//echo json_encode($data);

}

if($trans_status=="free"){
    $order_status = "free";
    $query_insert = $conn->prepare("UPDATE `vaccination_bookings` SET `payment_status`=? WHERE `booking_request_id`= ?;");
    $query_insert->execute(array($order_status,$requestID));
}


//get booking details and push to provider db
 $query_rsodest = $conn->prepare("SELECT * FROM vaccination_bookings v,users u WHERE booking_request_id=? AND u.user_id=v.user_id LIMIT 1");
 $query_rsodest->execute(array($requestID));
 $data=$query_rsodest->fetch(PDO::FETCH_ASSOC);
 

    $user_id=filter_var($data['user_id'],FILTER_SANITIZE_STRING);
    $booking_option=filter_var($data['booking_option'],FILTER_SANITIZE_STRING);
    $surname=filter_var($data['surname'],FILTER_SANITIZE_STRING);    
    $othernames=filter_var($data['othernames'], FILTER_SANITIZE_STRING);
    $sex=filter_var($data['sex'], FILTER_SANITIZE_STRING);
    $date_of_birth=filter_var($data['date_of_birth'], FILTER_SANITIZE_STRING);
    $pregnancy_status=filter_var($data['pregnancy_status'], FILTER_SANITIZE_STRING);
    $nationality=filter_var($data['nationality'], FILTER_SANITIZE_STRING);
    $home_address=filter_var($data['home_address'], FILTER_SANITIZE_STRING);
    $email=filter_var($data['email'], FILTER_SANITIZE_STRING);
    $ghanapost_gps_address=filter_var($data['ghanapost_gps_address'], FILTER_SANITIZE_STRING);
    $location_cordinates=filter_var($data['location_cordinates'], FILTER_SANITIZE_STRING);
    $contact_number=filter_var($data['contact_number'], FILTER_SANITIZE_STRING);
    $mobile_number=filter_var($data['telephone_no'], FILTER_SANITIZE_STRING);
    $covid_status=filter_var($data['covid_status'], FILTER_SANITIZE_STRING);
    $date_tested_positive=filter_var($data['date_tested_positive'], FILTER_SANITIZE_STRING);
    $is_close_contacted=filter_var($data['is_close_contacted'], FILTER_SANITIZE_STRING);
    $fever_sign=filter_var($data['fever_sign'], FILTER_SANITIZE_STRING);
    $cough_sign=filter_var($data['cough_sign'], FILTER_SANITIZE_STRING);
    $headache_sign=filter_var($data['headache_sign'], FILTER_SANITIZE_STRING);
    $body_weakness_sign=filter_var($data['body_weakness_sign'], FILTER_SANITIZE_STRING);
    $sore_throat_sign=filter_var($data['sore_throat_sign'], FILTER_SANITIZE_STRING);
    $sneezing_sign=filter_var($data['sneezing_sign'], FILTER_SANITIZE_STRING);
    $running_nose_sign=filter_var($data['running_nose_sign'], FILTER_SANITIZE_STRING);
    $type_of_worker=filter_var($data['type_of_worker'], FILTER_SANITIZE_STRING);
    $place_of_work=filter_var($data['place_of_work'], FILTER_SANITIZE_STRING);
    $package_type=filter_var($data['package_type'], FILTER_SANITIZE_STRING);
    $package_price=filter_var($data['package_price'], FILTER_SANITIZE_STRING);
    $pass=filter_var($data['user_token'], FILTER_SANITIZE_STRING);  
    $booking_date=filter_var($data['booking_date'], FILTER_SANITIZE_STRING);
    $facility=filter_var($data['facility'], FILTER_SANITIZE_STRING);
    $region=filter_var($data['region'], FILTER_SANITIZE_STRING);
    $status="pending";

    $current_date = date("Y-m-d H:i:s"); 
    $attendee_name = $othernames." ".$surname;
    
    if(!isUserAvailableInTracker($user_id)){
         $stmt = $conn4->prepare("INSERT INTO users (user_id,telephone_no,surname,othernames,email,password,gender,date_of_birth,nationality,home_address,date_added) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
         $stmt->execute([$user_id,$mobile_number,$surname,$othernames,$email,$pass,$sex,$date_of_birth,$nationality,$home_address,$current_date]);
    }
 
 //  $query_rsodest = $conn4->prepare("INSERT INTO vaccination_bookings (`booking_request_id`, `stage`, `current_state`, `service_provider_id`, `requested_booking_date`, `attendee_name`, `appointment_status`, `service_amount`, `datetime_added`, `in_process_by`, `ghanapost_gps_address`, `location_cordinates`, `contact_number`, `covid_status`, `date_tested_positive`, `is_close_contacted`, `fever_sign`, `cough_sign`, `headache_sign`, `body_weakness_sign`, `sore_throat_sign`, `sneezing_sign`, `running_nose_sign`, `type_of_worker`, `place_of_work`, `package_type`, `package_price`, `status`, `datetime_added`)
    
    $query_rsodest = $conn4->prepare("INSERT INTO appointments (`booking_request_id`,`region`,`service_provider`,`user_id`,`booking_option`, `stage`, `current_state`, `service_provider_id`, `requested_booking_date`, `attendee_name`, `appointment_status`, `service_amount`, `datetime_added`, `in_process_by`, `covid_status`, `date_tested_positive`, `is_close_contacted`, `fever_sign`, `cough_sign`, `headache_sign`, `body_weakness_sign`, `sore_throat_sign`, `sneezing_sign`, `running_nose_sign`, `type_of_worker`, `place_of_work`, `package_type`, `package_price`)
      VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $query_rsodest->execute(array($requestID,$region,$facility,$user_id,$booking_option,"0","1","1",$booking_date,$attendee_name,$order_status,$package_price,$current_date,"1",$covid_status,$date_tested_positive,$is_close_contacted,$fever_sign,$cough_sign,$headache_sign,$body_weakness_sign,$sore_throat_sign,$sneezing_sign,$running_nose_sign,$type_of_worker,$place_of_work,$package_type,$package_price));

    if($query_rsodest->rowCount()>0){
        $resp_code = '000';
        $booking_id = $requestID;
        $redirect_url = '';
        $resp_msg = 'You have been booked successfully.';
    }else{
        $resp_code = '100';
        $resp_msg = 'booking request failed';
    }
    
    $results = array(
        "resp_code"=>"$resp_code",
        "resp_msg"=>"$resp_msg",
        "request_id"=>"$booking_id",
        "redirect_url"=>""
        );
    
  echo json_encode($results);


$conn=null;

}







