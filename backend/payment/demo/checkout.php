<?php
date_default_timezone_set("Africa/Accra");
include("../../Connections/courier_pdo.php");
include("../../Connections/courier4_pdo.php");

function returnUrl($folder="myfirst"){

    if(isset($_SERVER['HTTPS'])){
        $protocol=($_SERVER['HTTPS']!="off")?"https":"http";    
    }else{
        $protocol='http';
    }
    $protocol=$protocol."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URL']."/".$folder."/";
    return $protocol;
}

    $data = json_decode(file_get_contents('php://input'), true);
    $request_id=$data["request_id"];
    $amount=$data["amount"];
    $mobile_number=$data["mobile_number"];
    $client_name = $data["client_name"];
    $client_email = $data["client_email"];
    $trans_mode = $data["trans_mode"];

      if(is_null($amount)||empty($amount)){
        $amount= "5";
      }

      $now=date("Y-m-d H:i:s");
      
      if(is_null($request_id)||empty($request_id)){
           $date = new DateTime();
           $time = $date->format('U');
           $request_id = $time.rand(1000,9999); 
      }
     $description = "Product ".$request_id." payment";
     
     $payment_api_key = "pk_test_1b7843591ba341ec2bafba83ff3b2e42bd92cab5";
      
      global $conn;
      try {
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query_insert = $conn->prepare("INSERT INTO `payment_request_test` (`Payment_Api_Key`,`Service_Request_Channel`,`payment_service_requestID`, `Service_Description`, `Service_Amount`,`Actual_ServiceAmount`,`Service_CustomerName`, `Service_CustomerID`, `Service_ProviderID`,`Service_CustomerTel`,`Service_CustomerEmail`, `DateTime_ServiceRequested`, `Payment_Type`, `Payment_NetworkType`, `Payment_MobilePhoneNo`, `Payment_Amount`, `Payment_SMSAmount`, `Payment_Commission_Mobile_Money`, `Payment_Commission_Visa_MasterCard`, `Payment_Commission`, `Payment_ServiceCharge`, `Payment_TotalAmount`,`Payment_Request_Channel`) VALUES (?,?,?,?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?);");
            $query_insert->execute(array($payment_api_key,"test",$request_id,$description,$amount,$amount,$client_name,"","",$mobile_number,$client_email,$trans_mode,$trans_mode,$mobile_number,$amount,"0","0","0","0","0",$amount,"test"));
            $insert_id=$conn->lastInsertId();
         } catch (PDOException $e) {
           echo $e->getMessage();
         }

        try {
             $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
             $query_rsodest = $conn->prepare("select * from payment_request_test where Payment_RequestID=? LIMIT 1");
             $query_rsodest->execute(array($insert_id));
             $totalRows_rsodest = $query_rsodest->rowCount();
             $row_rsodest_all = $query_rsodest->fetch(PDO::FETCH_ASSOC);
             $Service_RequestID=$row_rsodest_all['Service_RequestID'];
             $InvoiceID=$row_rsodest_all['Service_RequestID'];
          }catch (PDOException $e) {
           echo $e->getMessage();
          }
             
            $payment_url = returnUrl('payment/demo')."checkout_page.php?refid=".$InvoiceID."&amt=".$amount."&rid=".$request_id;   
            $pay_fields = array(
            "resp_code" =>"000",
            "resp_desc" => "Request successfully received for processing",
            "request_id" => $request_id,
            "invoice_id" => $InvoiceID,
            "redirect_url" => $payment_url
            );
            echo json_encode($pay_fields);
      
    
      $conn=null;  
           


?>