<!DOCTYPE html>

<?php
date_default_timezone_set("Africa/Accra");
include("../../Connections/courier_pdo.php");
include("../../Connections/courier4_pdo.php");


$id = $_GET['id'];


function get_CERT_DETAILS($request_id){
global $conn4;

//$query_rsodest = $conn4->prepare("SELECT a.*,av.code,av.dose,v.vaccine_code,v.vaccine_name FROM appointments a INNER JOIN users u ON u.user_id = a.user_id LEFT JOIN appointment_list_vaccine av ON a.booking_request_id = av.booking_request_id LEFT JOIN vaccines v ON av.code = v.vaccine_code WHERE a.user_id=? ORDER BY timestamp DESC");
$query_rsodest = $conn4->prepare("SELECT a.*,u.surname,u.othernames,u.email,u.telephone_no,u.date_of_birth FROM appointments a INNER JOIN users u ON u.user_id = a.user_id WHERE a.booking_request_id=? ORDER BY timestamp DESC LIMIT 1");
$query_rsodest->execute(array($request_id));
$row=$query_rsodest->fetch(PDO::FETCH_ASSOC);

echo json_encode($row);
exit;

/*
foreach($row as $row){
    
    $request_id = $row['booking_request_id'];
  /*  $query_rsodest = $conn4->prepare("SELECT * from appointment_list_vaccine where booking_request_id=? LIMIT 1");
    $query_rsodest->execute(array($request_id));
    $result = $query_rsodest->fetch();
    $code = $result['code'];
    $dose = $result['dose'];  
    
      $service_provider = getProviderApiName($row['service_provider_id']);
    
      $orders[] = array(
      'id'=>$row['id'],
      'service_type'=>$row['service_type'],
      'booking_request_id'=>$row['booking_request_id'],
      'user_id'=>$row['user_id'],
      'service_provider_id'=>$row['service_provider_id'],
      'service_provider'=>$service_provider,
      'appointment_status'=>$row['appointment_status'],
      'requested_booking_date'=>$row['requested_booking_date'],
      'attendee_name'=>$row['attendee_name'],
      'attendee_age'=>$row['attendee_age'],
      'service_amount' =>$row['service_amount'],
      'service_status'=>$row['service_status'],
      'stage'=>$row['stage'],
      'current_state'=>$row['current_state'],
      'code'=>$row['code'],
      'dose'=>$row['dose'],
      'vaccine_id' =>$row['"vaccine_id'],
      'vaccine_code'=>$row['vaccine_code'],
      'vaccine_name' =>$row['vaccine_name'],
      'vaccine_code'=>$row['vaccine_code'],
      'package_type'=>$row['package_type'],
      'status'=>$row['status'],
      'datetime_added'=>$row['datetime_added']
        );
      }    */
  
 //echo json_encode($orders);
 
}



if($id!=""){
 $request_id = $id;
 
 //get_CERT_DETAILS($request_id);
 

/*
 $fields = array(
	'METHOD'=>"GETBOOKINGDETAILS",
	'ORDERNUM'=>$orderno
  );
  
  $get_insertDelete = APICall($fields);
  $get_insertdelete_json = json_decode($get_insertDelete,"true");
  $getResult = $get_insertdelete_json['RESULTS'];
  //print_r($getResult);
  */
  
  
}

?>

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Certificate</title>
    <link rel="stylesheet" href="assets1/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets1/css/styles.css"> 
</head>
<style type="text/css">
    @media print {
        #printButton{
            display: none;
        }
    #printableArea12{
         align-content: center;
         margin-left:650px
    }
}
</style>

<body style="border-color: rgb(115,120,125);">

    <br><br><br>
    <div></div>
    <div></div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="max-width: 50%;min-width: 50%;">
                    <p><img src="assets1/img/logo.jpg" width="100" height="100"></p>
                </div>
                <div class="col-md-6" style="max-width: 50%;min-width: 50%; text-align: right;">
                    <p><span><strong>Vaccination ID:</strong>&nbsp;</span><span>840965</span></p>
                    <p><span><strong>Date of Vaccination:</strong>&nbsp;</span><span>2021-02-24</span></p>
                    <p><span><strong>Time of Vaccination:</strong>&nbsp;</span><span>11:45:09 am</span></p>
                </div>
                
            </div>

            <div style="float: right; margin-right: -100px; margin-top: -120px">
                <img src="assets1/img/asa.jpg">
            </div>

        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="border-style: solid;">
                    <p>&nbsp;<span><strong>Lab:</strong>&nbsp;</span><span>RX Health Info</span><code>          </code><span><strong>Address:&nbsp;</strong></span><span>Lagos avenue</span></p>
                    <p><span><strong>Phone Number:</strong></span><span>0247159599</span><code>            </code><span><strong>Email:</strong></span><span>ekdedume@gmail.com</span></p>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="width: 720px;height: 100px;">
                    <h3 style="height: 120px;font-size: 30px;"><br>COVID-19 VACCINATION CERTIFICATE<br><br><br></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" >
                
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td style="text-align: right;">Emmanuel Jimmy</td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td style="text-align: right;">23 June, 1995</td>
                                </tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Nationality</td>
                                    <td style="text-align: right;">Nigeria</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td style="text-align: right;">Ashongman Estate-Lagos</td>
                                </tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Sex</td>
                                    <td style="text-align: right;">Male</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: right;"></td>
                                </tr>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <!-- <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>Travel Information</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Passport Number</td>
                                    <td style="text-align: right;">8545785457</td>
                                </tr>
                                <tr>
                                    <td>Airline</td>
                                    <td style="text-align: right;">wwe44547545</td>
                                </tr>
                                 <tr>
                                    <td></td>
                                    <td style="text-align: right;"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>-->
    </div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>Covid - 19 Vaccination Schedule</h4>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Vaccination Location</td>
                                    <td style="text-align: right;">NNPC Medical Services</td>
                                </tr>
                                 <tr>
                                    <td></td>
                                    <td style="text-align: right;"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>Covid - 19 Vaccination Details<br></h4>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Vaccine Used</td>
                                    <td style="text-align: right;">Astrazeneca Vaccine&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Number of Doses</td>
                                    <td style="text-align: right;">2</td>
                                </tr>
                                
                                <tr>
                                    <td style="font-weight: bolder;">Vaccination Status <br></td>
                                    <td style="font-weight: bolder; text-align: right;">Vaccinated<br></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
             <div class="row">
                 <div class="col-md-6">
                     <div  class="qrframe" style="border:none;  width:110px; height:110px; text-align: center;" id="printableArea">
               <img style="float:left; width: 210px; height: 210px" src="https://chart.googleapis.com/chart?chs=60x60&cht=qr&chl=<?php echo $qrCode1;?>&choe=UTF-8" title="Item Code"/>
             </div>
                 </div>

                 <div class="col-md-4">
                    <br><br>
                    <p style="font-size: 13px;"> Date & time generated:</p>
                    <p><strong>24 February, 2021 </strong></p>
                    <p><strong>11:45:00AM </strong></p>
                    <p>#0267878</p>

                 </div>
             </div>


                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
    </div>
    <div></div>
    <div></div>


    <script src="assets1/js/jquery.min.js"></script>
    <script src="assets1/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
