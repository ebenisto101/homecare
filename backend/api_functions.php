<?php
require_once dirname(__DIR__) . '/vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Mpdf\Tag\P;

if (!function_exists("authenticateAPI")) {
    function authenticateAPI($user, $password, $apikey, $conn)
    {
        global $conn;

        $queryAuthenticate = "SELECT * FROM apicheck where user=? and password=? and apikey=?";
        $selectData = array($user, $password, $apikey);
        $totleData = selectData($queryAuthenticate, $selectData);
        //print_r(json_encode($totleData));
        $totalRows = $totleData['totalcount'];
        $rowData = $totleData['rowResults'];

        return $totalRows;
    }
}

if (!function_exists("listUsers")) {
    function listUsers($data)
    {
        $query_rsodest = "SELECT * FROM users_accounts";
        $totleData = selectMultipleData($query_rsodest, null);
        $totalRows = $totleData['totalcount'];
        $rowData = $totleData['rowResults'];

        if ($totalRows > 0) {

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $totalRows,
                "RESULTS" => $rowData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);
    }
}

if (!function_exists("listAgency")) {
    function listAgency($data)
    {
        $query_rsodest = "SELECT * FROM agencies";
        $totleData = selectMultipleData($query_rsodest, null);
        $totalRows = $totleData['totalcount'];
        $rowData = $totleData['rowResults'];

        if ($totalRows > 0) {

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $totalRows,
                "RESULTS" => $rowData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);
    }
}


if (!function_exists('getCountrylist')) {
    function getCountrylist()
    {
        // echo "ok";
        $getCountry = "SELECT * FROM countrylist order by country";
        $totleData = selectMultipleData($getCountry);
        $totalRows = $totleData['totalcount'];
        $rowData = $totleData['rowResults'];

        if ($totalRows > 0) {

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $totalRows,
                "RESULTS" => $rowData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);
    }
}

if (!function_exists('saveAgency')) {
    function saveAgency($decoded)
    {

        $name = $decoded['NAME'];
        $email = $decoded['EMAIL'];
        $tel = $decoded['TEL'];
        $address = $decoded['ADDRESS'];
        $location = $decoded['LOCATION'];
        $state = $decoded['STATE'];
        $country = $decoded['COUNTRY'];


        $saveQuery = "INSERT INTO agencies (`agency_name`,`agency_address`,`agency_location`,`tel`,`email`,`region`,`country`) values (?,?,?,?,?,?,?)";
        $saveData = array($name, $address, $location, $tel, $email, $state, $country);
        $saveData = InsertData($saveQuery, $saveData);

        //    echo "INSERT INTO agencies (`agency_name`,`agency_address`,`agency_location`,`tel`,`email`,`region`,`country`) values ($name,$address,$location,$tel,$email,$state,$country)";

        if ($saveData > 0) {
            UpdateData("update agencies set agency_id=? where id=?", array($saveData, $saveData));

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA SAVED SUCCESSFULLY",
                "AGENCYID" => $saveData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Failed to Save" . $saveData
            );
        }


        return $results = json_encode($data);
    }
}

if (!function_exists('GetAgency')) {
    function GetAgency($decoded)
    {
        $agencyID = $decoded['AGENCYID'];


        $agency = selectData("select * from agencies where id=?", array($agencyID));
        $totalRows = $agency['totalcount'];

        if ($totalRows > 0) {
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA SAVED SUCCESSFULLY",
                "RESULTS" => $agency['rowResults']
            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found"
            );
        }

        return $results = json_encode($data);
    }
}

// update agency details
if (!function_exists('UpdateAgency')) {
    function UpdateAgency($decoded)
    {

        $name = $decoded['NAME'];
        $email = $decoded['EMAIL'];
        $tel = $decoded['TEL'];
        $address = $decoded['ADDRESS'];
        $location = $decoded['LOCATION'];
        $state = $decoded['STATE'];
        $country = $decoded['COUNTRY'];
        $agencyID = $decoded['AGENCYID'];


        $saveQuery = "UPDATE agencies SET `agency_name`=?,`agency_address`=?,`agency_location`=?,`tel`=?,`email`=?,`region`=?,`country`=? WHERE id=?";
        $saveData = array($name, $address, $location, $tel, $email, $state, $country, $agencyID);
        $saveData = UpdateData($saveQuery, $saveData);

        //    echo "INSERT INTO agencies (`agency_name`,`agency_address`,`agency_location`,`tel`,`email`,`region`,`country`) values ($name,$address,$location,$tel,$email,$state,$country)";

        if ($saveData == "SUCCESS") {


            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "AGENCY UPDATED SUCCESSFULLY",
                "AGENCYID" => $saveData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Failed to Update" . $saveData
            );
        }


        return $results = json_encode($data);
    }
}




// delete agency details
if (!function_exists('DeleteAgency')) {
    function DeleteAgency($decoded)
    {

        $agencyID = $decoded['AGENCYID'];


        $saveQuery = "UPDATE agencies SET `status`=? WHERE id=?";
        $saveData = array("Inactive", $agencyID);
        $saveData = UpdateData($saveQuery, $saveData);

        //    echo "INSERT INTO agencies (`agency_name`,`agency_address`,`agency_location`,`tel`,`email`,`region`,`country`) values ($name,$address,$location,$tel,$email,$state,$country)";

        if ($saveData == "SUCCESS") {


            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "AGENCY UPDATED SUCCESSFULLY",
                "AGENCYID" => $saveData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Failed to Update" . $saveData
            );
        }


        return $results = json_encode($data);
    }
}

//get list of permission roles
if (!function_exists('GetPermRoles')) {
    function GetPermRoles()
    {

        $getList = selectMultipleData("select * from permission_roles order by security_role");
        $totalRows = $getList['totalcount'];

        if ($totalRows > 0) {

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $totalRows,
                "RESULTS" => $getList['rowResults']

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);
    }
}


// add new user
if (!function_exists('addUser')) {
    function addUser($decoded)
    {

        $surname = $decoded['SURNAME'];
        $firstnames = $decoded['FIRSTNAME'];
        $email = $decoded['EMAIL'];
        $tel = $decoded['TEL'];
        $agency = $decoded['AGENCY'];
        $user_role = $decoded['USERROLE'];

        //GET USERROLE NAME
        $userRole = selectData("select * from permission_roles where id=?", array($user_role));
        $role = $userRole['rowResults']['security_role'];

        //generate default password
        $characters = array("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        $PASSSALT = $characters[rand(0, 24)] . $characters[rand(0, 24)];
        //generate default password
        $numbers = range(1, 10);
        shuffle($numbers);
        $requestid = implode("", array_slice($numbers, 0, 4));

        $newpassword = "NW" . $requestid . $PASSSALT;
        $newpasswordhash = md5(sha1($newpassword));

        //insert into users

        $queryInsertUser = InsertData("INSERT INTO users_accounts (`firstnames`,`surname`,`email`,`tel`,`agency_id`,`password`,`security`,`perm_role_id`)VALUES(?,?,?,?,?,?,?,?)", array($firstnames, $surname, $email, $tel, $agency, $newpasswordhash, $role, $user_role));


        if ($queryInsertUser > 0) {
            //user save successfully:send password to email
            $title = "Home Care Account";
            $msg = "Dear " . $surname . ", thank you for initiating the sign up process onto the Home Care Platform.<br> An account has been created for you on the platform. Kindly use your email and the password <b>" . $newpassword . "</b> to log in. Kindly note that you would be require to change this password once you login for the first time.";

            $sendmail = emailSender('ebenezer.rxhealthinfosystems@gmail.com', 'Home Care', $email, "Client", '', MailTemplate($title, $msg), $title);


            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $queryInsertUser

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Error Adding User" . $queryInsertUser
            );
        }

        return $results = json_encode($data);
    }
}



if (!function_exists('getSingleUser')) {
    function getSingleUser($decoded)
    {
        $userID = $decoded['USERID'];


        $user = selectData("select * from users_accounts where id=?", array($userID));
        $totalRows = $user['totalcount'];

        if ($totalRows > 0) {
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "RESULTS" => $user['rowResults']
            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found"
            );
        }

       return $results = json_encode($data);
    }
}


if (!function_exists('updateUser')) {
    function updateUser($decoded)
    {

        $surname = $decoded['SURNAME'];
        $firstnames = $decoded['FIRSTNAME'];
        $email = $decoded['EMAIL'];
        $tel = $decoded['TEL'];
        $agency = $decoded['AGENCY'];
        $user_role = $decoded['USERROLE'];
        $userID = $decoded['USERID'];

        //GET USERROLE NAME
        $userRole = selectData("select * from permission_roles where id=?", array($user_role));
        $role = $userRole['rowResults']['security_role'];

        $updateUser = UpdateData("update users_accounts set `firstnames`=?,`surname`=?,`email`=?,`tel`=?,`agency_id`=?,`security`=?,`perm_role_id`=? where id=?", array($firstnames, $surname, $email, $tel, $agency, $role, $user_role, $userID));

        if ($updateUser == "SUCCESS") {
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "AGENCY UPDATED SUCCESSFULLY",
                "AGENCYID" => $updateUser

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Failed to Update" . $updateUser
            );
        }


        return $results = json_encode($data);
    }
}


if (!function_exists('listemployees')) {
    function listemployees($decoded)
    {
        $agency = $decoded['AGENCYID'];

        $query_rsodest = "SELECT * FROM users_accounts WHERE agency_id=?";
        $totleData = selectMultipleData($query_rsodest, array($agency));
        $totalRows = $totleData['totalcount'];
        $rowData = $totleData['rowResults'];

        if ($totalRows > 0) {

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $totalRows,
                "RESULTS" => $rowData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);
    }
}


if(!function_exists('getEmployee')){
    function getEmployee($decoded){

        $userID = $decoded['USERID'];

        //get userdetails
        $getEmployee = selectData("SELECT * FROM users_accounts where id=?",array($userID));
        $found = $getEmployee['totalcount'];

        //get assigned clients
        $assignedClients = selectMultipleData("select a.*,b.* from assigned_client a inner join client_information b on a.patient_id=b.id where a.user_id=?",array($userID));
        $totalRows = $assignedClients['totalcount'];

        //getagency
       $agency = GetAgency(array("AGENCYID"=>$getEmployee['rowResults']['agency_id']));
       $agencyDetails = json_decode($agency,"true");
      

        if($totalRows>0){
            $assigned = array(
                "TOTALCOUNT"=>$totalRows,
                "ASSIGNEDCLIENTS"=>$assignedClients['rowResults']
            );
        }else {
            $assigned = array(
                "TOTALCOUNT"=>$totalRows,
                "ASSIGNEDCLIENTS"=>""
            );
        }

        if($found>0){
            
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $getEmployee['rowResults'],
                "ASSIGNED"=>$assigned,
                "AGENCY"=>$agencyDetails['RESULTS']['agency_name']

            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);
    }
}


if(!function_exists('listAgencyPatients')){
    function listAgencyPatients($decoded){

        $agencyID = $decoded['AGENCYID'];

        $query_rsodest = "SELECT * FROM client_information where agency_id=?";
        $totleData = selectMultipleData($query_rsodest, array($agencyID));
        $totalRows = $totleData['totalcount'];
        $rowData = $totleData['rowResults'];

        if ($totalRows > 0) {

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $totalRows,
                "RESULTS" => $rowData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);

    }
}


if(!function_exists('assignPatient')){
    function assignPatient($decoded){

        $employee = $decoded['EMPLOYEE'];
        $patient = $decoded['PATIENT'];

        $insertQuery = InsertData("INSERT INTO assigned_client (patient_id,user_id)VALUES(?,?)",array($patient,$employee));

        if($insertQuery>0){

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "User Assigned Successfully"
            );
        }else{ 

            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Patient Assignment Failed".$insertQuery
            );
        }

        return $results = json_encode($data);

    }
}


if(!function_exists('unassignedPatients')){
    function unassignedPatients($decoded){

        $agencyID = $decoded['AGENCYID'];
        $userID = $decoded['USERID'];

        $assignedClients = selectMultipleData("select * from client_information where agency_id=? and id NOT IN (SELECT patient_id from assigned_client where user_id=?)",array($agencyID,$userID));
        $totalRows = $assignedClients['totalcount'];
        

        if($totalRows>0){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Data Found",
                "RESULTS"=>$assignedClients['rowResults']
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Patients Found"
            );
        }

        return $results = json_encode($data);
    }
}



if(!function_exists('getPatient')){
    function getPatient($decoded){

        $patientID = $decoded['PATIENTID'];

        $getclient = selectData("SELECT *,(DATE_FORMAT(current_date, '%Y') - DATE_FORMAT(dateofbirth, '%Y') - (DATE_FORMAT(current_date, '00-%m-%d') < DATE_FORMAT(dateofbirth, '00-%m-%d'))) AS age FROM client_information where id=?",array($patientID));
        $found = $getclient['totalcount'];

        //get assigned clients
        $assignedClients = selectMultipleData("select a.*,b.* from assigned_client a inner join users_accounts b on a.user_id=b.id where a.patient_id=?",array($patientID));
        $totalRows = $assignedClients['totalcount'];

        //getagency
       $agency = GetAgency(array("AGENCYID"=>$getclient['rowResults']['agency_id']));
       $agencyDetails = json_decode($agency,"true");
      

        if($totalRows>0){
            $assigned = array(
                "TOTALCOUNT"=>$totalRows,
                "ASSIGNEDCLIENTS"=>$assignedClients['rowResults']
            );
        }else {
            $assigned = array(
                "TOTALCOUNT"=>$totalRows,
                "ASSIGNEDCLIENTS"=>""
            );
        }

        if($found>0){
            
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $getclient['rowResults'],
                "ASSIGNED"=>$assigned,
                "AGENCY"=>$agencyDetails['RESULTS']['agency_name']

            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);
    }

}



if(!function_exists('changePassword')){
    function changePassword($decoded){

        $userID = $decoded['USERID'];
        $newPassword = md5(sha1($decoded['NEWPASSWORD']));

        $updatePassword = UpdateData("update users_accounts set password=?,req_pass_change=0 where id=?",array($newPassword,$userID));

       
        if($updatePassword=="SUCCESS"){
            $_SESSION['chgpassword']=NULL;
            unset($_SESSION['chgpassword']);

            $_SESSION['chgpassword'] = 0;
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "PASSWORD UPDATED SUCCESSFULLY",
                "AGENCYID" => $updatePassword

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Failed to Update" . $updatePassword
            );
        }

        return $results = json_encode($data);
    }
}




if (!function_exists('saveClientInfo')) {
    function saveClientInfo($data)
    {
        $surname = $data['SURNAME'];
        $othername = $data['OTHERNAMES'];
        $firstname = $data['FIRSTNAME'];
        $gender = $data['GENDER'];
        $dob = $data['DOB'];
        $tel = $data['TEL'];
        $email = $data['EMAIL'];
        $residential = $data['RESIDENTIAL'];
        $userID = $data['USERID'];
        $nationality = $data['NATIONALITY'];
        $reg_yr = date('Y');
        $picpath = "";
        $agency = $data['AGENCY'];
        $rel_name = $data['RELATIVENAME'];
        $rel_tel = $data['RELATIVETEL'];
        $rel_relationship = $data['RELATIVERELATIONSHIP'];
        $condition= $data['CONDITION'];
        

        $user_id_hash = password_hash($tel.$dob.$firstname,PASSWORD_DEFAULT);
        $user_id = substr($user_id_hash, strlen($user_id_hash) - 25, strlen($user_id_hash));
        $user_id = preg_replace("/[^a-zA-Z0-9]/","", $user_id);

        $insertQuery = "INSERT INTO client_information (`surname`,`othernames`,`firstname`,`dateofbirth`,`sex`,`country_of_origin`,`tel`,`email`,`agency_id`,`emergency_contact_name`,`emergency_contact_tel`,`emergency_contact_relationship`,`home_address`,`picpath`,`user_added`,`patient_status`,`reg_yr`,`user_id`,`condition`)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $insertData = array($surname, $othername, $firstname, $dob, $gender, $nationality, $tel, $email, $agency, $rel_name, $rel_tel, $rel_relationship, $residential,$picpath, $userID, "Active", $reg_yr,$user_id,$condition);
        $totleData = InsertData($insertQuery, $insertData);

        if ($totleData > 0) {

            $pid = $totleData;
            $alphabet_no = intval($pid / 9999);
            // $f = fmod($pid, 9999);
            $fraction = ($pid / 9999) - $alphabet_no;
            $modrx = $fraction * 9999;
            $f = round($modrx);
            // $f = fmod($pid, 9999);
            $characters = array("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
            $startalphabet = $characters[$alphabet_no];
            $lennum = strlen($f);
            $patient_id = SHTCODE . $startalphabet . str_repeat("0", (4 - $lennum)) . $f . "/" . date('y');

            //UPDATE CLIENT INFO
            $updateQuery = "update client_information set card_number=? where id=?";
            $updateData = array($patient_id, $pid);
            $updateResults = UpdateData($updateQuery, $updateData);

            if ($updateResults == "SUCCESS") {

                $selectQuery = "SELECT * FROM client_information where id=?";
                $selectData = array($pid);
                $totleData = selectData($selectQuery, $selectData);
                //print_r(json_encode($totleData));
                $totalRows = $totleData['totalcount'];
                $rowData = $totleData['rowResults'];
                //echo $totalRows;


                if ($totalRows > 0) {

                    $data = array(
                        "STATUSCODE" => "000",
                        "STATUSMSG" => "DATA FOUND",
                        "TOTALROWS" => $totalRows,
                        "RESULTS" => $rowData

                    );
                } else {
                    $data = array(
                        "STATUSCODE" => "001",
                        "STATUSMSG" => "NO RECORDS FOUND"
                    );
                }
            } else {
                $data = array(
                    "STATUSCODE" => "002",
                    "STATUSMSG" => $updateResults
                );
            }
        } else {
            $data = array(
                "STATUSCODE" => "003",
                "STATUSMSG" => $totleData
            );
        }

        return $results = json_encode($data);
    }
}


if (!function_exists('updateClientInfo')) {
    function updateClientInfo($data)
    {
        $surname = $data['SURNAME'];
        $othername = $data['OTHERNAMES'];
        $firstname = $data['FIRSTNAME'];
        $gender = $data['GENDER'];
        $dob = $data['DOB'];
        $tel = $data['TEL'];
        $email = $data['EMAIL'];
        $residential = $data['RESIDENTIAL'];
        $userID = $data['USERID'];
        $nationality = $data['NATIONALITY'];
        $reg_yr = date('Y');
        $picpath = "";
        $agency = $data['AGENCY'];
        $rel_name = $data['RELATIVENAME'];
        $rel_tel = $data['RELATIVETEL'];
        $rel_relationship = $data['RELATIVERELATIONSHIP'];
        $condition= $data['CONDITION'];
        $patientID = $data['PATIENTID'];

        $insertQuery = "UPDATE client_information SET `surname`=?,`othernames`=?,`firstname`=?,`dateofbirth`=?,`sex`=?,`country_of_origin`=?,`tel`=?,`email`=?,`emergency_contact_name`=?,`emergency_contact_tel`=?,`emergency_contact_relationship`=?,`home_address`=?,`picpath`=?,`user_updated`=?,`condition`=? where id=?";
        $insertData = array($surname, $othername, $firstname, $dob, $gender, $nationality, $tel, $email,$rel_name, $rel_tel, $rel_relationship, $residential,$picpath, $userID,$condition,$patientID);
        $totleData = UpdateData($insertQuery, $insertData);

        if ($totleData =="SUCCESS") {

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Data Updated",
            );
                
           
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Failed to Update ".$totleData
            );
        }

        return $results = json_encode($data);
    }
}


if(!function_exists('updatePatientStatus')){
    function updatePatientStatus($decoded){
        $patientID = $decoded['PATIENTID'];
        $status= $decoded['STATUS'];


        if($status=='On'){
            $Nstatus="Inactive";
        }else if($status=="Off"){
            $Nstatus = "Active";
        }

        $pUpdate = UpdateData("UPDATE client_information set patient_status=? where id=?",array($Nstatus,$patientID));

        if($pUpdate=="SUCCESS"){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Data Updated",
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Failed to Update ".$pUpdate
            );
        }


        return $results = json_encode($data);
    }
}


if(!function_exists('assignedPatients')){
    function assignedPatients($decoded){

        $userid = $decoded['USERID'];

        $assignedClients = selectMultipleData("select a.*,b.*, b.id as patientID from assigned_client a inner join client_information b on a.patient_id=b.id where a.user_id=? AND b.patient_status=?",array($userid,"Active"));
        $totalRows = $assignedClients['totalcount'];

        if($totalRows>0){

            $rowData = $assignedClients['rowResults'];
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $totalRows,
                "RESULTS" => $rowData

            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$assignedClients
            );
        }

        return $results = json_encode($data);
    }
}

if(!function_exists('patientAllergies')){
    function patientAllergies($decoded){
        $patientID = $decoded['PATIENTID'];


        $getAllergies = selectMultipleData("select * from allergy_history where patient_id=? order by date_added DESC LIMIT 0,3",array($patientID));
        $foundAllergy = $getAllergies['totalcount'];

        if($foundAllergy>0){
            $rowData = $getAllergies['rowResults'];
            
            $data = array(
            "STATUSCODE" => "000",
            "STATUSMSG" => "DATA FOUND",
            "TOTALROWS" => $foundAllergy,
            "RESULTS" => $rowData

        );
    }else{
        $data = array(
            "STATUSCODE" => "001",
            "STATUSMSG" => "No Data Found ".$getAllergies
        );
    }

    return $results = json_encode($data);

    }
}


if(!function_exists('listVitals')){
    function listVitals($decoded){
        $patientID = $decoded['PATIENTID'];
        $OFFSET = $decoded['OFFSET'];

        if($OFFSET=="ALL"){
            $getvitals = selectMultipleData("SELECT * FROM patient_vitals where patient_id=? ORDER BY date DESC",array($patientID));
        }else{
            $getvitals = selectMultipleData("SELECT * FROM patient_vitals where patient_id=? ORDER BY date DESC LIMIT 0,5",array($patientID));
        }
       
        $found = $getvitals['totalcount'];

        if($found>0){
            $rowData = $getvitals['rowResults'];
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $rowData
    
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$getvitals
            );
        }
    
        return $results = json_encode($data);
    
    }
}



if(!function_exists('listAllergies')){
    function listAllergies(){

        //get allergies
        $selectAllergies = selectMultipleData("select * from allergy_data order by allergy_name ASC");
        $found = $selectAllergies['totalcount'];

        if($found>0){
            $rowData = $selectAllergies['rowResults'];
            
            $data = array(
            "STATUSCODE" => "000",
            "STATUSMSG" => "DATA FOUND",
            "TOTALROWS" => $found,
            "RESULTS" => $rowData

        );
    }else{
        $data = array(
            "STATUSCODE" => "001",
            "STATUSMSG" => "No Data Found ".$selectAllergies
        );
    }

    return $results = json_encode($data);
    }
}

if(!function_exists('listReaction')){
    function listReaction(){
     
         //get allergies
         $selectReaction = selectMultipleData("select * from allergy_reactions order by allergy_reaction ASC");
         $found = $selectReaction['totalcount'];
 
         if($found>0){
             $rowData = $selectReaction['rowResults'];
             
             $data = array(
             "STATUSCODE" => "000",
             "STATUSMSG" => "DATA FOUND",
             "TOTALROWS" => $found,
             "RESULTS" => $rowData
 
         );
     }else{
         $data = array(
             "STATUSCODE" => "001",
             "STATUSMSG" => "No Data Found ".$selectReaction
         );
     }
 
     return $results = json_encode($data);
    }
}


if(!function_exists('saveAllergy')){
    function saveAllergy($decoded){

        $allergy = $decoded['ALLERGY'];
        $reaction = $decoded['ALLERGYREACTION'];
        $patientID = $decoded['PATIENTID'];


        $insertData = InsertData("INSERT INTO allergy_history(`patient_id`,`allergy`,`reaction`)VALUES(?,?,?)",array($patientID,$allergy,$reaction));


        if($insertData>0){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA Saved Successfully"
            );
                
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Error Saving Allergy ".$insertData
            );
        }

        return $results = json_encode($data);
    }
}


if(!function_exists('saveVitals')){
    function saveVitals($decoded){

        $weight = $decoded['WEIGHT'];
        $height = $decoded['HEIGHT'];
        $temp = $decoded['TEMP'];
        $respiration = $decoded['RESPIRATION'];
        $oxy_saturation = $decoded['OXYSATURATION'];
        $bp1 = $decoded['BP1'];
        $bp2 = $decoded['BP2'];
        $pulse = $decoded['PULSE'];
        $patientID = $decoded['PATIENTID'];
        $user_id = $decoded['USERID'];


        $insertVitals = InsertData("insert into patient_vitals(`patient_id`,`height`,`weight`,`temperature`,`pulse`,`resp`,`bp1`,`bp2`,`oxygen_saturation`,`user_id`)VALUES(?,?,?,?,?,?,?,?,?,?)",array($patientID,$height,$weight,$temp,$pulse,$respiration,$bp1,$bp2,$oxy_saturation,$user_id));

        if($insertVitals>0){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA Saved Successfully"
            );
                
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Error Saving Allergy ".$insertVitals
            );
        }

        return $results = json_encode($data);
    }
}



if(!function_exists('listNotes')){
    function listNotes($decoded){
        $patientID = $decoded['PATIENTID'];
        $OFFSET = $decoded['OFFSET'];

        if($OFFSET=="ALL"){
            $getvitals = selectMultipleData("SELECT * FROM nurses_note where patient_id=? ORDER BY date_added DESC",array($patientID));
        }else{
            $getvitals = selectMultipleData("SELECT * FROM nurses_note where patient_id=? ORDER BY date_added DESC LIMIT 0,5",array($patientID));
        }
       
        $found = $getvitals['totalcount'];

        if($found>0){
            $rowData = $getvitals['rowResults'];
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $rowData
    
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$getvitals
            );
        }
    
        return $results = json_encode($data);
    }
}



if(!function_exists('saveNote')){
    function saveNote($decoded){
        // using the same function for add new and update old
        //PatientID for add new will be patient id of pateint
        //patientid for update will be id of record to tbe updated.

        $note = $decoded['VALUE'];
        $patientID = $decoded['PATIENTID'];
        $user_id = $decoded['USERID'];
        $type = $decoded['TYPE'];


        if($type=="saveNote"){
        $insertNotes = InsertData("insert into nurses_note(`patient_id`,`user_id`,`note`)VALUES(?,?,?)",array($patientID,$user_id,$note));

        if($insertNotes>0){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA Saved Successfully"
            );
                
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Error Saving Note ".$insertNotes
            );
        }
    }

    if($type=="UpdateNote"){
        $updatetNotes = UpdateData("UPDATE nurses_note SET `note`=?,updated_user=? where id=?",array($note,$user_id,$patientID));

        if($updatetNotes=="SUCCESS"){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA Saved Successfully"
            );
                
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "Error Updating  note ".$updatetNotes
            );
        }
    }

    

        return $results = json_encode($data);
    }
}


if(!function_exists('listPatientDrugs')){
    function listPatientDrugs($decoded){
        $patientID = $decoded['PATIENTID'];
        $OFFSET = $decoded['OFFSET'];

        if($OFFSET=="ALL"){
            $getvitals = selectMultipleData("SELECT * FROM medical_items where patient_id=? and item_service=? ORDER BY date_added DESC",array($patientID,"Drugs"));
        }else{
            $getvitals = selectMultipleData("SELECT * FROM medical_items where patient_id=? and item_service=? ORDER BY date_added DESC LIMIT 0,5",array($patientID,"Drugs"));
        }
       
        $found = $getvitals['totalcount'];

        if($found>0){
            $rowData = $getvitals['rowResults'];
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $rowData
    
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$getvitals
            );
        }
    
        return $results = json_encode($data);
    }
}


if(!function_exists('getItems')){
    function getItems($decoded){
        $patientID = $decoded['PATIENTID'];
        $TYPE = $decoded['TYPE'];

       
            $getItems = selectMultipleData("SELECT * FROM items where service=? ORDER BY item ASC",array($TYPE));
    
       
        $found = $getItems['totalcount'];

        if($found>0){
            $rowData = $getItems['rowResults'];
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $rowData
    
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$getItems
            );
        }
    
        return $results = json_encode($data);
    }
}


if(!function_exists('getDose')){
    function getDose($decoded){
        
       
            $getDose = selectMultipleData("SELECT * FROM dosage_form ORDER BY dosage_form ASC");
            $getFrequency = selectMultipleData("SELECT * FROM frequency ORDER BY frequency ASC");
    
       
        $found = $getDose['totalcount'];

        if($found>0){
            $rowDataDose = $getDose['rowResults'];
            $rowDataDF = $getFrequency['rowResults'];
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "DOSAGE" => $rowDataDose,
                "FREQUENCY"=>$rowDataDF
    
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$getDose
            );
        }
    
        return $results = json_encode($data);
    }
}


if(!function_exists('savePrescription')){
    function savePrescription($decoded){

        $patientID = $decoded['PATIENTID'];
        $userid = $decoded['USERID'];
        $itemID = $decoded['ITEMID'];
        $dose = $decoded['DOSE'];
        $dosage = $decoded['DOSAGE'];
        $frequency = $decoded['FREQ'];
        $noofdays = $decoded['NOOFDAYS'];
        $agency = $decoded['AGENCYID'];

        //GET ITEM DETAILS
        $getItem = selectData("select * from items where id=?",array($itemID));

        $found = $getItem['totalcount'];

        if($found>0){
            $itemDetails = $getItem['rowResults'];

            //insert into medical items
            $insertItem = InsertData("INSERT INTO medical_items (`patient_id`,`agency_id`,`user_id`,`item_id`,`item_code`,`item`,`item_name`,`item_service`,`qty`,`dose`,`dosage_form`,`frequency`,`no_of_days`)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",array($patientID,$agency,$userid,$itemID,$itemDetails['service_code'],$itemDetails['item'],$itemDetails['item_name'],$itemDetails['service'],"1",$dose,$dosage,$frequency,$noofdays));

            if($insertItem>0){
                $data = array(
                    "STATUSCODE" => "000",
                    "STATUSMSG" => "Prescription Added Successfully"
                );
            }else{
                $data = array(
                    "STATUSCODE" => "001",
                    "STATUSMSG" => "Error Saving Prescription ".$insertItem
                );
            }

        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$getItem
            );
        }

        return $results=json_encode($data);

    }
}


if(!function_exists('saveTreatment')){
    function saveTreatment($decoded){

        $patientID = $decoded['PATIENTID'];
        $userid = $decoded['USERID'];
        $itemID = $decoded['ITEMID'];
        $claimID = $decoded['CLAIMSID'];
        $date_recorded = $decoded['DATERECORDED'];
        $time_recorded = $decoded['TIMERECORDED'];
        $status = $decoded['STATUS'];
        $remarks = $decoded['REMARKS'];
        

        $insertTreatment = InsertData("INSERT INTO patient_treatment (`patient_id`,`item_id`,`medical_record_id`,`date_recorded`,`time_recorded`,`status`,`remarks`,`user_id`)VALUES(?,?,?,?,?,?,?,?)",array($patientID,$itemID,$claimID,$date_recorded,$time_recorded,$status,$remarks,$userid));

        if($insertTreatment>0){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Treatment Recorded Successfully"
            );
        }else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$insertTreatment
            )
            ;
        }

        return $results = json_encode($data);
    }
}



//LIST TREATMENT RECORDS

if(!function_exists('listTreatments')){
    function listTreatments($decoded){
        $patientID = $decoded['PATIENTID'];


        $getTreatments = selectMultipleData("SELECT DISTINCT a.date_recorded,GROUP_CONCAT(a.`time_recorded` SEPARATOR ' | ') as timesRecorded,a.item_id,b.item,GROUP_CONCAT(a.`user_id` SEPARATOR ',') as userids FROM patient_treatment a inner join items b on a.item_id=b.id  where patient_id=? group by date_recorded,item_id ORDER BY date_recorded DESC",array($patientID));


        $found = $getTreatments['totalcount'];
        if($found>0){
            $rowData = $getTreatments['rowResults'];

            $data= array(
                "STATUSCODE"=>"000",
                "STATUSMSG"=>"Data Found",
                "RESULTS"=>$rowData
            );
        }else{
            $data= array(
                "STATUSCODE"=>"001",
                "STATUSMSG"=>"No Data Found".$getTreatments
                
            );   
        }

        return $results = json_encode($data);
    }

}



if(!function_exists('listGlucose')){
    function listGlucose($decoded){
        $patientID = $decoded['PATIENTID'];
        $OFFSET = $decoded['OFFSET'];

        if($OFFSET=="ALL"){
            $getvitals = selectMultipleData("SELECT * FROM glucose_monitoring where patient_id=? ORDER BY date_added DESC",array($patientID));
        }else{
            $getvitals = selectMultipleData("SELECT * FROM glucose_monitoring where patient_id=? ORDER BY date_added DESC LIMIT 0,5",array($patientID));
        }
       
        $found = $getvitals['totalcount'];

        if($found>0){
            $rowData = $getvitals['rowResults'];
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $rowData
    
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$getvitals
            );
        }
    
        return $results = json_encode($data);
    }
}



if(!function_exists('savePatientGlucose')){
    function savePatientGlucose($decoded){

        $patientID = $decoded['PATIENTID'];
        $userid = $decoded['USERID'];
        $date_recorded = $decoded['DATERECORDED'];
        $time_recorded = $decoded['TIMERECORDED'];
        $rbs = $decoded['RBS'];
        $fbs = $decoded['FBS'];
        $remarks = $decoded['REMARKS'];
        

        $insertTreatment = InsertData("INSERT INTO glucose_monitoring (`patient_id`,`date_recorded`,`time_recorded`,`fbs`,`rbs`,`remarks`,`user_id`)VALUES(?,?,?,?,?,?,?)",array($patientID,$date_recorded,$time_recorded,$fbs,$rbs,$remarks,$userid));

        if($insertTreatment>0){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Glucose Recorded Successfully"
            );
        }else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$insertTreatment
            )
            ;
        }

        return $results = json_encode($data);
    }
}



if(!function_exists('listUrine')){
    function listUrine($decoded){
        $patientID = $decoded['PATIENTID'];
        $OFFSET = $decoded['OFFSET'];

        if($OFFSET=="ALL"){
            $geturine = selectMultipleData("SELECT * FROM urine_monitoring where patient_id=? ORDER BY date_added DESC",array($patientID));
        }else{
            $geturine = selectMultipleData("SELECT * FROM urine_monitoring where patient_id=? ORDER BY date_added DESC LIMIT 0,5",array($patientID));
        }
       
        $found = $geturine['totalcount'];

        if($found>0){
            $rowData = $geturine['rowResults'];
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $rowData
    
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$geturine
            );
        }
    
        return $results = json_encode($data);
    }
}


if(!function_exists('savePatientUrine')){
    function savePatientUrine($decoded){
        $patientID = $decoded['PATIENTID'];
        $userid = $decoded['USERID'];
        $date_recorded = $decoded['DATERECORDED'];
        $time_recorded = $decoded['TIMERECORDED'];
        $protein = $decoded['PROTEIN'];
        $sugar = $decoded['SUGAR'];
        $weight = $decoded['WEIGHT'];
        $kerotones = $decoded['KEROTONES'];
        $other = $decoded['OTHER'];
        

        $insertTreatment = InsertData("INSERT INTO urine_monitoring (`patient_id`,`date_recorded`,`time_recorded`,`protein`,`sugar`,`weight`,`kerotones`,`other`,`user_id`)VALUES(?,?,?,?,?,?,?,?,?)",array($patientID,$date_recorded,$time_recorded,$protein,$sugar,$weight,$kerotones,$other,$userid));

        if($insertTreatment>0){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Urine Recorded Successfully"
            );
        }else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$insertTreatment
            )
            ;
        }

        return $results = json_encode($data);
    }
}


if(!function_exists('listDiseases')){
    function listDiseases(){
    
     $getDisease = selectMultipleData("SELECT * FROM diseases ORDER BY Disease ASC",array());
    
      //var_dump($getDisease);
        $found = $getDisease['totalcount'];
        $rowData = $getDisease['rowResults'];
        

        if($found>0){
           // print_r($rowData);
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $found,
                "RESULTS" => $rowData
    
            );
        }else{
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$getDisease
            );
        }
    
    //    print_r(json_encode($data));
    //    echo json_last_error();
        return $results = json_encode($data,JSON_INVALID_UTF8_IGNORE);
    }
}



if (!function_exists('generateVisitID')) {
    function generateVisitID($data)
    {
        $clientID = $data['CLIENTID'];
        $userID = $data['USER'];
        $AgencyID = $data['AGENCYID'];
        

        //getclientdata
        $clientInfoQuery = "SELECT * FROM client_information where id=?";
        $clientInfoData = array($clientID);

        $clientInfo = selectData($clientInfoQuery, $clientInfoData);
        $ClientFound = $clientInfo['totalcount'];
        $clientInfoResults = $clientInfo['rowResults'];

        if ($ClientFound > 0) {

            $modeofpayment = $clientInfoResults['typeofclient'];           
            $status = 'In Use';

            $query_rsodest_claim = "select *, DATE_FORMAT(datetimeadded, '%e %b %Y %r') as dateprocessing from clientvisits where  client_id=? and agency_id=? and status=?";
            $queryData = array($clientID, $AgencyID, $status);
            
           
            $rowEncounter = selectData($query_rsodest_claim, $queryData);
            $totalEncounter = $rowEncounter['totalcount'];
            $ExistingEncounter = $rowEncounter['rowResults'];

            if ($totalEncounter == 0) {

                       
                $characters = array("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9");
                $PASSSALT = $characters[rand(0, 32)] . $characters[rand(0, 32)];

                //generate ID
                $min = 1;
                $max = 50;
                $loopcount = 5;
                $lab_request_id = generateIDNumeric($min, $max, $loopcount);

                // $encounterID = SHTCODE . $lab_request_id . $PASSSALT . date('y');
                $encounterID = SHTCODE . $lab_request_id;


                // End ID Generation

                //insert GENERATED DATA

                $query_rsodest = "INSERT INTO `clientvisits` (`agency_id`, `client_id`, `datetimeadded`, `date_added`, `user_id`,`process_claim_no`,`status`) VALUES (?,?,NOW(),CURDATE(),?,?,?)";
                $insertEncounterData = array($AgencyID, $clientID, $userID, $encounterID,$status);


                $RunEncounterInsert = InsertData($query_rsodest, $insertEncounterData);

                //get inserted data
                if ($RunEncounterInsert > 0) {
                    $getInsertedQuery = "SELECT *,DATE_FORMAT(datetimeadded, '%e %b %Y %r') as dateprocessing FROM clientvisits where id=?";
                    $getQueryData = array($RunEncounterInsert);

                    $getInsertedData = selectData($getInsertedQuery, $getQueryData);
                    $totalRows = $getInsertedData['totalcount'];
                    $rowData = $getInsertedData['rowResults'];

                    if ($totalRows > 0) {

                        $data = array(
                            "STATUSCODE" => "000",
                            "STATUSMSG" => "ENCOUNTER ID GENERATED SUCCESSFULLY",
                            "TOTALROWS" => $totalRows,
                            "RESULTS" => $rowData

                        );
                    } else {
                        $data = array(
                            "STATUSCODE" => "003",
                            "STATUSMSG" => "NO RECORDS FOUND"
                        );
                    }
                } else {
                    $data = array(
                        "STATUSCODE" => "002",
                        "STATUSMSG" => "UNABLE TO INSERT RECORDS->" . $RunEncounterInsert
                    );
                }
            } else {
                $data = array(
                    "STATUSCODE" => "000",
                    "STATUSMSG" => "ALREADY EXISTING ENCOUNTER OPENED",
                    "TOTALROWS" => $totalEncounter,
                    "RESULTS" => $ExistingEncounter

                );
            }
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "INVALID CLIENT ID"
            );
        }

        return $results = json_encode($data);
    }
}


function generateIDNumeric($min, $max, $quantity)
{

    $numbers = range($min, $max);
    shuffle($numbers);
    $requestid = implode("", array_slice($numbers, 0, $quantity));

    //check if generated ID already exist

    $queryID = "SELECT * FROM clientvisits where process_claim_no=?";
    $queryData = array($requestid);
    $rowQuery = selectData($queryID, $queryData);
    $trows = $rowQuery['totalcount'];

    if ($trows > 0) {
        return generateIDNumeric($min, $max, $quantity);
    }

    return $requestid;
}


if (!function_exists('listEncounters')) {
    function listEncounters($decoded)
    {
        $clientID = $decoded['PATIENTID'];

        $SelectQuery = "SELECT * FROM clientvisits where client_id=? order by datetimeadded DESC";
        $selectData = array($clientID);

        $totleData = selectMultipleData($SelectQuery, $selectData);
        $totalRows = $totleData['totalcount'];
        $rowData = $totleData['rowResults'];

        if ($totalRows > 0) {

            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "DATA FOUND",
                "TOTALROWS" => $totalRows,
                "RESULTS" => $rowData

            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "NO RECORDS FOUND"
            );
        }

        return $results = json_encode($data);
    }
}


if (!function_exists('getSecurityLevelsPermissions')) {
    function getSecurityLevelsPermissions()
    {

        $query_rsodest = "select * from usersecurity where main_menu=? ORDER BY menu_place, menu_position";
        $rsodest = selectMultipleData($query_rsodest, array("yes"));

        $row_rsodest = $rsodest['rowResults'];
        $totalRows_rsodest = $rsodest['totalcount'];

        if ($totalRows_rsodest > 0) {
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Data Found",
                "RESULTS" => $row_rsodest
            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => $rsodest
            );
        }


        return $results = json_encode($data);
    }
}


if (!function_exists('subSecurityLevels')) {
    function subSecurityLevels($data)
    {
        $menu_place = $data['MENUPLACE'];

        $query_rsodest_sub = "select * from usersecurity where menu_place=? and menu_position>? ORDER BY menu_position";
        $rsodest_sub = selectMultipleData($query_rsodest_sub, array($menu_place, "0"));
        $row_rsodest_sub = $rsodest_sub['rowResults'];
        $totalRows_rsodest_sub = $rsodest_sub['totalcount'];


        if ($totalRows_rsodest_sub > 0) {
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Data Found",
                "RESULTS" => $row_rsodest_sub
            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => $rsodest_sub
            );
        }


        return $results = json_encode($data);
    }
}



if(!function_exists('saveDiagnosis')){
    function saveDiagnosis($decoded){
      $diseaseid=$decoded['DISEASEID'];
      $diseasecode = $decoded['DISEASECODE'];

        $patientID = $decoded['PATIENTID'];
        $userid = $decoded['USERID'];

        //get disease details

        $getdisease = selectData("select * from diseases where DiseaseID=?",array($diseaseid));
        $rowDisease = $getdisease['rowResults'];

        $diseasename = $rowDisease['Disease'];
        

        $insertTreatment = InsertData("INSERT INTO patient_diagnosis (`patient_id`,`disease_code`,`disease_id`,`disease_name`,`user_id`)VALUES(?,?,?,?,?)",array($patientID,$diseasecode,$diseaseid,$diseasename,$userid));

        if($insertTreatment>0){
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Diagnosis Recorded Successfully"
            );
        }else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => "No Data Found ".$insertTreatment
            )
            ;
        }

        return $results = json_encode($data);


    }
}

if(!function_exists('getDiagnosisHistory')){
    function getDiagnosisHistory($decoded){

        $patientID = $decoded['PATIENTID'];

        $query_rsodest = "select * from patient_diagnosis where patient_id=? ORDER BY date_added DESC LIMIT 0,5";
        $rsodest = selectMultipleData($query_rsodest, array($patientID));

        $row_rsodest = $rsodest['rowResults'];
        $totalRows_rsodest = $rsodest['totalcount'];

        if ($totalRows_rsodest > 0) {
            $data = array(
                "STATUSCODE" => "000",
                "STATUSMSG" => "Data Found",
                "RESULTS" => $row_rsodest
            );
        } else {
            $data = array(
                "STATUSCODE" => "001",
                "STATUSMSG" => $rsodest
            );
        }


        return $results = json_encode($data);

    }
}


if (!function_exists('emailSender')) {
    function emailSender($from, $sendername, $to, $recipientname, $copy, $msg, $sub)
    {

        global $conn;
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'ssl://smtp.googlemail.com';            // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'ebenezer.rxhealthinfosystems@gmail.com';                // SMTP username   rxhealthhrportal@gmail.com       rxhealthker@gmail.com   22Kwaku_       hello@healthker.com        99Kwame@!
        $mail->Password = 'mejzdzhbfxrzdpjl';                           // SMTP password     @gun12345        lagosbiobankmail@gmail.com    lvqgcsgmkdjvdqel
        $mail->SMTPSecure = 'ssl';                             // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                     // TCP port to connect to

        $from  = "ebenezer.rxhealthinfosystems@gmail.com";

        $mail->setFrom($from, $sendername);
        $mail->addAddress($to, $recipientname);
        if ($copy != false) {
            # code...
            foreach ($copy as $key) {
                # code...
                $name = $key["name"];
                $cemail = $key["email"];
                $mail->addCC($cemail, $name);
            }
        }
        // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('ebenezer.rxhealthinfosystems@gmail.com', 'Information');

        //$mail->addBCC('bcc@example.com');

        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $sub;
        $mail->Body    = $msg;
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if (!$mail->send()) {
            // echo 'Message could not be sent.';
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
            $results = array(
                "resp_code" => "001",
                "resp_msg" => $mail->ErrorInfo
            );
        } else {
            // echo 'Message has been sent';
            $results = array(
                "resp_code" => "000",
                "resp_msg" => "Message has been sent"
            );
            $current_date = date("Y-m-d H:i:s");
            $content = filter_var($msg, FILTER_SANITIZE_STRING);
            // $query_rsodest = $conn->prepare("INSERT INTO email_log (`email`,`datetime_added`,`subject`,`content`)
            // VALUES (?,?,?,?)");
            //$query_rsodest->execute(array($to,$current_date,$sub,$content));          

        }

        return $results;
    }
}


if (!function_exists('MailTemplate')) {
    function MailTemplate($title, $content)
    {
        $message = '<html>
		<head>
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
			<style>
				.panel {
					margin-bottom: 20px;
					background-color: #fff;
					border: 1px solid #20c073;
					border-radius: 4px;
					-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
					box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
				}
		
				.panel-body {
					padding: 15px;
				}
		
				.panel-heading {
					padding: 10px 15px;
					background-color: #0F0368;
					color: #fff;
					border-bottom: 1px solid transparent;
					border-top-left-radius: 3px;
					border-top-right-radius: 3px;
				}
		
				.green {
					color:#20c073;
				}
		
				.bold {
					font-weight: bold;
				}
		
				.text-center {
					text-align: center;
				}
		
				.center-block {
					display: block;
					margin-right: auto;
					margin-left: auto;
				}
		
				.bg-dark {
					background-color: #0F0368;
					color: #abafa6;
				}
		
				.b-a {
					border: 1px solid #eaeef1;
				}
		
				.btn {
					display: inline-block;
					padding: 2px 10px;
					margin-bottom: 0;
					font-size: 13px;
					font-weight: normal;
					line-height: 1.42857143;
					text-align: center;
					white-space: nowrap;
					vertical-align: middle;
					cursor: pointer;
					-webkit-user-select: none;
					-moz-user-select: none;
					-ms-user-select: none;
					user-select: none;
					background-image: none;
					border: 1px solid transparent;
					border-radius: 4px;
					text-decoration: none;
				}
		
				.btn-success {
					color: #fff;
					background-color: #159a78;
					border-color: #158364;
				}
		
				.btn-danger {
					color: #fff;
					background-color: #D9231C;
					border-color: #BC231C;
				}
			</style>
		</head>
		
		<body style="font-family: ' . "Open Sans" . ', sans-serif; font-size: 13px; max-width: 800px">
			<div class="b-a" style="padding: 10px; width: 80%; margin: 20px auto;">
			   
				<div class="panel">
					<div class="panel-heading bold">
						' . $title . '
					</div>
					<div class="panel-body">' . $content . '
						<p>Thank You,</p>
						<p class="bold"></p>
					</div>
					<br>
					<div class="bg-dark text-center" style="padding: 10px; font-size: 12px">
						<p class="h4 text-white"> iCliq  © 2023 </p>
						
					</div>
				</div>
			</div>
		
		</body>
		</html>';

        return $message;
    }
}
