<?php
//   if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
//     throw new Exception('Request method must be POST!');
// }


if(!function_exists('BackEndController')){
    function BackEndController($received){
        //print_r($received);
    date_default_timezone_set('Africa/Accra');
    
    $data = json_decode($received,true);
        
    error_log(sprintf("APICall:RECEIVED:%s", $received ));
//print_r($data);

//echo "ddd";
    $method=filter_var($data['method'], FILTER_SANITIZE_STRING);
    $apikey=filter_var($data['api_key'], FILTER_SANITIZE_STRING);
    $passcode=filter_var($data['passcode'], FILTER_SANITIZE_STRING);
    $user=filter_var($data['user'], FILTER_SANITIZE_STRING);


    include_once("config/dbconnections.php");
   // include_once("../functions/functions.php");
    include_once("config/constants.php");
    include_once("api_functions.php");


    $authenticate=authenticateAPI($user,$passcode,$apikey,$conn);

    error_log(sprintf("APICall:AUTHENTICATE:%s", $authenticate ));


//var_dump($data);


if($authenticate>0){

    
switch ($method){

    
    case 'LISTUSERS':
        $finalResults=listUsers($data);
        break;

    case 'LISTAGENCY':
        $finalResults = listAgency($data);
        break;
    
    case 'COUNTRYLIST':
        $finalResults= getCountrylist();
        break;

    case 'SAVEAGENCY':
        $finalResults= saveAgency($data);
        break;

    case 'GETAGENCY':
        $finalResults= GetAgency($data);
        break;

    case 'UPDATEAGENCY':
        $finalResults= UpdateAgency($data);
        break;

    case 'DELETEAGENCY':    
        $finalResults= DeleteAgency($data);
        break;

    case 'GETPERMROLES':
        $finalResults = GetPermRoles();
        break;

    case 'ADDUSER':
        $finalResults = addUser($data);
        break;

    case 'GETUSER':
        $finalResults = getSingleUser($data);
        break;

    case 'UPDATEUSER':
        $finalResults = updateUser($data);
        break;

    case 'LISTEMPLOYEES':
        $finalResults = listemployees($data);
        break;

    case 'GETEMPLOYEE':
        $finalResults = getEmployee($data);
        break;

    case 'LISTAGENCYPATIENTS':
        $finalResults = listAgencyPatients($data);
        break;

    case 'ASSIGNPATIENT':
        $finalResults = assignPatient($data);
        break;

    case 'AGENCYUNASSIGNEDPATIENTS':
        $finalResults = unassignedPatients($data);
        break;

    case 'GETPATIENT':
        $finalResults = getPatient($data);
        break;
    
    case 'CHANGEPASSWORD':
        $finalResults = changePassword($data);
        break;
    
    case 'SAVECLIENT':
        $finalResults = saveClientInfo($data);
        break;

    case 'UPDATECLIENT':
        $finalResults = updateClientInfo($data);
    
    case 'ASSIGNEDPATIENTS':
        $finalResults =  assignedPatients($data);
        break;

    case 'GETALLERGYHISTORY':
        $finalResults = patientAllergies($data);
        break;

    case 'LISTALLERGY':
        $finalResults=listAllergies();
        break;
    
    case 'LISTREACTION':
        $finalResults=listReaction();
        break;

    case 'SAVEPATIENTALLERGY':
        $finalResults = saveAllergy($data);
        break;

    case 'GETPATIENTVITALS':
         $finalResults = listVitals($data);
         break;

    case 'SAVEPATIENTVITAL':
        $finalResults = saveVitals($data);
        break;

    case 'GETPATIENTNOTES':
        $finalResults = listNotes($data);
        break;

    case 'SAVENOTE':
        $finalResults = saveNote($data);
        break;

    case 'GETPATIENTDRUGS':
        $finalResults = listPatientDrugs($data);
        break;

    case 'GETITEMS':
        $finalResults = getItems($data);
        break;

    case 'GETDOSE':
        $finalResults = getDose($data);
        break;

    case 'SAVEPRESCRIPTION':
        $finalResults = savePrescription($data);
        break;

    case 'SAVETREATMENT':
        $finalResults = saveTreatment($data);
        break;

    case 'GETPATIENTTREATMENTS':
        $finalResults = listTreatments($data);
        break;

    case 'GETPATIENTGLUCOSE':
        $finalResults = listGlucose($data);
        break;

    case 'SAVEPATIENTGLUCOSE':
        $finalResults = savePatientGlucose($data);
        break;

    case 'GETPATIENTURINE':
        $finalResults = listUrine($data);
        break;

    case 'SAVEPATIENTURINE':
        $finalResults = savePatientUrine($data);
        break;

    case 'GETDISEASES':
        $finalResults = listDiseases($data);
        break;

    case 'UPDATEPATIENTSTATUS':
        $finalResults=updatePatientStatus($data);
        break;
    
    case 'GENERATEVISITNO':
       $finalResults =  generateVisitID($data);
        break;

    case 'GETPATIENTINVOICES':
        $finalResults = listEncounters($data);
        break;
        
    case 'SECURITYLEVELS':
        $finalResults = getSecurityLevelsPermissions();
        break;

    case 'SUBSECURITYLEVELS':
        $finalResults = subSecurityLevels($data);
        break;

    case 'SAVEPATIENTDIAGNOSIS':
        $finalResults = saveDiagnosis($data);
        break;

    case 'GETDIAGNOSISHISTORY':
        $finalResults = getDiagnosisHistory($data);
        break;
    
}




}else {
    $finalResults== json_encode(
        array(
            "STATUSCODE"=>"001",
            "STATUSMSG"=>"SORRY YOU DO NOT HAVE PERMISSION"
        )
    );


    
}

return $finalResults;
}
}
