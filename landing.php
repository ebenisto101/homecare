<!doctype html>
<html lang="en" dir="ltr">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Homecare</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="assets/images/favicon.ico" />
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
      <!-- Bootstrap CSS -->
      <link id="bootstrap-css" rel="stylesheet" href="assets/css/bootstrap.min.css">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="assets/css/typography.css">
      <!-- Style CSS -->
      <link rel="stylesheet" href="assets/css/style.css">
      <!-- Style-Rtl CSS -->
      <link rel="stylesheet" href="assets/css/style-rtl.css">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="assets/css/responsive.css">
       <!-- Full calendar -->
      <!-- <link href='assets/fullcalendar/core/main.css' rel='stylesheet' />
      <link href='assets/fullcalendar/daygrid/main.css' rel='stylesheet' />
      <link href='assets/fullcalendar/timegrid/main.css' rel='stylesheet' />
      <link href='assets/fullcalendar/list/main.css' rel='stylesheet' /> -->
      <link href="assets/vendors/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
      <!-- <link href="assets/css/select2.min.css" rel="stylesheet" type="text/css" /> -->
      
      
      <link href="assets/vendors/fontawesome/css/all.min.css" rel="stylesheet" type="text/css" />
	
	

      <link rel="stylesheet" href="assets/css/flatpickr.min.css">
      <link rel="stylesheet" href="assets/css/my.css">


   </head>
   <body class="sidebar-main-menu">
      <!-- loader Start -->
      <!-- <div id="loading">
         <div id="loading-center">

         </div>
      </div> -->
      <!-- loader END -->
      <!-- Wrapper Start -->
      <div class="wrapper">
         <!-- Sidebar  -->
       <?php 
            include_once("main_menu.php");
       ?>
         
         <!-- Page Content  -->
         <div id="content-page" class="content-page">
            <!-- TOP Nav Bar -->
         <div class="iq-top-navbar header-top-sticky">
            <div class="iq-navbar-custom">
               <div class="iq-sidebar-logo">
                  <div class="top-logo">
                     <a href="index.html" class="logo">
                     <img src="assets/images/logo.png" class="img-fluid" alt="">
                     <span>icliq</span>
                     </a>
                  </div>
               </div>
               <nav class="navbar navbar-expand-lg navbar-light p-0">
                  <!-- <div class="iq-search-bar">
                     <form action="#" class="searchbox">
                        <input type="text" class="text search-input" placeholder="Type here to search...">
                        <a class="search-link" href="#"><i class="ri-search-line"></i></a>
                     </form>
                  </div> -->
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" href="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="ri-menu-3-line"></i>
                  </button>
                  <div class="iq-menu-bt align-self-center">
                     <div class="wrapper-menu">
                        <div class="main-circle"><i class="ri-more-fill"></i></div>
                           <div class="hover-circle"><i class="ri-more-2-fill"></i></div>
                     </div>
                  </div>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav ms-auto navbar-list align-items-center">
                        
                        <!-- <li class="nav-item">
                           <a href="javascript:void(0);" class="rtl-switch-toogle">
                              <span class="form-check form-switch">
                                 <input class="form-check-input rtl-switch" type="checkbox" role="switch" id="rtl-switch">
                                 <span class="rtl-toggle-tooltip ltr-tooltip">Ltr</span>
                                 <span class="rtl-toggle-tooltip rtl-tooltip">Rtl</span>
                              </span>
                           </a>
                        </li>    -->
                        <li class="nav-item iq-full-screen">
                           <a href="#" class="iq-waves-effect" id="btnFullscreen"><i class="ri-fullscreen-line"></i></a>
                        </li>
                       
                      
                     </ul>
                  </div>
                  <ul class="navbar-list">
                     <li>
                        <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center">
                           <img src="assets/images/user/1.jpg" class="img-fluid rounded" alt="user">
                           <div class="caption">
                              <h6 class="mb-0 line-height"><?php echo $_SESSION['myMM_Fullname']; ?></h6>
                              <span class="font-size-12">Available</span>
                           </div>
                        </a>
                        <div class="iq-sub-dropdown iq-user-dropdown">
                           <div class="iq-card shadow-none m-0">
                              <div class="iq-card-body p-0 ">
                                 <div class="bg-primary p-3">
                                    <h5 class="mb-0 text-white line-height"><?php echo $_SESSION['myMM_Fullname']; ?></h5>
                                    <span class="text-white font-size-12">Available</span>
                                 </div>
                                 <!-- <a href="profile.html" class="iq-sub-card iq-bg-primary-hover">
                                    <div class="media align-items-center d-flex">
                                       <div class="rounded iq-card-icon iq-bg-primary">
                                          <i class="ri-file-user-line"></i>
                                       </div>
                                       <div class="media-body ms-3">
                                          <h6 class="mb-0 ">My Profile</h6>
                                          <p class="mb-0 font-size-12">View personal profile details.</p>
                                       </div>
                                    </div>
                                 </a> -->
                                 <a href="profile-edit.html" class="iq-sub-card iq-bg-primary-hover">
                                    <div class="media align-items-center d-flex">
                                       <div class="rounded iq-card-icon iq-bg-primary">
                                          <i class="ri-profile-line"></i>
                                       </div>
                                       <div class="media-body ms-3">
                                          <h6 class="mb-0 ">Change Password</h6>
                                          <p class="mb-0 font-size-12">Modify your Login Details.</p>
                                       </div>
                                    </div>
                                 </a>
                                 <!-- <a href="account-setting.html" class="iq-sub-card iq-bg-primary-hover">
                                    <div class="media align-items-center d-flex">
                                       <div class="rounded iq-card-icon iq-bg-primary">
                                          <i class="ri-account-box-line"></i>
                                       </div>
                                       <div class="media-body ms-3">
                                          <h6 class="mb-0 ">Account settings</h6>
                                          <p class="mb-0 font-size-12">Manage your account parameters.</p>
                                       </div>
                                    </div>
                                 </a> -->
                                 <!-- <a href="privacy-setting.html" class="iq-sub-card iq-bg-primary-hover">
                                    <div class="media align-items-center d-flex">
                                       <div class="rounded iq-card-icon iq-bg-primary">
                                          <i class="ri-lock-line"></i>
                                       </div>
                                       <div class="media-body ms-3">
                                          <h6 class="mb-0 ">Privacy Settings</h6>
                                          <p class="mb-0 font-size-12">Control your privacy parameters.</p>
                                       </div>
                                    </div>
                                 </a> -->
                                 <div class="d-inline-block w-100 text-center p-3">
                                    <a class="bg-primary iq-sign-btn" href="<?php echo BASE_URL ?>signout.php" role="button">Sign out<i class="ri-login-box-line ms-2"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </nav>

            </div>
         </div>
         <!-- TOP Nav Bar END -->
            <div class="container-fluid" id="kt_app_main">
             <?php include_once('dashboard.php'); ?>
               
            </div>
            <!-- Footer -->
      <footer class="bg-white iq-footer">
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-6">
                  <!-- <ul class="list-inline mb-0">
                     <li class="list-inline-item"><a href="privacy-policy.html">Privacy Policy</a></li>
                     <li class="list-inline-item"><a href="terms-of-service.html">Terms of Use</a></li>
                  </ul> -->
               </div>
               <div class="col-lg-6 text-end">
                  Copyright 2024 <a href="#">iCliq Info Systems</a> All Rights Reserved.
               </div>
            </div>
         </div>
      </footer>
      <!-- Footer END -->
         </div>
      </div>
      <style id="loading_container_style">
		#loading_container {
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			z-index: 999999999;
			display: none;
		}

		#loading_container>*,
		#loading_container {
			box-sizing: border-box;
		}

		#loading_container>div {
			width: 100%;
			height: 100%;
			display: flex;
			display: -webkit-flex;
			align-items: center;
			justify-content: center;
		}

		#loading_container>div>svg>path {
			fill: #007aff;
		}
	</style>
	<div id="loading_container">
		<div>
      <div class="html-embed-2 w-embed"><style>
svg path,
svg rect{
  fill: #FF6700;
}
</style> 
  <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
  <path fill="#000" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
    <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 25 25" to="360 25 25" dur="0.6s" repeatCount="indefinite"></animateTransform>
    </path>
  </svg></div>

			<!--			<svg style="vertical-align: middle; " version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px" height="80px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">-->
			<!--				<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">-->
			<!--					<animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 25 25" to="360 25 25" dur="0.6s" repeatCount="indefinite" />-->
			<!--				</path>-->
			<!--			</svg>-->
		</div>
	</div>
      <!-- Wrapper END -->
      
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/popper.min.js"></script>
      <script src="assets/js/bootstrap.min.js"></script>
      <!-- Appear JavaScript -->
      <script src="assets/js/jquery.appear.js"></script>
      <!-- Countdown JavaScript -->
      <script src="assets/js/countdown.min.js"></script>
      <!-- Counterup JavaScript -->
      <script src="assets/js/waypoints.min.js"></script>
      <script src="assets/js/jquery.counterup.min.js"></script>
      <!-- Wow JavaScript -->
      <script src="assets/js/wow.min.js"></script>
      <!-- Apexcharts JavaScript -->
      <script src="assets/js/apexcharts.js"></script>
      <!-- Swiper Slider JavaScript -->
      <script src="assets/js/swiper-bundle.min.js"></script>
      <!-- Select2 JavaScript -->
      <script src="assets/js/select2.min.js"></script>
     
      <!-- Owl Carousel JavaScript -->
      <script src="assets/js/owl.carousel.min.js"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="assets/js/jquery.magnific-popup.min.js"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="assets/js/smooth-scrollbar.js"></script>
      <!-- lottie JavaScript -->
      <script src="assets/js/lottie.js"></script>
      <!-- am core JavaScript -->
      <script src="assets/js/core.js"></script>
      <!-- am charts JavaScript -->
      <script src="assets/js/amcharts.js"></script>
      <!-- am animated JavaScript -->
      <script src="assets/js/animated.js"></script>
      <!-- am kelly JavaScript -->
      <script src="assets/js/kelly.js"></script>
      <!-- Flatpicker Js -->
      <script src="assets/js/flatpickr.js"></script>
      <!-- Chart Custom JavaScript -->
      <script src="assets/js/chart-custom.js"></script>
      <!-- Custom JavaScript -->
      <script src="assets/js/custom.js"></script>
      <script src="assets/js/myJs.js"></script>
      <script src="assets/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
      <script src="ckeditor5/ckeditor.js?v=<?php echo rand(); ?>" type="text/javascript"></script>
      
	

      <?php if ($_SESSION['chgpassword']=="1") {
			$showModal = "false";
		}?>

<div class="modal fade bd-example-modal-lg" tabindex="-1" aria-hidden="true" id="change_expiredpass" data-keyboard="false" data-bs-backdrop="static">
                              <div class="modal-dialog modal-lg">
                                 <div class="modal-content" id="test_wrapper_results2">
                                    
                                
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">First Time Login - Reset Password</h4>
				</div>
				<form name="form" method="post">
					<div class="modal-body">
						<!-- <span style="color:red;font-weight:bold">Your Password has expired. Please fill form below to reset </span><br><br><br> -->
						<div class="col">
							<label for="">Old Password</label>
							<input id="exp_pass" type="password" name="exp_pass" class="form-control" type="text">
							<br>
							<label for="">New Password</label>
							<input id="new_exp_pass" type="password" name="new_exp_pass" class="form-control" type="text">
							<br>
							<label for="">Confirm Password</label>
							<input id="confirm_exp_pass" type="password" name="confirm_exp_pass" class="form-control" type="text">
							<hr>
							<!-- <span>
								<p><b>Password Must Contain</b></p>
								<p style="color:red">1. At least one letter<br>2. At least one capital letter<br>3. At least one number<br>4. At least one special characters including ~!@#$%^&*_-+=`|\(){}[]:;"'<>,.?/<br>5. Be at least 10 characters</p>
							</span> -->
						</div>
					</div>
					<div class="modal-footer">
						<div>
							<button type="button" onclick="processInfo('change_exp_pass','<?php echo $_SESSION['myMM_Userid'] ?>');return false;" class="btn btn-info">Change Password</button>
							
						</div>
					</div>
				</form>
			
                                   
                                 </div>
                              </div>
                           </div>
   </body>
</html>
<?php
if (!empty($showModal)) {
		// CALL MODAL HERE
		echo '<script type="text/javascript">
			$(document).ready(function(){
				$("#change_expiredpass").modal("show");
			});
		</script>';
	}
	?>
<script>

$(document).ready(function() {
$('.js-example-basic-single').select2();
});

</script>


   
