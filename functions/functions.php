<?php

require_once (dirname(__DIR__). '/backend/index.php');

//error_reporting(0);

if (!function_exists("APICall")) {
    function APICall($a)
    {
        //print_r($a);
        //echo API_LINK;
        error_log(sprintf("APICall:URL:%s", $url));
        
        $payload = json_encode($a);
        //print_r($payload);
        $result = BackEndController($payload);
        //print_r($result);
        error_log(sprintf("APICallRESULTS:RESULT:%s", $result));
        return $result;
    }
}



// if (!function_exists("APICall")) {
//     function APICall($a, $url = "",$curltimeout='5')
//     {
//         //echo API_LINK;

//         if ($url == "") {
//             $url = API_LINK;
//         }
//         error_log(sprintf("APICall:URL:%s", $url));
//         $ch = curl_init($url);
//         # Setup request to send json via POST.
//         $payload = json_encode($a);
//         error_log(sprintf("APICall:PAYLOAD:%s", $payload));
//         curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//         # Return response instead of printing.
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//         curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $curltimeout); 
//         curl_setopt($ch, CURLOPT_TIMEOUT, $curltimeout);
//         # Send request.

//         $result = curl_exec($ch);

//         error_log(sprintf("APICall:RESULT:%s", $result));
      

//         if (curl_errno($ch)) {
//             $error = curl_error($ch);
//             error_log(sprintf("APICall:ERROR:%s", $error));
//             return false;
//         }

//         return $result;
//         curl_close($ch);
//     }
// }




if (!function_exists("getmonth")) {
    function getmonth($month)
    {
        $array = array(1 => "January", 2 => "Feburary", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
        $monthname = $array[$month];

        return $monthname;
    }
}



if (!function_exists('selectData')) {
    function selectData($sql, $data = NULL)
    {
        global $conn;

        error_log(sprintf("APICall:SELECT:%s", $sql));

        $query = $conn->prepare($sql);
        $query->execute($data);
        $rowResults = $query->fetch(PDO::FETCH_ASSOC);
        $totalrows = $query->rowCount();

        //error_log(vsprintf("APICall:SELECTRESULTS1:%s",$rowResults));

        $results = array(
            "totalcount" => $totalrows,
            "rowResults" => $rowResults
        );

        //error_log(sprintf("APICall:SELECTRESULTS:%s", $rowResults ));

        return $results;
    }
}


if (!function_exists('selectMultipleData')) {
    function selectMultipleData($sql, $data = NULL)
    {
        global $conn;

        error_log(sprintf("APICall:SELECTMULT:%s", $sql));

        try {
            $query = $conn->prepare($sql);
            $query->execute($data);
            $rowResults = $query->fetchAll(PDO::FETCH_ASSOC);
            $totalrows = $query->rowCount();

           // error_log(vsprintf("APICall:SELECTMULRESULTS:",$query));

            $results = array(
                "totalcount" => $totalrows,
                "rowResults" => $rowResults
            );

            return $results;
        } catch (PDOException $e) {
            return "Selection Failed" . $e->getMessage();
        }
    }
}



if (!function_exists('InsertData')) {
    function InsertData($sql, $data = NULL)
    {
        global $conn;

         error_log(sprintf("APICall:INSERT:%s", $sql ));

        try {
            $query = $conn->prepare($sql);
            $query->execute($data);
            $lastID = $conn->lastInsertId();

            return $lastID;
        } catch (PDOException $e) { //echo "Insertion failed: " . $e->getMessage(); 
            return "Insertion Failed" . $e->getMessage();
            error_log(sprintf("APICall:INSERTRESULT:%s", $e->getMessage() ));
        }

        

        // return $lastID;


    }
}


if (!function_exists('UpdateData')) {
    function UpdateData($sql, $data = NULL)
    {
        global $conn;

        //error_log(sprintf("APICall:UPDATE:%s", $sql ));

        $query = $conn->prepare($sql);
        $query->execute($data);

        if ($query == false) {
            return $conn->errorInfo();
        } else {
            return "SUCCESS";
        }

        // error_log(sprintf("APICall:INSERTRESULT:%s", $conn->errorInfo()));


    }
}


if (!function_exists('deleteData')) {
    function deleteData($sql, $data = NULL)
    {
        global $conn;

        $query = $conn->prepare($sql);
        $query->execute($data);
        $rowCount = $query->rowCount();

        if ($rowCount < 0) {
            return $conn->errorInfo();
        } else {
            return "SUCCESS";
        }
    }
}



if (!function_exists("timeAgo")) {
    function timeAgo($timestamp)
    {
        global $conn;
        $datetime1 = new DateTime("now");
        $datetime2 = date_create($timestamp);
        $diff = date_diff($datetime1, $datetime2);
        //print_r($diff);
        $timemsg = '';
        if ($diff->y > 0) {
            $timemsg = $diff->y . ' year' . ($diff->y > 1 ? "s" : '');
            
        } else if ($diff->m > 0) {
         $timemsg = $diff->m . ' month' . ($diff->m > 1 ? "s" : '');
        } else if ($diff->d > 0) {
            $timemsg = $diff->d . ' day' . ($diff->d > 1 ? "s" : '');
        } else if ($diff->h > 0) {
            $timemsg = $diff->h . ' hour' . ($diff->h > 1 ? "s" : '');
        } else if ($diff->i > 0) {
            $timemsg = $diff->i . ' minute' . ($diff->i > 1 ? "s" : '');
        } else if ($diff->s > 0) {
            $timemsg = $diff->s . ' second' . ($diff->s > 1 ? "s" : '');
        }
        $timemsg = $timemsg;
        return $timemsg;
    }
}





function getUserPermid($Model,$userid) {

  $query_rsodest122 = "select * from usersecuritypermissions where user_id=? and securitylevel_id=?";
  $rsodest122 = selectData($query_rsodest122,array($userid,$Model));
  $row_rsodest122 = $rsodest122['totalcount'];

	if($row_rsodest122>0) {
		
			return "yes";
		
	} else {
        return "No";
    }
}



if(!function_exists('verifyRecaptcha')){
    function verifyRecaptcha($data){
        

        // Put secret key here, which we get from google console 
        //$secret_key = 'your_secret_key'; 
      
        // Hitting request to the URL, Google will 
        // respond with success or error scenario 
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.RECAPTCHA_PRIVATE_KEY.'&response='.$data; 
      
        // Making request to verify captcha 
        $response = file_get_contents($url); 
      
        // Response return by google is in JSON format, so we have to parse that json 
        $response = json_decode($response); 
        //print_r($response);
      //echo $data;
     //exit;
        // Checking, if response is true or not 
        if ($response->success == true) { 
            return "verified";
        } else { 
            return "not-verified";
        } 
    }

}


if (!function_exists("getUser")) {
    function getUser($userid)
    {


        $user_query = "SELECT * FROM users_accounts where id=?";
        $userData = array($userid);

        $getUser = selectData($user_query, $userData);
        $userResults = $getUser['rowResults'];

        if ($getUser['totalcount'] > 0) {
            $fullname = $userResults['surname'];
        } else {
            $fullname = "N/A";
        }


        return $fullname;
    }
}

if(!function_exists('calculateBMI')){
    function calculateBMI($mass,$height){

        $bmi = $mass/($height * $height);

        if ($bmi <= 18.5) {
            $output = "UNDERWEIGHT";

          } else if ($bmi > 18.5 AND $bmi<=24.9 ) {
            $output = "NORMAL WEIGHT";

          } else if ($bmi > 24.9 AND $bmi<=29.9) {
            $output = "OVERWEIGHT";

          } else if ($bmi > 30.0) {
            $output = "OBESE";
          }

          return $bmi."@@".$output;
    }
}


function getUserPermnew($Model,$menutype,$userid){
    //echo $userid;
   // get menu id
   //echo "SELECT * FROM securitylevel where menu_name='$Model'";

   global $conn;
   $get_menu = $conn->query("SELECT * FROM usersecurity where menu_name='$Model'");
   $row_menu = $get_menu->fetch(PDO::FETCH_ASSOC);
   $menu_id= $row_menu['id'];

   //CHECK IF USER HAS THAT PERMISION
   //echo "select * from securitypermissions where securitylevel_id='$menu_id' and user_id='$userid'";
   $query_rsodest1 = $conn->prepare("select * from usersecuritypermissions where securitylevel_id=? and user_id=?");
   $query_rsodest1->execute(array($menu_id,$userid));
   $TotalMenuCount = $query_rsodest1->rowCount();

   if($TotalMenuCount>0){
       $menu_status = 'yes';
   }else{
       $menu_status= 'no';
   }
   //echo $menu_status;
   return $menu_status;

}