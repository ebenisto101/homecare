<?php
include_once("./includes/includes.php");

if (!isset($_SESSION)) {
    session_start();
}

$modalType = $_POST['act'];
$id = $_POST['id'];

//GET COUNTRYLIST
$data = array(
    "method" => "COUNTRYLIST",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD
);
$get_countrylist = APICall($data);
$get_country_json = json_decode($get_countrylist, "true");


if ($modalType == "CreateAgency") { ?>
    <!--begin::Modal header-->

    <!--begin::Modal header-->
    <div class="modal-header">
        <!--begin::Modal title-->
        <div class="modal-title">Add Agency</div>
        <!--end::Modal title-->
        <!--begin::Close-->
        <div id="kt_modal_add_customer_close" class="btn btn-icon btn-sm btn-active-icon-primary">
            <i class="ki-duotone ki-cross fs-1">
                <span class="path1"></span>
                <span class="path2"></span>
            </i>
        </div>
        <!--end::Close-->
    </div>
    <!--end::Modal header-->
    <!--begin::Modal body-->
    <div class="modal-body py-10 px-lg-17">

        <form id="" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
            <!--begin::Scroll-->
            <div class="" style="max-height: 800px; overflow-y:scroll">
                <div class="form-group">
                    <label for="Name">Agency Name</label>
                    <input id="name" name="name" type="text" class="form-control my-2" placeholder="name of agency">

                </div>

                <div class="form-group">
                    <label for="email">Contact Info</label>
                    <div class="row">
                        <div class="col">
                            <input type="text" id="tel" name="tel" class="form-control my-2" placeholder="Telephone Number">
                        </div>
                        <div class="col">
                            <input type="email" id="email" name="email" class="form-control my-2" placeholder="email:example@domain.com">
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label for="email">Address Info</label>
                    <div class="row">
                        <div class="col">

                            <select name="country" aria-label="Select a Country" data-placeholder="Select a Country..." class="form-select form-select-solid" tabindex="-1" aria-hidden="true" data-kt-initialized="1" id='country'>
                                <option value="">Select a Country...</option>
                                <?php foreach ($get_country_json['RESULTS'] as $country) { ?>
                                    <option value="<?php echo $country['country'] ?>"><?php echo $country['country'] ?></option>
                                <?php } ?>
                            </select>

                        </div>
                        <div class="col">
                            <input type="text" id="state" name="state" class="form-control my-2" placeholder="State / Region">

                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <!-- <label for="Name">Business Address</label> -->
                    <div class="row">
                        <div class="col">
                            <input type="text" id="city" name="city" class="form-control my-2" placeholder="city/town">
                        </div>
                        <div class="col">
                            <input id="address" name="address" type="text" class="form-control my-2" placeholder="Address">
                        </div>
                    </div>

                </div>
                <!--end::Scroll-->
                <!--begin::Actions-->
                <div class="text-center pt-15">
                    <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal">Cancel</button>
                    <input type="button" class="btn btn-primary" value="Submit" onclick="saveInfo('SaveAgency','','');return false;">

                </div>
                <!--end::Actions-->
        </form>
    </div>
    <!--end::Modal body-->
    <!--begin::Modal footer-->

    <!--end::Modal footer-->

<?php
}



if ($modalType == "editAgency") {
    //get existing data


    $data1 = array(
        "method" => "GETAGENCY",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "AGENCYID" => $id
    );
    $get_agency = APICall($data1);
    $get_agency_json = json_decode($get_agency, "true");
    $agency = $get_agency_json['RESULTS'];

?>
    <!--begin::Modal header-->
    <form class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST" id="kt_modal_add_customer_form">
        <!--begin::Modal header-->
        <div class="modal-header" id="kt_modal_add_customer_header">
            <!--begin::Modal title-->
            <h3 class="modal-title">Update Agency</h3>
            <!--end::Modal title-->
            <!--begin::Close-->
            <div id="kt_modal_add_customer_close" class="btn btn-icon btn-sm btn-active-icon-primary">
                <i class="ki-duotone ki-cross fs-1">
                    <span class="path1"></span>
                    <span class="path2"></span>
                </i>
            </div>
            <!--end::Close-->
        </div>
        <!--end::Modal header-->
        <!--begin::Modal body-->
        <div class="modal-body py-10 px-lg-17">


            <form id="" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <!--begin::Scroll-->
                <div class="" style="max-height: 800px; overflow-y:scroll">
                    <div class="form-group">
                        <label for="Name">Agency Name</label>
                        <input id="name" name="name" type="text" value="<?php echo $agency['agency_name']; ?>" class="form-control my-2" placeholder="name of agency">

                    </div>

                    <div class="form-group">
                        <label for="email">Contact Info</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" value="<?php echo $agency['tel']; ?>" id="tel" name="tel" class="form-control my-2" placeholder="Telephone Number">
                            </div>
                            <div class="col">
                                <input type="email" value="<?php echo $agency['email']; ?>" id="email" name="email" class="form-control my-2" placeholder="email:example@domain.com">
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="email">Address Info</label>
                        <div class="row">
                            <div class="col">

                                <select name="country" aria-label="Select a Country" data-placeholder="Select a Country..." class="form-select form-select-solid" tabindex="-1" aria-hidden="true" data-kt-initialized="1" id='country'>
                                    <option value="">Select a Country...</option>
                                    <?php foreach ($get_country_json['RESULTS'] as $country) { ?>
                                        <option <?php if ($agency['country'] == $country['country']) { ?>selected <?php } ?> value="<?php echo $country['country'] ?>"><?php echo $country['country'] ?></option>
                                    <?php } ?>
                                </select>

                            </div>
                            <div class="col">
                                <input type="text" value="<?php echo $agency['region']; ?>" id="state" name="state" class="form-control my-2" placeholder="State / Region">

                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <!-- <label for="Name">Business Address</label> -->
                        <div class="row">
                            <div class="col">
                                <input type="text" id="city" value="<?php echo $agency['agency_location']; ?>" name="city" class="form-control my-2" placeholder="city/town">
                            </div>
                            <div class="col">
                                <input id="address" name="address" value="<?php echo $agency['agency_address']; ?>" type="text" class="form-control my-2" placeholder="Address">
                            </div>
                        </div>

                    </div>
                    <!--end::Scroll-->
                    <!--begin::Actions-->
                    <div class="text-center pt-15">
                        <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal">Cancel</button>
                        <input type="button" class="btn btn-primary" value="Submit" onclick="saveInfo('UpdateAgency','<?php echo $id; ?>','');return false;">

                    </div>
                    <!--end::Actions-->
            </form>

        </div>

    <?php
}

if ($modalType == "addUser") {
    //get agencies
    $data = array(
        "method" => "LISTAGENCY",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD
    );
    $get_agencyList = APICall($data);
    $get_agencyList_json = json_decode($get_agencyList, "true");


    // get permission Roles
    $data1 = array(
        "method" => "GETPERMROLES",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD
    );
    $get_permList = APICall($data1);
    $get_permList_json = json_decode($get_permList, "true");
    ?>
        <div class="modal-header">
            <!--begin::Modal title-->
            <h3 class="modal-title">Add User</h3>
            <!--end::Modal title-->
            <!--begin::Close-->
            <div class="btn btn-icon btn-sm btn-active-icon-primary" data-kt-users-modal-action="close">
                <i class="ki-duotone ki-cross fs-1">
                    <span class="path1"></span>
                    <span class="path2"></span>
                </i>
            </div>
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px mx-5 my-7">
            <!--begin::Form-->
            <form id="" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <!--begin::Scroll-->
                <div class="" style="max-height: 800px; overflow-y:scroll">
                    <div class="form-group">
                        <label for="email">Full Name:</label>
                        <div class="row">
                            <div class="col">
                                <input id="surname" name="surname" type="text" class="form-control my-2" placeholder="Surname">
                            </div>
                            <div class="col">
                                <input id="firstnames" name="firstnames" type="text" class="form-control my-2" placeholder="First Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Contact Info</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" id="tel" name="tel" class="form-control my-2" placeholder="Telephone Number">
                            </div>
                            <div class="col">
                                <input type="email" id="user_email" name="user_email" class="form-control my-2" placeholder="email:example@domain.com">
                            </div>
                        </div>

                    </div>


                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Agency</label>
                        <select name="agency" id="agency" class="form-select my-2" id="exampleFormControlSelect1">
                            <option selected="" disabled="">Select Agency</option>
                            <?php foreach ($get_agencyList_json['RESULTS'] as $agency) { ?>
                                <option value="<?php echo $agency['id'] ?>"><?php echo $agency['agency_name'] ?></option>
                            <?php } ?>

                        </select>
                    </div>

                    <div class="mb-7">
                        <!--begin::Label-->
                        <label class="required fs-6 fw-bold mb-5">Role</label>

                        <?php
                        if ($get_permList_json['TOTALROWS'] > 0) {
                            foreach ($get_permList_json['RESULTS'] as $perm) { ?>
                                <div class="d-flex fv-row">
                                    <!--begin::Radio-->
                                    <div class="form-check form-check-custom form-check-solid">
                                        <!--begin::Input-->
                                        <input class="form-check-input me-3" id="user_role" name="user_role" type="radio" value="<?php echo $perm['id']; ?>" id="kt_modal_update_role_option_0">
                                        <!--end::Input-->
                                        <!--begin::Label-->
                                        <label class="form-check-label" for="kt_modal_update_role_option_0">
                                            <div class="fw-bold text-gray-800"><?php echo $perm['security_role']; ?></div>
                                            <div class="text-gray-600"><?php echo $perm['description']; ?></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="separator-dashed my-5a"></div>

                        <?php }
                        } ?>
                    </div>

                </div>
                <!--end::Scroll-->
                <!--begin::Actions-->
                <div class="text-center pt-15">
                    <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal">Cancel</button>
                    <input type="button" class="btn btn-primary" value="Submit" onclick="saveInfo('UserAdd','','');return false;">

                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>
    <?php }



if ($modalType == "editUser") {

    //get agencies
    $data = array(
        "method" => "LISTAGENCY",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD
    );
    $get_agencyList = APICall($data);
    $get_agencyList_json = json_decode($get_agencyList, "true");

    //get user
    $data2 = array(
        "method" => "GETUSER",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "USERID" => $id
    );
    $get_user = APICall($data2);
    $get_user_json = json_decode($get_user, "true");
    $user = $get_user_json['RESULTS'];
//print_r(json_encode($data2));
//print_r($get_user_json);


   // get permission Roles
    $data1 = array(
        "method" => "GETPERMROLES",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD
    );
    $get_permList = APICall($data1);
    $get_permList_json = json_decode($get_permList, "true");
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <h2 class="fw-bold">Edit User</h2>
            <!--end::Modal title-->
            <!--begin::Close-->
            <div class="btn btn-icon btn-sm btn-active-icon-primary" data-kt-users-modal-action="close">
                <i class="ki-duotone ki-cross fs-1">
                    <span class="path1"></span>
                    <span class="path2"></span>
                </i>
            </div>
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->
            <form id="" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <!--begin::Scroll-->

                <div class="" style="max-height: 800px; overflow-y:scroll">
                    <div class="form-group">
                        <label for="email">Full Name:</label>
                        <div class="row">
                            <div class="col">
                                <input id="surname" name="surname" value="<?php echo $user['surname']; ?>" type="text" class="form-control my-2" placeholder="Surname">
                            </div>
                            <div class="col">
                                <input id="firstnames" name="firstnames" value="<?php echo $user['firstnames']; ?>" type="text" class="form-control my-2" placeholder="First Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Contact Info</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" id="tel" name="tel" value="<?php echo $user['tel']; ?>" class="form-control my-2" placeholder="Telephone Number">
                            </div>
                            <div class="col">
                                <input type="email" id="user_email" name="user_email" value="<?php echo $user['email']; ?>" class="form-control my-2" placeholder="email:example@domain.com">
                            </div>
                        </div>

                    </div>


                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Agency</label>
                        <select name="agency" id="agency" class="form-select my-2" id="exampleFormControlSelect1">
                            <option selected="" disabled="">Select Agency</option>
                            <?php foreach ($get_agencyList_json['RESULTS'] as $agency) { ?>
                                <option <?php if ($agency['id'] == $user['agency_id']) { ?> selected <?php } ?> value="<?php echo $agency['id'] ?>"><?php echo $agency['agency_name'] ?></option>
                            <?php } ?>

                        </select>
                    </div>

                    <div class="mb-7">
                        <!--begin::Label-->
                        <label class="required fw-semibold fs-7 mb-5">Role</label>

                        <?php
                        if ($get_permList_json['TOTALROWS'] > 0) {
                            foreach ($get_permList_json['RESULTS'] as $perm) { ?>
                                <div class="d-flex fv-row">
                                    <!--begin::Radio-->
                                    <div class="form-check form-check-custom form-check-solid">
                                        <!--begin::Input-->
                                        <input class="form-check-input me-3" name="user_role" type="radio" <?php if ($perm['id'] == $user['perm_role_id']) { ?>checked <?php } ?> value="<?php echo $perm['id']; ?>" id="kt_modal_update_role_option_0">
                                        <!--end::Input-->
                                        <!--begin::Label-->
                                        <label class="form-check-label" for="kt_modal_update_role_option_0">
                                            <div class="fw-bold text-gray-800"><?php echo $perm['security_role']; ?></div>
                                            <div class="text-gray-600"><?php echo $perm['description']; ?></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="separator separator-dashed my-5a"></div>

                        <?php }
                        } ?>
                    </div>

                </div>


                <!--end::Scroll-->
                <!--begin::Actions-->
                <div class="text-center pt-15">
                    <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal">Cancel</button>
                    <input type="button" class="btn btn-primary" value="Update User" onclick="saveInfo('SaveUserEdit','<?php echo $id; ?>','');return false;">

                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>

    <?php }


if ($modalType == "addEmployee") {
    //get agencies
    $agency = $_SESSION['agencyID'];


    // get permission Roles
    $data1 = array(
        "method" => "GETPERMROLES",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD
    );
    $get_permList = APICall($data1);
    $get_permList_json = json_decode($get_permList, "true");
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <h2 class="fw-bold">Add Employee</h2>
            <!--end::Modal title-->
            <!--begin::Close-->
            <div class="btn btn-icon btn-sm btn-active-icon-primary" data-kt-users-modal-action="close">
                <i class="ki-duotone ki-cross fs-1">
                    <span class="path1"></span>
                    <span class="path2"></span>
                </i>
            </div>
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->


            <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <div class="" style="max-height: 800px; overflow-y:scroll">
                    <div class="form-group">
                        <label for="email">Full Name:</label>
                        <div class="row">
                            <div class="col">
                                <input id="surname" name="surname" type="text" class="form-control my-2" placeholder="Surname">
                            </div>
                            <div class="col">
                                <input id="firstnames" name="firstnames" type="text" class="form-control my-2" placeholder="First Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Contact Info</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" id="tel" name="tel" class="form-control my-2" placeholder="Telephone Number">
                            </div>
                            <div class="col">
                                <input type="email" id="user_email" name="user_email" class="form-control my-2" placeholder="email:example@domain.com">
                                <input type="hidden" id="agency" name='agency' value="<?php echo $agency ?>">
                            </div>
                        </div>

                    </div>
                    <div class="mb-7">
                        <!--begin::Label-->
                        <label class="required fs-6 fw-bold mb-5">Role</label>

                        <?php
                        if ($get_permList_json['TOTALROWS'] > 0) {
                            foreach ($get_permList_json['RESULTS'] as $perm) { ?>
                                <div class="d-flex fv-row">
                                    <!--begin::Radio-->
                                    <div class="form-check form-check-custom form-check-solid">
                                        <!--begin::Input-->
                                        <input class="form-check-input me-3" id="user_role" name="user_role" type="radio" value="<?php echo $perm['id']; ?>" id="kt_modal_update_role_option_0">
                                        <!--end::Input-->
                                        <!--begin::Label-->
                                        <label class="form-check-label" for="kt_modal_update_role_option_0">
                                            <div class="fw-bold text-gray-800"><?php echo $perm['security_role']; ?></div>
                                            <div class="text-gray-600"><?php echo $perm['description']; ?></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="separator-dashed my-5a"></div>

                        <?php }
                        } ?>
                    </div>

                </div>

                <div class="text-center pt-15">
                    <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal">Cancel</button>
                    <input type="button" class="btn btn-primary" value="Submit" onclick="saveInfo('UserAdd','','');return false;">

                </div>
            </form>
            <!--end::Form-->
        </div>
    <?php }



if ($modalType == "editEmployee") {
    $agency = $_SESSION['agencyID'];

    //get user
    $data2 = array(
        "method" => "GETUSER",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "USERID" => $id
    );
    //print_r(json_encode($data2));
    $get_user = APICall($data2);
    $get_user_json = json_decode($get_user, "true");
    $user = $get_user_json['RESULTS'];
//print_r($user);

    // get permission Roles
    $data1 = array(
        "method" => "GETPERMROLES",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD
    );
    $get_permList = APICall($data1);
    $get_permList_json = json_decode($get_permList, "true");
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <h2 class="fw-bold">Edit User</h2>
            <!--end::Modal title-->
            <!--begin::Close-->
            <div class="btn btn-icon btn-sm btn-active-icon-primary" data-kt-users-modal-action="close">
                <i class="ki-duotone ki-cross fs-1">
                    <span class="path1"></span>
                    <span class="path2"></span>
                </i>
            </div>
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->

            <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <div class="" style="max-height: 800px; overflow-y:scroll">
                    <div class="form-group">
                        <label for="email">Full Name:</label>
                        <div class="row">
                            <div class="col">
                                <input id="surname" name="surname" value="<?php echo $user['surname']; ?>" type="text" class="form-control my-2" placeholder="Surname">
                            </div>
                            <div class="col">
                                <input id="firstnames" name="firstnames" value="<?php echo $user['firstnames']; ?>" type="text" class="form-control my-2" placeholder="First Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Contact Info</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" id="tel" name="tel" value="<?php echo $user['tel']; ?>" class="form-control my-2" placeholder="Telephone Number">
                            </div>
                            <div class="col">
                                <input type="email" id="user_email" value="<?php echo $user['email']; ?>" name="user_email" class="form-control my-2" placeholder="email:example@domain.com">
                                <input type="hidden" id="agency" name='agency' value="<?php echo $agency ?>">
                            </div>
                        </div>

                    </div>
                    <div class="mb-7">
                        <!--begin::Label-->
                        <label class="required fs-6 fw-bold mb-5">Role</label>

                        <?php
                        if ($get_permList_json['TOTALROWS'] > 0) {
                            foreach ($get_permList_json['RESULTS'] as $perm) { ?>
                                <div class="d-flex fv-row">
                                    <!--begin::Radio-->
                                    <div class="form-check form-check-custom form-check-solid">
                                        <!--begin::Input-->
                                        <input class="form-check-input me-3" id="user_role" name="user_role" type="radio" value="<?php echo $perm['id']; ?>" <?php if ($perm['id'] == $user['perm_role_id']) { ?>checked <?php } ?> id="kt_modal_update_role_option_0">
                                        <!--end::Input-->
                                        <!--begin::Label-->
                                        <label class="form-check-label" for="kt_modal_update_role_option_0">
                                            <div class="fw-bold text-gray-800"><?php echo $perm['security_role']; ?></div>
                                            <div class="text-gray-600"><?php echo $perm['description']; ?></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="separator-dashed my-5a"></div>

                        <?php }
                        } ?>
                    </div>

                </div>

                <div class="text-center pt-15">
                    <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal">Cancel</button>
                    <input type="button" class="btn btn-primary" value="Update" onclick="saveInfo('SaveUserEdit','<?php echo $id; ?>','');return false;">

                </div>
            </form>
            <!--end::Form-->
        </div>

    <?php }


if ($modalType == "addPatient") {

    $agency = $_SESSION['agencyID'];
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <div class="modal-title">
                <h4>Add New Patient</h4>
            </div>
            <!--end::Modal title-->
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->
            <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <!--begin::Scroll-->
                <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">
                    <div class="form-group">
                        <label for="email">Full Name:</label>
                        <div class="row">
                            <div class="col">
                                <input id="surname" name="surname" type="text" class="form-control my-2" placeholder="Surname">
                            </div>
                            <div class="col">
                                <input id="firstnames" name="firstnames" type="text" class="form-control my-2" placeholder="First Name">
                            </div>
                            <div class="col">
                                <input id="firstnames" name="othernames" type="text" class="form-control my-2" placeholder="Other Name">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="email">Date Of Birth:</label>
                                <input id="dateOfBirth" name="dateOfBirth" type="date" class="form-control my-2" placeholder="Date of Birth">
                            </div>
                            <div class="col">
                                <label for="email">Gender:</label>
                                <select name="gender" aria-label="Select Gender" data-placeholder="Select a Gender..." class="form-control my-2" tabindex="-1" aria-hidden="true" data-kt-initialized="1" id='gender'>
                                    <option value="">Select Gender...</option>
                                    <option value="Female">Female</option>
                                    <option value="Male">Male</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="email">Nationality:</label>
                                <select name="nationality" aria-label="Select a Country" data-placeholder="Select a Country..." class="form-control my-2" aria-hidden="true" data-kt-initialized="1" id='nationality'>
                                    <option value="">Select a Country...</option>
                                    <?php foreach ($get_country_json['RESULTS'] as $country) { ?>
                                        <option value="<?php echo $country['country'] ?>"><?php echo $country['country'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="tel">Telephone:</label>
                                <input id="tel" name="tel" type="text" class="form-control my-2" placeholder="Telephone number">
                            </div>
                            <div class="col">
                                <label for="email">Email:</label>
                                <input id="email" name="email" type="email" class="form-control my-2" placeholder="Email">
                            </div>
                            <div class="col">
                                <label for="residential Address">Residential Address:</label>
                                <input id="residential_address" name="residential_address" type="text" class="form-control my-2" placeholder="Residential Address">
                            </div>
                        </div>
                    </div>
                    <hr>

                    <label>
                        <h4>Family Relative Details</h4>
                    </label>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="tel">Name:</label>
                                <input id="relative_name" name="relative_name" type="text" class="form-control my-2" placeholder="Relative Name">
                            </div>
                            <div class="col">
                                <label for="email">Telephone:</label>
                                <input id="relative_tel" name="relative_tel" type="text" class="form-control my-2" placeholder="Relative Tel">
                            </div>
                            <div class="col">
                                <label for="residential Address">Relationship:</label>
                                <select name="relative_relationship" aria-label="Select a Relation" data-placeholder="Select a Country..." class="form-control my-2" tabindex="-1" aria-hidden="true" data-kt-initialized="1" id='relative_relationship'>
                                    <option value="">Select a Relation...</option>
                                    <option value="Spouse">Spouse</option>
                                    <option value="Son">Son</option>
                                    <option value="Daughter">Daughter</option>
                                    <option value="Aunty">Aunty</option>
                                    <option value="Uncle">Uncle</option>
                                </select>
                                <input type="hidden" name="agency" id="agency" value="<?php echo $agency; ?>"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                <label for="condition">Patient Condition:</label>
                                <textarea id="condition" name="condition" type="text" class="form-control my-2" placeholder="Patient Condition"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--end::Scroll-->
                    <!--begin::Actions-->
                    <div class="text-center pt-15">
                        <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                        <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('patientAdd','','');return false;">
                    </div>
                    <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>

    <?php }



if ($modalType == "editPatient") {

    $agency = $_SESSION['agencyID'];

    $data = array(
        "method" => "GETPATIENT",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id
    );
    //print_r(json_encode($data));
    $get_patient = APICall($data);
    $get_PatientJson = json_decode($get_patient, "true");
    $patient = $get_PatientJson['RESULTS'];
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <div class="modal-title">
                <h4>Add New Patient</h4>
            </div>
            <!--end::Modal title-->
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->
            <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <!--begin::Scroll-->
                <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">
                    <div class="form-group">
                        <label for="email">Full Name:</label>
                        <div class="row">
                            <div class="col">
                                <input id="surname" value="<?php echo $patient['surname'] ?>" name="surname" type="text" class="form-control my-2" placeholder="Surname">
                            </div>
                            <div class="col">
                                <input id="firstnames" value="<?php echo $patient['firstname'] ?>" name="firstnames" type="text" class="form-control my-2" placeholder="First Name">
                            </div>
                            <div class="col">
                                <input id="firstnames" value="<?php echo $patient['othernames'] ?>" name="othernames" type="text" class="form-control my-2" placeholder="Other Name">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="email">Date Of Birth:</label>
                                <input id="dateOfBirth" name="dateOfBirth" value="<?php echo $patient['dateofbirth'] ?>" type="date" class="form-control my-2" placeholder="Date of Birth">
                            </div>
                            <div class="col">
                                <label for="email">Gender:</label>
                                <select name="gender" aria-label="Select Gender" data-placeholder="Select a Gender..." class="form-control my-2" tabindex="-1" aria-hidden="true" data-kt-initialized="1" id='gender'>
                                    <option value="">Select Gender...</option>
                                    <option <?php if($patient['sex']=="Female"){?>selected <?php }?> value="Female">Female</option>
                                    <option <?php if($patient['sex']=="Male"){?>selected <?php }?> value="Male">Male</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="email">Nationality:</label>
                                <select name="nationality" aria-label="Select a Country" data-placeholder="Select a Country..." class="form-control my-2" aria-hidden="true" data-kt-initialized="1" id='nationality'>
                                    <option value="">Select a Country...</option>
                                    <?php foreach ($get_country_json['RESULTS'] as $country) { ?>
                                        <option <?php if($patient['country_of_origin']==$country['country']){?>selected <?php }?> value="<?php echo $country['country'] ?>"><?php echo $country['country'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="tel">Telephone:</label>
                                <input id="tel" value="<?php echo $patient['tel'] ?>" name="tel" type="text" class="form-control my-2" placeholder="Telephone number">
                            </div>
                            <div class="col">
                                <label for="email">Email:</label>
                                <input id="email" value="<?php echo $patient['email'] ?>" name="email" type="email" class="form-control my-2" placeholder="Email">
                            </div>
                            <div class="col">
                                <label for="residential Address">Residential Address:</label>
                                <input id="residential_address" value="<?php echo $patient['home_address'] ?>" name="residential_address" type="text" class="form-control my-2" placeholder="Residential Address">
                            </div>
                        </div>
                    </div>
                    <hr>

                    <label>
                        <h4>Family Relative Details</h4>
                    </label>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="tel">Name:</label>
                                <input id="relative_name" value="<?php echo $patient['emergency_contact_name'] ?>" name="relative_name" type="text" class="form-control my-2" placeholder="Relative Name">
                            </div>
                            <div class="col">
                                <label for="email">Telephone:</label>
                                <input id="relative_tel" value="<?php echo $patient['emergency_contact_tel'] ?>" name="relative_tel" type="text" class="form-control my-2" placeholder="Relative Tel">
                            </div>
                            <div class="col">
                                <label for="residential Address">Relationship:</label>
                                <select name="relative_relationship" aria-label="Select a Relation" data-placeholder="Select a Country..." class="form-control my-2" tabindex="-1" aria-hidden="true" data-kt-initialized="1" id='relative_relationship'>
                                    <option value="">Select a Relation...</option>
                                    <option <?php if($patient['emergency_contact_relationship']=="Spouse"){?> selected <?php } ?> value="Spouse">Spouse</option>
                                    <option <?php if($patient['emergency_contact_relationship']=="Son"){?> selected <?php } ?> value="Son">Son</option>
                                    <option <?php if($patient['emergency_contact_relationship']=="Daughter"){?> selected <?php } ?> value="Daughter">Daughter</option>
                                    <option <?php if($patient['emergency_contact_relationship']=="Aunty"){?> selected <?php } ?> value="Aunty">Aunty</option>
                                    <option <?php if($patient['emergency_contact_relationship']=="Uncle"){?> selected <?php } ?> value="Uncle">Uncle</option>
                                </select>
                                <input type="hidden" name="agency" id="agency" value="<?php echo $agency; ?>"> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                <label for="condition">Patient Condition:</label>
                                <textarea id="condition" name="condition" type="text" class="form-control my-2" placeholder="Patient Condition"><?php echo $patient['condition'];?></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--end::Scroll-->
                    <!--begin::Actions-->
                    <div class="text-center pt-15">
                        <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                        <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('patientUpdate','<?php echo $id ?>','');return false;">
                    </div>
                    <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>

    <?php }



if ($modalType == "addAllergy") {

    $patientID = $_POST['id'];
    //get allergies from db
    $dataAllergy = array(
        "method" => "LISTALLERGY",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
    );
    $get_Allergies = APICall($dataAllergy);
    $get_allergies_json = json_decode($get_Allergies, "true");
    $allergylist = $get_allergies_json['RESULTS'];
    //print_r($allergylist);

    $dataReaction = array(
        "method" => "LISTREACTION",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
    );
    $get_Reaction = APICall($dataReaction);
    $get_Reaction_json = json_decode($get_Reaction, "true");
    $reactionlist = $get_Reaction_json['RESULTS'];



    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <h2 class="fw-bold">Add Allergy</h2>
            <!--end::Modal title-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->

            <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <div class="" style="max-height: 800px; overflow-y:scroll">
                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="email">Allergy:</label>
                                <select class="js-example-basic-single form-control" name="allergy" id="allergy">
                                    <?php foreach ($allergylist as $allergy) { ?>
                                        <option value="<?php echo $allergy['allergy_name']; ?>"><?php echo $allergy['allergy_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col">
                                <label for="email">Reaction:</label>
                                <select class="js-example-basic-single form-control" name="allergy_reaction" id="allergy_reaction">
                                    <?php foreach ($reactionlist as $reaction) { ?>
                                        <option value="<?php echo $reaction['allergy_reaction']; ?>"><?php echo $reaction['allergy_reaction']; ?></option>
                                    <?php } ?>

                                </select>
                                <input type="hidden" value="<?php echo $patientID; ?>" name="patientID" id="patientID">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group" style="display: none;" id="newAllergy">
                
                <div class="row">
                    <div class="col">
                    <label for="email">Other Allergy</label>
                        <input type="text" id="tel" name="tel" value="<?php echo $user['tel']; ?>" class="form-control my-2" placeholder="Telephone Number">
                    </div>
                    <div class="col">
                        <label for="email">Other Reaction</label>
                        <input type="email" id="user_email" value="<?php echo $user['email']; ?>"  name="user_email" class="form-control my-2" placeholder="email:example@domain.com">
                        <input type="hidden" id="agency" name='agency' value="<?php echo $agency ?>">
                    </div>
                </div>

            </div> -->


                </div>

                <div class="text-center pt-15">
                    <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal">Cancel</button>
                    <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('savePatientAllergy','<?php echo $patientID; ?>','');return false;">

                </div>
            </form>
            <!--end::Form-->
        </div>
    <?php
}


if ($modalType == "viewVitals") {

    $selectRecord = selectData("SELECT * FROM patient_vitals where id=?", array($id));
    $vitals = $selectRecord['rowResults'];
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <h2 class="fw-bold">Vital Detail</h2>
            <!--end::Modal title-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->

            <table class="table table-responsive" id="mypatienttable" style="width: 100%;">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date Added</th>
                        <th scope="col">User</th>
                        <th scope="col">Weight</th>
                        <th scope="col">Height</th>
                        <th scope="col">Temp</th>
                        <th scope="col">Pulse</th>
                        <th scope="col">Resp</th>
                        <th scope="col">BP</th>
                        <th scope="col">Oxygen Saturation</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <th scope="row"><?php echo $count; ?></th>
                        <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($vitals['date'])) ?></span></td>
                        <td><?php echo  ucwords(strtolower(getUser($vitals['user_id']))); ?></td>
                        <td><?php echo $vitals['weight'] ?></td>
                        <td><?php echo $vitals['height'] ?></td>
                        <td><?php echo $vitals['temperature'] ?></td>
                        <td><?php echo $vitals['pulse'] ?></td>
                        <td><?php echo $vitals['resp'] ?></td>
                        <td><?php echo $vitals['BP1'] . " / " . $vitals['BP2'] ?></td>
                        <td><?php echo $vitals['oxygen_saturation'] ?></td>
                    </tr>


                </tbody>
            </table>
            <!--end::Form-->
        </div>
    <?php }


if ($modalType == "viewallvitals") {

    $vitalsData = array(
        "method" => "GETPATIENTVITALS",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "OFFSET" => "ALL"
    );

    //  print_r($vitalsData);
    $get_vitalsData = APICall($vitalsData);
    $get_VitalsJson = json_decode($get_vitalsData, "true");
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <h2 class="fw-bold">Vital Detail</h2>
            <!--end::Modal title-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->

            <table class="table table-responsive" id="mypatienttable" style="width: 100%;">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date Added</th>
                        <th scope="col">User</th>
                        <th scope="col"><i style="color:#6777ef" class="fas fa-balance-scale fa-fw"></i>&nbsp;Weight</th>
                        <th scope="col"><i style="color:#6777ef" class="fas fa-ruler-combined fa-fw"></i>&nbsp;Height</th>
                        <th scope="col"><i style="color:#6777ef" class="fas fa-thermometer-half fa-fw"></i>&nbsp;Temp</th>
                        <th scope="col"><i style="color:#6777ef" class="fas fa-stethoscope fa-fw"></i>&nbsp;Pulse</th>
                        <th scope="col"><i style="color:#6777ef" class="fas fa-lungs fa-fw"></i>&nbsp;Resp</th>
                        <th scope="col"><i style="color:#6777ef" class="fas fa-heartbeat fa-fw"></i>&nbsp;BP</th>
                        <th scope="col">Oxygen Saturation</th>
                        <th>BP Status</th>
                        <th>BMI</th>
                        <th>BMI Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($get_VitalsJson['STATUSCODE'] == "000") {
                        $count = 0;
                        foreach ($get_VitalsJson['RESULTS'] as $vitals) {
                            $count = $count + 1;
                    ?>

                            <tr>
                                <th scope="row"><?php echo $count; ?></th>
                                <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($vitals['date'])) ?></span></td>
                                <td><?php echo  ucwords(strtolower(getUser($vitals['user_id']))); ?></td>
                                <td><?php echo $vitals['weight'] ?></td>
                                <td><?php echo $vitals['height'] ?></td>
                                <td><?php echo $vitals['temperature'] ?></td>
                                <td><?php echo $vitals['pulse'] ?></td>
                                <td><?php echo $vitals['resp'] ?></td>
                                <td><?php echo $vitals['BP1'] . " / " . $vitals['BP2'] ?></td>
                                <td><?php echo $vitals['oxygen_saturation'] ?></td>
                                <td><?php

                                    $BP1_hyper1 = $vitals['BP1'];
                                    $BP2_hyper2 = $vitals['BP2'];
                                    if ($BP1_hyper1 > 180 && $BP2_hyper2 > 120) {
                                        $Hypertensiveness = 'Stage 3 (Crises)';
                                        $clss = "badge badge-danger";
                                    } else if (($BP1_hyper1 >= 140 && $BP2_hyper2 >= 90)) {
                                        $Hypertensiveness = 'Stage 2';
                                        $clss = "badge badge-danger";
                                    } else if ($BP1_hyper1 >= 130 && $BP2_hyper2 >= 80) {
                                        $Hypertensiveness = 'Stage 1';
                                        $clss = "badge badge-warning";
                                    } else if ($BP1_hyper1 >= 120 && $BP2_hyper2 > 80) {
                                        $Hypertensiveness = 'Elevated';
                                        $clss = "badge badge-info";
                                    } else {
                                        $Hypertensiveness = 'Normal';
                                        $clss = "badge badge-success";
                                    }

                                    ?>
                                    <span class="<?php echo $clss ?>"><?php echo $Hypertensiveness; ?></span>

                                </td>
                                <td><?php
                                    if($vitals['weight']>0 && $vitals['height']>0){
                                    list($bmi, $bmi_status) = explode("@@", calculateBMI($vitals['weight'], $vitals['height']));

                                    echo round($bmi, 2);
                                    }else { echo "N/A";}
                                    ?></td>
                                <td><?php 
                                 if($vitals['weight']>0 && $vitals['height']>0){
                                echo $bmi_status;
                                  }else {echo "N/A";} ?></td>

                            </tr>
                    <?php }
                    } ?>

                </tbody>
            </table>
            <!--end::Form-->
        </div>
    <?php }


if ($modalType == "AddNewVital") { ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <div class="modal-title">
                <h4>Add New Vital</h4>
            </div>
            <!--end::Modal title-->
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->
            <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <!--begin::Scroll-->
                <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">
                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="email">Height(m):</label>
                                <input id="height" name="height" type="text" class="form-control my-2" placeholder="height">
                            </div>
                            <div class="col">
                                <label for="email">Weight(kg):</label>
                                <input id="weight" name="weight" type="text" class="form-control my-2" placeholder="weight">
                            </div>

                        </div>
                    </div>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="email">Temperature(°C):</label>
                                <input id="temperature" name="temperature" type="text" class="form-control my-2" placeholder="temperature">
                            </div>
                            <div class="col">
                                <label for="email">Pulse(bpm):</label>
                                <input id="pulse" name="pulse" type="text" class="form-control my-2" placeholder="pulse">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="tel">Respiration Rate:</label>
                                <input id="respiration" name="respiration" type="text" class="form-control my-2" placeholder="respiration">
                            </div>
                            <div class="col">
                                <label for="email">Oxygen Saturation:</label>
                                <input id="oxy_saturation" name="oxy_saturation" type="text" class="form-control my-2" placeholder="oxy_saturation">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="row">
                            <div class="col">
                                <label for="tel">Pressure(Systolic):</label>
                                <input id="bp1" name="bp1" type="text" class="form-control my-2" placeholder="systolic">
                            </div>
                            <div class="col">
                                <label for="email">Pressure(Diastolic):</label>
                                <input id="bp2" name="bp2" type="text" class="form-control my-2" placeholder="diastolic">
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <!--end::Scroll-->
                <!--begin::Actions-->
                <div class="text-center pt-15">
                    <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                    <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('saveVital','<?php echo $id ?>','');return false;">
                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>
    <?php }




if ($modalType == "AddNewNote") { ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <div class="modal-title">
                <h4>Add New Note</h4>
            </div>
            <!--end::Modal title-->
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->
            <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <!--begin::Scroll-->
                <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">
                    <div class="row">
                        <div class="col">

                            <br>
                            <div class="custom-date-container">
                                <label for="">Results</label><br>
                                <label>User:<?php echo getUser($_SESSION['myMM_Userid']) ?></label>
                                <textarea id="editor_<?php echo $id; ?>" name="editor_<?php echo $id; ?>"><?php //echo $value; 
                                                                                                            ?></textarea>
                            </div>
                            <br>
                        </div>
                    </div>

                    <hr>
                </div>
                <!--end::Scroll-->
                <!--begin::Actions-->
                <div class="text-center pt-15">
                    <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                    <input type="button" class="btn btn-primary" value="Save" onclick="process_ckeditor('saveNote', '', '<?php echo $id ?>','Completed')">
                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>
    <?php }


if ($modalType == "viewNote") {

    $selectRecord = selectData("SELECT * FROM nurses_note where id=?", array($id));
    $notes = $selectRecord['rowResults'];
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <h2 class="fw-bold">Note Detail</h2>
            <!--end::Modal title-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->

            <span><?php echo $notes['note']; ?></span>
            <!--end::Form-->
        </div>
    <?php }



if ($modalType == "editNote") {

    //get note
    $selectNote = selectData("select * from nurses_note where id=?", array($id));
    $notes = $selectNote['rowResults'];
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <div class="modal-title">
                <h4>Update Note</h4>
            </div>
            <div class="modal-toolbar"><input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                <input type="button" class="btn btn-primary" value="Save" onclick="process_ckeditor('saveNote', '', '<?php echo $id ?>','Completed')">
            </div>
            <!--end::Modal title-->
            <!--end::Close-->
        </div>
        <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
            <!--begin::Form-->
            <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
                <!--begin::Scroll-->
                <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">
                    <div class="row">
                        <div class="col">

                            <br>
                            <div class="custom-date-container">
                                <label for="">Results</label><br>
                                <label>User:<?php echo getUser($notes['user_id']) ?></label>
                                <textarea id="editor_<?php echo $id; ?>" name="editor_<?php echo $id; ?>"><?php echo $notes['note']; ?></textarea>
                            </div>
                            <br>
                        </div>
                    </div>

                    <hr>
                </div>
                <!--end::Scroll-->
                <!--begin::Actions-->
                <div class="text-center pt-15">
                    <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                    <input type="button" class="btn btn-primary" value="Save" onclick="process_ckeditor('UpdateNote', '', '<?php echo $id ?>','Completed')">
                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>
    <?php }




if ($modalType == "viewallnotess") {

    //get note
    $notesData = array(
        "method" => "GETPATIENTNOTES",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "OFFSET" => "ALL"
    );

    //print_r(json_encode($vitalsData));
    $get_notesData = APICall($notesData);
    $get_notesJson = json_decode($get_notesData, "true");
    ?>
        <div class="modal-header" id="kt_modal_add_user_header">
            <!--begin::Modal title-->
            <div class="modal-title">
                <h4>Patient Notes</h4>
            </div>
            <div class="modal-toolbar"><input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">

                <!--end::Modal title-->
                <!--end::Close-->
            </div>
        </div>
            <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
                <!--begin::Form-->
                <ul class="iq-timeline">
                    <?php
                    if ($get_notesJson['STATUSCODE'] == "000") {
                        $count = 0;
                        foreach ($get_notesJson['RESULTS'] as $notes) {
                            $count = $count + 1;
                    ?>
                            <li>
                                <div class="<?php if (fmod($count, 2) > 0) {
                                                            echo 'timeline-dots border-success';
                                                        } else {
                                                            echo 'timeline-dots border-danger';
                                                        }?>"></div>
                                <h6 class=""><?php echo getUser($notes['user_id']) . "-  <span class='badge border border-success text-success'>" . date('j F Y g:i:a', strtotime($notes['date_added']))."</span>"; ?><small><span><i>Last update:</i><?php
                                $valueDate = getUser($notes['updated_user']) . "(" . date('jS F Y g:i a', strtotime($notes['last_updated'])) . ")";                                                                  
                                echo ($notes['last_updated'] == '' || $notes['last_updated'] == "00-00-00" || is_null($notes['last_updated'])) ? 'N/A' : $valueDate;?></span></small></h6>
                                <small class="mt-1"><?php echo $notes['note']; ?></small>
                            </li>
                        <?php }
                    } else { ?>
                        <li>
                            <div class="timeline-dots border-success"></div>
                            <h6 class="">No Records Found</h6>

                        </li>
                    <?php  } ?>
                </ul>
                <!--end::Form-->
            </div>
        <?php }




if ($modalType == "AddNewDrug") { 
    
    $itemsData = array(
        "method" => "GETITEMS",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "TYPE" => "Drugs"
    );

    //print_r(json_encode($vitalsData));
    $get_itemsData = APICall($itemsData);
    $get_itemsJson = json_decode($get_itemsData, "true");


    $doseData = array(
        "method" => "GETDOSE",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
    );

    //print_r(json_encode($vitalsData));
    $get_DoseData = APICall($doseData);
    $get_DoseJson = json_decode($get_DoseData, "true");
    
    
    ?>
    <div class="modal-header" id="kt_modal_add_user_header">
        <!--begin::Modal title-->
        <div class="modal-title">
            <h4>Add Prescription</h4>
        </div>
        <!--end::Modal title-->
        <!--end::Close-->
    </div>
    <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
        <!--begin::Form-->
        <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
            <!--begin::Scroll-->
            <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">
                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="email">Item:</label>
                            <select class="js-example-basic-single form-control"  name="item" id="item">
											<option></option>
											<?php foreach($get_itemsJson['RESULTS'] AS $items){?>
												<option value="<?php echo $items['id'] ?>"><?php echo $items['item']; ?></option>
											<?php }?>
										</select>
                        </div>

                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="email">Dose:</label>
                            
                            <input id="dose" name="dose" type="text" class="form-control my-2" placeholder="Dose">
                        </div>
                        <div class="col">
                            <label for="email">Dosage Form:</label>
                            <select class="form-control"  name="dosage" id="dosage">
											<option></option>
											<?php foreach($get_DoseJson['DOSAGE'] AS $dosage){?>
												<option value="<?php echo $dosage['dosage_form'] ?>"><?php echo $dosage['dosage_form']; ?></option>
											<?php }?>
							</select>
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="tel">Frequency:</label>
                            <select class="form-control"  name="frequency" id="frequency">
											<option></option>
											<?php foreach($get_DoseJson['FREQUENCY'] AS $freq){?>
												<option value="<?php echo $freq['frequency'] ?>"><?php echo $freq['frequency']; ?></option>
											<?php }?>
							</select>
                           
                        </div>
                        <div class="col">
                            <label for="email">Days:</label>
                            <input id="noofdays" name="noofdays" type="text" class="form-control my-2" placeholder="Days">
                        </div>
                    </div>
                </div>
            
                <hr>
            </div>
            <!--end::Scroll-->
            <!--begin::Actions-->
            <div class="text-center pt-15">
                <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('savePrescription','<?php echo $id ?>','');return false;">
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
<?php }


if($modalType=="AddNewTreatment"){
    $DrugsData = array(
        "method" => "GETPATIENTDRUGS",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "OFFSET" => "ALL"
    );

    //print_r(json_encode($vitalsData));
    $get_drugsData = APICall($DrugsData);
    $get_DrugsJson = json_decode($get_drugsData, "true");
    
    
    ?>
    <div class="modal-header" id="kt_modal_add_user_header">
        <!--begin::Modal title-->
        <div class="modal-title">
            <h4>Add Prescription</h4>
        </div>
        <!--end::Modal title-->
        <!--end::Close-->
    </div>
    <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
        <!--begin::Form-->
        <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
            <!--begin::Scroll-->
            <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">
                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="email">Drug:</label>
                            <select class="js-example-basic-single form-control"  name="item" id="item">
											<option></option>
											<?php foreach($get_DrugsJson['RESULTS'] AS $items){?>
												<option value="<?php echo $items['id']."@@".$items['item_id'] ?>"><?php echo $items['item']; ?></option>
											<?php }?>
										</select>
                        </div>

                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="email">Date:</label>
                            
                            <input id="dateAdded" name="dateAdded" type="date" readonly value="<?php echo date('Y-m-d') ?>" class="form-control my-2" placeholder="date">
                        </div>
                        <div class="col">
                            <label for="email">Time</label>
                            <input id="timeAdded" name="timeAdded" type="time" class="form-control my-2" placeholder="Dose">
                           
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="tel">Status:</label>
                            <select class="form-control"  name="status" id="status">
											<option value="">Select</option>
                                            <option value="Taken">Taken</option>
                                            <option value="Refused">Refused</option>
                                            <option value="Vomitted">Vomitted</option>
											
							</select>
                           
                        </div>
                        <div class="col">
                            <label for="email">Remarks:</label>
                            <textarea name="remarks" class="form-control" id="remarks"></textarea>
                        </div>
                    </div>
                </div>
            
                <hr>
            </div>
            <!--end::Scroll-->
            <!--begin::Actions-->
            <div class="text-center pt-15">
                <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('saveTreatment','<?php echo $id ?>','');return false;">
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
    <?php
}




if($modalType=="AddNewGlucose"){?>
    <div class="modal-header" id="kt_modal_add_user_header">
        <!--begin::Modal title-->
        <div class="modal-title">
            <h4>Add Glucose Record</h4>
        </div>
        <!--end::Modal title-->
        <!--end::Close-->
    </div>
    <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
        <!--begin::Form-->
        <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
            <!--begin::Scroll-->
            <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="date">Date:</label>
                            
                            <input id="dateAdded" name="dateAdded" type="date" readonly value="<?php echo date('Y-m-d') ?>" class="form-control my-2" placeholder="date">
                        </div>
                        <div class="col">
                            <label for="email">Time</label>
                            <input id="timeAdded" name="timeAdded" type="time" class="form-control my-2" placeholder="Dose">
                           
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="tel">FBS:</label>
                            <input type="text" name="fbs" id="fbs" class="form-control" placeholder="fbs">
                           
                        </div>
                        <div class="col">
                            <label for="rbs">RBS:</label>
                            <input type="text" name="rbs" id="rbs" class="form-control" placeholder="rbs">
                        
                        </div>
                    </div>
                </div>
                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="rbs">REMARKS:</label>
                           
                            <textarea name="remarks" class="form-control" id="remarks"></textarea>
                        </div>
                    </div>
                </div>
            
                <hr>
            </div>
            <!--end::Scroll-->
            <!--begin::Actions-->
            <div class="text-center pt-15">
                <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('saveGlucose','<?php echo $id ?>','');return false;">
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
    <?php
}




if($modalType=="AddNewUrine"){?>
    <div class="modal-header" id="kt_modal_add_user_header">
        <!--begin::Modal title-->
        <div class="modal-title">
            <h4>Add Urine Record</h4>
        </div>
        <!--end::Modal title-->
        <!--end::Close-->
    </div>
    <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
        <!--begin::Form-->
        <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
            <!--begin::Scroll-->
            <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="date">Date:</label>
                            
                            <input id="dateAdded" name="dateAdded" type="date" readonly value="<?php echo date('Y-m-d') ?>" class="form-control my-2" placeholder="date">
                        </div>
                        <div class="col">
                            <label for="email">Time</label>
                            <input id="timeAdded" name="timeAdded" type="time" class="form-control my-2" placeholder="Dose">
                           
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="protein">Protein:</label>
                            <input type="text" name="protein" id="protein" class="form-control" placeholder="protein">
                           
                        </div>
                        <div class="col">
                            <label for="sugar">Sugar:</label>
                            <input type="text" name="sugar" id="sugar" class="form-control" placeholder="sugar">
                        
                        </div>
                    </div>
                </div>
                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="weight">Weight:</label>
                            <input type="text" name="weight" id="weight" class="form-control" placeholder="weight">
                           
                        </div>
                        <div class="col">
                            <label for="sugar">Kerotones:</label>
                            <input type="text" name="kerotones" id="kerotones" class="form-control" placeholder="kerotones">
                        
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="row">
                        <div class="col">
                            <label for="other">Other:</label>
                           
                            <input name="other" class="form-control" id="other" placeholder="other"/>
                        </div>
                    </div>
                </div>
            
                <hr>
            </div>
            <!--end::Scroll-->
            <!--begin::Actions-->
            <div class="text-center pt-15">
                <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('saveUrine','<?php echo $id ?>','');return false;">
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
    <?php
}


if($modalType=="addDiagnosis"){
    
    $DiseaseData = array(
        "method" => "GETDISEASES",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD
    );

    //print_r(json_encode($DiseaseData));
    $get_DiseaseData = APICall($DiseaseData);
    $get_DiseaseJson = json_decode($get_DiseaseData, "true");
   // var_dump($get_DiseaseData);
    
    ?>
    <div class="modal-header" id="kt_modal_add_user_header">
        <!--begin::Modal title-->
        <div class="modal-title">
            <h4>Add Diagnosis</h4>
        </div>
        <!--end::Modal title-->
        <!--end::Close-->
    </div>
    <div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
        <!--begin::Form-->
        <form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
            <!--begin::Scroll-->
            <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px" style="max-height: 800px;">

            
            <div class="form-group">

<div class="row">
    <div class="col">
        <label for="email">Disease:</label>
        <select class="js-example-basic-single form-control"  name="disease" id="disease">
                        <option></option>
                        <?php foreach($get_DiseaseJson['RESULTS'] AS $disease){?>
                            <option value="<?php echo $disease['DiseaseID']."@@".$disease['Code'] ?>"><?php echo $disease['Disease']; ?></option>
                        <?php }?>
                    </select>
    </div>

</div>
</div>
          
                <hr>
            </div>
            <!--end::Scroll-->
            <!--begin::Actions-->
            <div class="text-center pt-15">
                <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('saveDiagnosis','<?php echo $id ?>','');return false;">
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
    <?php
}


if($modalType=="userPermssion"){

    //getPermission Groups
$data1= array(
    "method"=>"SECURITYLEVELS",
    "api_key"=> APIKEY,
    "user"=> USER,
    "passcode"=> PASSWORD
);
$get_clientlist1 = APICall($data1);
$get_client_json = json_decode($get_clientlist1,"true");

//print_r($get_client_json);

$userpermid=$id;

 
?>
<div class="modal-header" id="kt_modal_add_user_header">
    <!--begin::Modal title-->
    <div class="modal-title">
    User Permission  for <?php echo strtoupper(getUser($id));?>
                <input name="id4" type="hidden" id="id4" value="<?php echo $id; ?>" />
    </div>
    <!--end::Modal title-->
    <!--end::Close-->
</div>
<div class="modal-body mh-1000px scroll-y mx-5 mx-xl-15 my-7">
<form id="kt_modal_add_user_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST">
    <div class="row">
    <?php
foreach($get_client_json['RESULTS'] AS $row_rsodest){?>
            <div class="col-md-4">
                <div class="iq-card iq-card--height-fluid">
                    <div class="iq-card-header d-flex justify-content-between">
                            <h4 class="iq-header-title">
                               <?php echo $row_rsodest['menu_name']; ?>
                            </h4>
                    </div>
                    
                    <div class="iq-card-body">
                        <!--begin::k-widget4-->
                        <?php
							  $menuplace=$row_rsodest['menu_place'];
                                //getsubmenus
                                $field= array(
                                    "method"=>"SUBSECURITYLEVELS",
                                    "api_key"=> APIKEY,
                                    "user"=> USER,
                                    "passcode"=> PASSWORD,
                                    "MENUPLACE"=>$menuplace
                                );
                                $get_submenu = APICall($field);
                                $get_submenu_json = json_decode($get_submenu,"true");


							foreach($get_submenu_json['RESULTS'] AS $row_rsodest_sub) {  ?>
                            <div class="form-group">
                                 <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="item<?php echo $row_rsodest_sub['id'];?>" id="item<?php echo $row_rsodest_sub['id'];?>" value="<?php echo $row_rsodest_sub['id'];?>" <?php if(getUserPermid($row_rsodest_sub['id'],$id)=='yes'){?>checked="checked" <?php }?>>
                                    <label class="custom-control-label" for="customCheck1"><?php echo $row_rsodest_sub['menu_name']; ?></label>
                                 </div>
                                 
                              </div>
						<?php }?>
                        <!--end::Widget 9-->
                    </div>
                </div>
                
            </div>

            <?php } ?>
                            </div>
</form>

</div>

<?php } ?>


        <script>
            //  let editor;
            ClassicEditor
                .create(document.querySelector('#editor_<?php echo $id; ?>'))
                .then(newEditor => {
                    editor = newEditor;
                })
                .catch(error => {
                    console.error(error);
                });
        </script>

