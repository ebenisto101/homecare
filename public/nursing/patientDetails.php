<?php
include_once("./includes/includes.php");

if (!isset($_SESSION)) {
    session_start();
}
$action = $_POST['act'];
$id = $_POST['id'];

//GET COUNTRYLIST
$data = array(
    "method" => "GETPATIENT",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "PATIENTID" => $id
);
//print_r(json_encode($data));
$get_employee = APICall($data);
$get_employeeJson = json_decode($get_employee, "true");
$employee = $get_employeeJson['RESULTS'];
//print_r($get_employeeJson);
$assigned = $get_employeeJson['ASSIGNED'];

//get allergies
$allergyData = array(
    "method" => "GETALLERGYHISTORY",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "PATIENTID" => $id
);

$get_allergyHistory = APICall($allergyData);
$get_allergyJson = json_decode($get_allergyHistory, "true");



//get Diagnosis
$diagnosisData = array(
    "method" => "GETDIAGNOSISHISTORY",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "PATIENTID" => $id
);

$get_diagnosisHistory = APICall($diagnosisData);
$get_diagnosisJson = json_decode($get_diagnosisHistory, "true");
//print_r($get_diagnosisJson);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
                <div class="iq-header-title">
                    <h4 class="card-title">Patient Details</h4>
                </div>
                <div class="iq-header-toolbar">
                    <button type="button" class="btn btn-outline-success mb-3" onclick="routeTrigger('<?php echo $action ?>','<?php echo $id; ?>')" id="ref2">
                        <i class="fa fa-refresh"></i>Refresh</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="iq-card">
            <div class="iq-card-body ps-0 pe-0 pt-0">
                <div class="docter-details-block">
                    <div class="doc-profile-bg bg-primary" style="height:150px;">
                    </div>
                    <div class="docter-profile text-center">
                        <img src="assets/images/user/11.png" alt="profile-img" class="avatar-130 img-fluid">
                    </div>
                    <div class="text-center mt-3 ps-3 pe-3">
                        <h4><b><?php echo $employee['surname'] . " " . $employee['firstname'] . " " . $employee['othernames']; ?></b></h4>
                        <p><?php echo $employee['age'] . " years"; ?></p>
                        <p class="mb-0"><?php echo $employee['condition'];?></p>
                    </div>
                    <hr>
                    <!-- <ul class="doctoe-sedual d-flex align-items-center justify-content-between p-0 m-0">
                                 <li class="text-center">
                                    <h3 class="counter">4500</h3>
                                    <span>Operations</span>
                                  </li>
                                  <li class="text-center">
                                    <h3 class="counter">100</h3>
                                    <span>Hospital</span>
                                  </li>
                                  <li class="text-center">
                                    <h3 class="counter">10000</h3>
                                    <span>Patients</span>
                                  </li>
                              </ul> -->
                </div>
            </div>
        </div>
        <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
                <div class="iq-header-title">
                    <h4 class="card-title">Personal Information</h4>
                </div>
            </div>
            <div class="iq-card-body">
                <div class="about-info m-0 p-0">
                    <div class="row">
                        <div class="col-4">Name:</div>
                        <div class="col-8"><?php echo $employee['surname'] . " " . $employee['firstname'] . " " . $employee['othernames']; ?></div>
                        <div class="col-4">Age:</div>
                        <div class="col-8"><?php echo $employee['age'] . " years"; ?></div>
                        <div class="col-4">Email:</div>
                        <div class="col-8"><?php echo $employee['email']; ?></div>
                        <div class="col-4">Phone:</div>
                        <div class="col-8"><a href="tel:<?php echo $employee['tel']; ?>"><?php echo $employee['tel']; ?></a></div>
                        <div class="col-4">Location:</div>
                        <div class="col-8"><?php echo $employee['home_address']; ?></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-lg-9" id="client Details">
        <div class="row">
            <div class="col-md-6">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Patient Allergies</h4>
                        </div>
                        <div class="iq-header-toolbar">
                            <button type="button" class="btn btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('addAllergy','<?php echo $id; ?>', 'test_wrapper_results2')">
                                <i class="fa fa-plus"></i>Add New</button>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <ul class="speciality-list m-0 p-0">
                            <?php if ($get_allergyJson['STATUSCODE'] == "000") {
                                foreach ($get_allergyJson['RESULTS'] as $allergy) { ?>
                                    <li class="d-flex mb-4 align-items-center">
                                        <div class="user-img img-fluid"><a href="#" class="iq-bg-primary"><i class="ri-award-fill"></i></a></div>
                                        <div class="media-support-info ms-3">
                                            <h6><?php echo $allergy['allergy'] . "- <span class='badge border border-success text-success'>" . date('j F Y', strtotime($allergy['date_added'])) . "</span>"; ?></h6>
                                            <p class="mb-0"><?php echo $allergy['reaction']; ?></p>
                                        </div>
                                    </li>

                                <?php }
                            } else { ?>
                                <h4>No Allergies Found</h4>
                            <?php } ?>


                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Patient Diagnosis</h4>
                        </div>
                        <div class="iq-header-toolbar">
                        <button type="button" class="btn btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('addDiagnosis','<?php echo $id; ?>', 'test_wrapper_results2')">
                                <i class="fa fa-plus"></i>Add Disease</button>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <ul class="iq-timeline">
                        <?php if ($get_diagnosisJson['STATUSCODE'] == "000") {
                                foreach ($get_diagnosisJson['RESULTS'] as $diagnosis) { ?>
                            <li>
                                <div class="timeline-dots border-success"></div>
                                <h6 class=""><?php echo $diagnosis['disease_name']; ?></h6>
                                <small class="mt-1"><?php echo date("j F Y g:i:a",strtotime($diagnosis['date_added'])); ?></small>
                            </li>
                           <?php } }else{
                            echo "No Diagnosis Found";
                           }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">Nursing Care</h4>
                    </div>
                </div>
                <div class="iq-card-body">

                    <div class="row">
                        <div class="col-sm-3" style="border-style: 15px solid red;">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-home" aria-selected="true">Patient Vitals</a>
                                <a class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-profile" aria-selected="false" tabindex="-1">Nursing Notes</a>
                                <a class="nav-link" id="v-pills-messages-tab" data-bs-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-messages" aria-selected="false" tabindex="-1">Treatment Sheet</a>
                                <a class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-settings" aria-selected="false" tabindex="-1">Glucose Monitoring</a>
                                <a class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-settings" aria-selected="false" tabindex="-1">Urine Monitoring</a>
                                <a class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" href="#v-pills-6" role="tab" aria-controls="v-pills-settings" aria-selected="false" tabindex="-1">Patient Drugs</a>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="tab-content mt-0" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
                                    <!-- LIST VITALS --->
                                    <?php

                                    $vitalsData = array(
                                        "method" => "GETPATIENTVITALS",
                                        "api_key" => APIKEY,
                                        "user" => USER,
                                        "passcode" => PASSWORD,
                                        "PATIENTID" => $id,
                                        "OFFSET" => "5"
                                    );

                                    //print_r(json_encode($vitalsData));
                                    $get_vitalsData = APICall($vitalsData);
                                    $get_VitalsJson = json_decode($get_vitalsData, "true");
                                    ?>
                                    <div class="iq-card">
                                        <div class="iq-card-header d-flex justify-content-between">
                                            <div class="iq-header-title">
                                                <h4 class="card-title">Patient Vitals</h4>
                                            </div>
                                            <div class="iq-header-toolbar">
                                                <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewVital','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>&nbsp; | &nbsp;
                                                <button type="button" class="btn btn-sm me-2 btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewallvitals','<?php echo $id; ?>', 'test_wrapper_results3')">
                                                    <i class="fa fa-eye"></i>View All</button>
                                            </div>
                                        </div>
                                        <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                            <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Date Added</th>
                                                        <th scope="col">User</th>
                                                        <th scope="col">Weight</th>
                                                        <th scope="col">Height</th>
                                                        <th scope="col">Temp</th>
                                                        <th scope="col">Pulse</th>
                                                        <th scope="col">Resp</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($get_VitalsJson['STATUSCODE'] == "000") {
                                                        $count = 0;
                                                        foreach ($get_VitalsJson['RESULTS'] as $vitals) {
                                                            $count = $count + 1;
                                                    ?>
                                                            <tr>
                                                                <th scope="row"><?php echo $count; ?></th>
                                                                <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($vitals['date'])) ?></span></td>
                                                                <td><?php echo  ucwords(strtolower(getUser($vitals['user_id']))); ?></td>
                                                                <td><?php echo $vitals['weight'] ?></td>
                                                                <td><?php echo $vitals['height'] ?></td>
                                                                <td><?php echo $vitals['temperature'] ?></td>
                                                                <td><?php echo $vitals['pulse'] ?></td>
                                                                <td><?php echo $vitals['resp'] ?></td>
                                                                <td style="cursor: pointer"><span data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewVitals','<?php echo $vitals['id']; ?>','test_wrapper_results3')"><i class="fa fa-eye" style="color: #81c91d; cursor: pointer"></i></span>

                                                                </td>
                                                            </tr>
                                                    <?php  }
                                                    } ?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
                                    <?php

                                    $notesData = array(
                                        "method" => "GETPATIENTNOTES",
                                        "api_key" => APIKEY,
                                        "user" => USER,
                                        "passcode" => PASSWORD,
                                        "PATIENTID" => $id,
                                        "OFFSET" => "5"
                                    );

                                    //print_r(json_encode($vitalsData));
                                    $get_notesData = APICall($notesData);
                                    $get_notesJson = json_decode($get_notesData, "true");
                                    ?>
                                    <div class="iq-card">
                                        <div class="iq-card-header d-flex justify-content-between">
                                            <div class="iq-header-title">
                                                <h4 class="card-title">Nurses Notes</h4>
                                            </div>
                                            <div class="iq-header-toolbar">
                                                <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewNote','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>&nbsp; | &nbsp;
                                                <button type="button" class="btn btn-sm me-2 btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewallnotess','<?php echo $id; ?>', 'test_wrapper_results3')">
                                                    <i class="fa fa-eye"></i>View All</button>
                                            </div>
                                        </div>
                                        <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                            <table class="table table-responsive" id="mypatienttable" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Date Added</th>
                                                        <th scope="col">Note</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($get_notesJson['STATUSCODE'] == "000") {
                                                        $count = 0;
                                                        foreach ($get_notesJson['RESULTS'] as $notes) {
                                                            $count = $count + 1;
                                                    ?>
                                                            <tr>
                                                                <th scope="row"><?php echo $count; ?></th>
                                                                <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($notes['date_added'])) ?></span></td>
                                                                <td><?php echo substr_replace($notes['note'], "...", 50); ?> <a style="color:blue;cursor:pointer" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('viewNote','<?php echo $notes['id']; ?>','test_wrapper_results2')">read more</a></td>
                                                                <td><small><span><i>Owner</i>:<?php echo getUser($notes['user_id']) . "(" . date('jS F Y g:i a', strtotime($notes['date_added'])) . ")"; ?></span><br /><span><i>Last update:</i><?php
                                                                                                                                                                                                                                                    $valueDate = getUser($notes['updated_user']) . "(" . date('jS F Y g:i a', strtotime($notes['last_updated'])) . ")";
                                                                                                                                                                                                                                                    echo ($notes['last_updated'] == '' || $notes['last_updated'] == "00-00-00" || is_null($notes['last_updated'])) ? 'N/A' : $valueDate;
                                                                                                                                                                                                                                                    //echo $valueDate12;
                                                                                                                                                                                                                                                    ?></span></small></td>
                                                                <td><?php if ($notes['user_id'] == $_SESSION['myMM_Userid'] && date('Y-m-d', strtotime($notes['date_added'])) == date("Y-m-d")) { ?><span data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('editNote','<?php echo $notes['id']; ?>','test_wrapper_results2')"><i class="fa fa-edit"></i></span><?php } ?></td>

                                                            </tr>
                                                        <?php  }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="5">No Records Found</td>
                                                        </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
                                    <?php

                                    $treatmentData = array(
                                        "method" => "GETPATIENTTREATMENTS",
                                        "api_key" => APIKEY,
                                        "user" => USER,
                                        "passcode" => PASSWORD,
                                        "PATIENTID" => $id,
                                        "OFFSET" => "5"
                                    );

                                    //print_r(json_encode($vitalsData));
                                    $get_treatmentData = APICall($treatmentData);
                                    $get_treatmentJson = json_decode($get_treatmentData, "true");
                                    //print_r($get_treatmentJson);
                                    ?>
                                    <div class="iq-card">
                                        <div class="iq-card-header d-flex justify-content-between">
                                            <div class="iq-header-title">
                                                <h4 class="card-title">Treatment Chart</h4>
                                            </div>
                                            <div class="iq-header-toolbar">
                                                <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewTreatment','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>&nbsp; | &nbsp;
                                                <!-- <button type="button" class="btn btn-sm me-2 btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewallnotess','<?php echo $id; ?>', 'test_wrapper_results3')">
                                                    <i class="fa fa-eye"></i>View All</button> -->
                                            </div>
                                        </div>
                                        <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                            <table class="table table-responsive" id="mypatienttable" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">Time</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($get_treatmentJson['STATUSCODE'] == "000") {
                                                        $count = 0;
                                                        foreach ($get_treatmentJson['RESULTS'] as $treatments) {
                                                            $count = $count + 1;
                                                    ?>
                                                            <tr>
                                                                <th scope="row"><?php echo $count; ?></th>
                                                                <td><span class='badge border border-success text-success'><?php echo date('j F Y', strtotime($treatments['date_recorded'])) ?></span></td>
                                                                <td><?php echo $treatments['item']; ?></td>
                                                                <td><?php echo $treatments['timesRecorded'] ?></td>
                                                                <!-- <td><?php //if ($notes['user_id'] == $_SESSION['myMM_Userid'] && date('Y-m-d', strtotime($notes['date_added'])) == date("Y-m-d")) { ?><span data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('editNote','<?php echo $notes['id']; ?>','test_wrapper_results2')"><i class="fa fa-edit"></i></span> -->
                                                                    <?php //} ?></td>

                                                            </tr>
                                                        <?php  }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="5">No Records Found</td>
                                                        </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
                                    <!-- LIST VITALS --->
                                    <?php

                                    $glucoseData = array(
                                        "method" => "GETPATIENTGLUCOSE",
                                        "api_key" => APIKEY,
                                        "user" => USER,
                                        "passcode" => PASSWORD,
                                        "PATIENTID" => $id,
                                        "OFFSET" => "ALL"
                                    );

                                    //print_r(json_encode($vitalsData));
                                    $get_glucoseData = APICall($glucoseData);
                                    $get_glucoseJson = json_decode($get_glucoseData, "true");
                                    //print_r($get_glucoseJson);
                                    ?>
                                    <div class="iq-card">
                                        <div class="iq-card-header d-flex justify-content-between">
                                            <div class="iq-header-title">
                                                <h4 class="card-title">Glucose Monitoring</h4>
                                            </div>
                                            <div class="iq-header-toolbar">
                                                <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewGlucose','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>&nbsp; | &nbsp;
                                                <!-- <button type="button" class="btn btn-sm me-2 btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewallvitals','<?php echo $id; ?>', 'test_wrapper_results3')">
                                                    <i class="fa fa-eye"></i>View All</button> -->
                                            </div>
                                        </div>
                                        <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                            <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Date Time</th>
                                                        <th scope="col">FBS</th>
                                                        <th scope="col">RBS</th>
                                                        <th scope="col">Remark</th>
                                                        <th scope="col">User</th>

                                                        <!-- <th scope="col">Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($get_glucoseJson['STATUSCODE'] == "000") {
                                                        $count = 0;
                                                        foreach ($get_glucoseJson['RESULTS'] as $glucose) {
                                                            $count = $count + 1;
                                                    ?>
                                                            <tr>
                                                                <th scope="row"><?php echo $count; ?></th>
                                                                <td><?php echo date('j F Y g:i:a', strtotime($glucose['date_recorded'])); ?><span class='badge border border-success text-success'><?php echo $glucose['time_recorded'] ?></span></td>

                                                                <td><?php echo $glucose['fbs'] ?></td>
                                                                <td><?php echo $glucose['rbs'] ?></td>
                                                                <td><?php echo $glucose['remarks'] ?></td>
                                                                <td><?php echo  ucwords(strtolower(getUser($glucose['user_id']))); ?></td>


                                                                </td>
                                                            </tr>
                                                    <?php  }
                                                    } ?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5-tab">
                                    <?php

                                    $urineData = array(
                                        "method" => "GETPATIENTURINE",
                                        "api_key" => APIKEY,
                                        "user" => USER,
                                        "passcode" => PASSWORD,
                                        "PATIENTID" => $id,
                                        "OFFSET" => "ALL"
                                    );

                                    //print_r(json_encode($vitalsData));
                                    $get_urineData = APICall($urineData);
                                    $get_urineJson = json_decode($get_urineData, "true");
                                    //print_r($get_glucoseJson);
                                    ?>
                                    <div class="iq-card">
                                        <div class="iq-card-header d-flex justify-content-between">
                                            <div class="iq-header-title">
                                                <h4 class="card-title">Urine Monitoring</h4>
                                            </div>
                                            <div class="iq-header-toolbar">
                                                <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewUrine','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>&nbsp; | &nbsp;
                                                <!-- <button type="button" class="btn btn-sm me-2 btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewallvitals','<?php echo $id; ?>', 'test_wrapper_results3')">
                <i class="fa fa-eye"></i>View All</button> -->
                                            </div>
                                        </div>
                                        <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                            <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Date Time</th>
                                                        <th scope="col">Protein</th>
                                                        <th scope="col">Sugar</th>
                                                        <th scope="col">Weight</th>
                                                        <th scope="col">Kerotones</th>
                                                        <th scope="col">Other</th>
                                                        <th scope="col">User</th>

                                                        <!-- <th scope="col">Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($get_urineJson['STATUSCODE'] == "000") {
                                                        $count = 0;
                                                        foreach ($get_urineJson['RESULTS'] as $urine) {
                                                            $count = $count + 1;
                                                    ?>
                                                            <tr>
                                                                <th scope="row"><?php echo $count; ?></th>
                                                                <td><?php echo date('j F Y g:i:a', strtotime($urine['date_recorded'])); ?><span class='badge border border-success text-success'><?php echo $urine['time_recorded'] ?></span></td>

                                                                <td><?php echo $urine['protein'] ?></td>
                                                                <td><?php echo $urine['sugar'] ?></td>
                                                                <td><?php echo $urine['weight'] ?></td>
                                                                <td><?php echo $urine['kerotones'] ?></td>
                                                                <td><?php echo $urine['other'] ?></td>
                                                                <td><?php echo  ucwords(strtolower(getUser($urine['user_id']))); ?></td>


                                                                </td>
                                                            </tr>
                                                    <?php  }
                                                    } ?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-6-tab">
                                    <?php

                                    $DrugsData = array(
                                        "method" => "GETPATIENTDRUGS",
                                        "api_key" => APIKEY,
                                        "user" => USER,
                                        "passcode" => PASSWORD,
                                        "PATIENTID" => $id,
                                        "OFFSET" => "ALL"
                                    );

                                    //print_r(json_encode($vitalsData));
                                    $get_drugsData = APICall($DrugsData);
                                    $get_DrugsJson = json_decode($get_drugsData, "true");
                                    ?>
                                    <div class="iq-card">
                                        <div class="iq-card-header d-flex justify-content-between">
                                            <div class="iq-header-title">
                                                <h4 class="card-title">Patient Vitals</h4>
                                            </div>
                                            <div class="iq-header-toolbar">
                                                <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewDrug','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>
                                            </div>
                                        </div>
                                        <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                            <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Date Added</th>
                                                        <th scope="col">User</th>
                                                        <th scope="col">Prescrition</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($get_DrugsJson['STATUSCODE'] == "000") {
                                                        $count = 0;
                                                        foreach ($get_DrugsJson['RESULTS'] as $drugs) {
                                                            $count = $count + 1;
                                                    ?>
                                                            <tr>
                                                                <th scope="row"><?php echo $count; ?></th>
                                                                <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($drugs['date_added'])) ?></span></td>
                                                                <td><?php echo  ucwords(strtolower(getUser($drugs['user_id']))); ?></td>
                                                                <td><small>
                                                                        <div class="alert alert-primary" role="alert">
                                                                            <div class="iq-alert-text"><?php echo $drugs['item'] . ":" . $drugs['dose'] . " " . $drugs['dosage_form'] . " " . $drugs['frequency'] . " " . $drugs['no_of_days'] . " days"; ?></div>
                                                                        </div>
                                                                    </small></td>

                                                            </tr>
                                                    <?php  }
                                                    } ?>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" style="display: none;" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-lg">
        <!--begin::Modal content-->
        <div class="modal-content" id="test_wrapper_results2">

        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>

<div class="modal fade bd-example-modal-xl" tabindex="-1" style="display: none;" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-xl">
        <!--begin::Modal content-->
        <div class="modal-content" id="test_wrapper_results3">

        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>

<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>