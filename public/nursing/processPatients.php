<?php
include_once("./includes/includes.php");

if (!isset($_SESSION)) {
    session_start();
}

$action = $_POST['act'];
$id = $_POST['id'];

if ($action == "savePatientAllergy") {


    $allergy = $_POST['allergy'];
    $reaction = $_POST['allergy_reaction'];
    $patientID = $_POST['patientID'];

    $saveAllergyData = array(
        "method" => "SAVEPATIENTALLERGY",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "ALLERGY" => $allergy,
        "ALLERGYREACTION" => $reaction,
        "PATIENTID" => $patientID
    );
    $get_saveAllergy = APICall($saveAllergyData);
    $get_saveAllergy_json = json_decode($get_saveAllergy, "true");
    // $saveAllergy = $get_saveAllergy_json['RESULTS'];

    if ($get_saveAllergy_json['STATUSCODE'] == '000') {
        $message = array(
            "code" => "000",
            "message" => "Allergy successfuly added",
        );
    } else {
        // echo "dddd";
        $message = array(
            "code" => $data['STATUSCODE'],
            "message" => $data['STATUSMSG'],
        );
    }

    echo json_encode($message);
    die();
}


if ($action == "saveVital") {
    $weight = $_POST['weight'];
    $height = $_POST['height'];
    $temp = $_POST['temperature'];
    $pulse = $_POST['pulse'];
    $respiration = $_POST['respiration'];
    $oxy_saturation = $_POST['oxy_saturation'];
    $bp1 = $_POST['bp1'];
    $bp2 = $_POST['bp2'];


    $saveVitalData = array(
        "method" => "SAVEPATIENTVITAL",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "WEIGHT" => $weight,
        "HEIGHT" => $height,
        "TEMP" => $temp,
        "RESPIRATION" => $respiration,
        "OXYSATURATION" => $oxy_saturation,
        "BP1" => $bp1,
        "BP2" => $bp2,
        "PULSE" => $pulse,
        "PATIENTID" => $id,
        "USERID" => $_SESSION['myMM_Userid']
    );
    $get_saveVital = APICall($saveVitalData);
    $get_saveVital_json = json_decode($get_saveVital, "true");
    // $saveAllergy = $get_saveAllergy_json['RESULTS'];

    if ($get_saveVital_json['STATUSCODE'] == '000') {
        $message = array(
            "code" => "000",
            "message" => "Allergy successfuly added",
        );
    } else {
        // echo "dddd";
        $message = array(
            "code" => $get_saveVital_json['STATUSCODE'],
            "message" => $get_saveVital_json['STATUSMSG'],
        );
    }

    echo json_encode($message);
    die();
}


if ($action == "saveNote" || $action == "UpdateNote") {


    //  $resultValue = $_POST['editor'];
    $resultValue = str_ireplace('&nbsp;', ' ', $_POST['editor2']);
    // $string = htmlentities($resultValue, null, 'utf-8');
    $content = str_replace("&nbsp;", "", $resultValue);
    //echo $_POST['editor'];
    $data = array(
        "method" => "SAVENOTE",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "USERID" => $_SESSION['myMM_Userid'],
        "VALUE" => $content,
        "TYPE" => $action
    );

    //print_r(json_encode($data));
    $get_clientlist = APICall($data);
    $get_client_json = json_decode($get_clientlist, "true");

    if ($get_client_json['STATUSCODE'] == "000") {

        $message = array(
            "code" => "000",
            "message" => "Note successfuly added",
        );
    } else {
        // echo "dddd";
        $message = array(
            "code" => $get_client_json['STATUSCODE'],
            "message" => $get_client_json['STATUSMSG'],
        );
    }

    echo json_encode($message);
    die();
}


if ($action == "savePrescription") {

    $item = $_POST['item'];
    $dose = $_POST['dose'];
    $form = $_POST['dosage'];
    $frequency = $_POST['frequency'];
    $noofdays = $_POST['noofdays'];


    $data = array(
        "method" => "SAVEPRESCRIPTION",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "USERID" => $_SESSION['myMM_Userid'],
        "ITEMID" => $item,
        "DOSE" => $dose,
        "DOSAGE" => $form,
        "FREQ" => $frequency,
        "NOOFDAYS" => $noofdays,
        "AGENCYID" => $_SESSION['agencyID']
    );
 
    //print_r(json_encode($data));
    $get_clientlist = APICall($data);
    $get_client_json = json_decode($get_clientlist, "true");

    if ($get_client_json['STATUSCODE'] == "000") {

        $message = array(
            "code" => "000",
            "message" => $get_client_json['STATUSMSG'],
        );
    } else {
        // echo "dddd";
        $message = array(
            "code" => $get_client_json['STATUSCODE'],
            "message" => $get_client_json['STATUSMSG'],
        );
    }

    echo json_encode($message);
    die();
}


if ($action == "saveTreatment") {

    list($claimsid,$itemid) =explode("@@",$_POST['item']);
    $dateAdded = $_POST['dateAdded'];
    $timeAdded = $_POST['timeAdded'];
    $status = $_POST['status'];
    $remarks = $_POST['remarks'];


    $data = array(
        "method" => "SAVETREATMENT",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "USERID" => $_SESSION['myMM_Userid'],
        "ITEMID" => $itemid,
        "STATUS" => $status,
        "DATERECORDED"=>$dateAdded,
        "TIMERECORDED"=>$timeAdded,
        "CLAIMSID"=>$claimsid,
        "REMARKS"=>$remarks
    );
 
    //print_r(json_encode($data));
    $get_clientlist = APICall($data);
    $get_client_json = json_decode($get_clientlist, "true");

    if ($get_client_json['STATUSCODE'] == "000") {

        $message = array(
            "code" => "000",
            "message" => $get_client_json['STATUSMSG'],
        );
    } else {
        // echo "dddd";
        $message = array(
            "code" => $get_client_json['STATUSCODE'],
            "message" => $get_client_json['STATUSMSG'],
        );
    }

    echo json_encode($message);
    die();
}


if($action=="saveGlucose"){
   
    $dateAdded = $_POST['dateAdded'];
    $timeAdded = $_POST['timeAdded'];
    $fbs = $_POST['fbs'];
    $rbs = $_POST['rbs'];
    $remarks = $_POST['remarks'];


    $data = array(
        "method" => "SAVEPATIENTGLUCOSE",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "USERID" => $_SESSION['myMM_Userid'],
        "RBS" => $rbs,
        "FBS" => $fbs,
        "DATERECORDED"=>$dateAdded,
        "TIMERECORDED"=>$timeAdded,
        "REMARKS"=>$remarks
    );
 
    //print_r(json_encode($data));
    $get_clientlist = APICall($data);
    $get_client_json = json_decode($get_clientlist, "true");

    if ($get_client_json['STATUSCODE'] == "000") {

        $message = array(
            "code" => "000",
            "message" => $get_client_json['STATUSMSG'],
        );
    } else {
        // echo "dddd";
        $message = array(
            "code" => $get_client_json['STATUSCODE'],
            "message" => $get_client_json['STATUSMSG'],
        );
    }

    echo json_encode($message);
    die();
}



if($action=="saveUrine"){
   
    $dateAdded = $_POST['dateAdded'];
    $timeAdded = $_POST['timeAdded'];
    $protein = $_POST['protein'];
    $sugar = $_POST['sugar'];
    $weight = $_POST['weight'];
    $kerotones = $_POST['kerotones'];
    $other = $_POST['other'];


    $data = array(
        "method" => "SAVEPATIENTURINE",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "USERID" => $_SESSION['myMM_Userid'],
        "PROTEIN" => $protein,
        "SUGAR" => $sugar,
        "WEIGHT"=>$weight,
        "KEROTONES"=>$kerotones,
        "OTHER"=>$other,
        "DATERECORDED"=>$dateAdded,
        "TIMERECORDED"=>$timeAdded
    );
 
    //print_r(json_encode($data));
    $get_clientlist = APICall($data);
    $get_client_json = json_decode($get_clientlist, "true");

    if ($get_client_json['STATUSCODE'] == "000") {

        $message = array(
            "code" => "000",
            "message" => $get_client_json['STATUSMSG'],
        );
    } else {
        // echo "dddd";
        $message = array(
            "code" => $get_client_json['STATUSCODE'],
            "message" => $get_client_json['STATUSMSG'],
        );
    }

    echo json_encode($message);
    die();
}


if($action=="saveDiagnosis"){
  

    list($diseaseid,$diseasecode) = explode("@@",$_POST['disease']);

    $data = array(
        "method" => "SAVEPATIENTDIAGNOSIS",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "PATIENTID" => $id,
        "USERID" => $_SESSION['myMM_Userid'],
        "DISEASEID" => $diseaseid,
        "DISEASECODE"=>$diseasecode
    );
 
   // print_r(json_encode($data));  exit;
    $get_clientlist = APICall($data);
    $get_client_json = json_decode($get_clientlist, "true");

    if ($get_client_json['STATUSCODE'] == "000") {

        $message = array(
            "code" => "000",
            "message" => $get_client_json['STATUSMSG'],
        );
    } else {
        // echo "dddd";
        $message = array(
            "code" => $get_client_json['STATUSCODE'],
            "message" => $get_client_json['STATUSMSG'],
        );
    }

    echo json_encode($message);
    die();
}


