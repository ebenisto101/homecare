<?php
include_once("./includes/includes.php");

if(!isset($_SESSION)){
    session_start();
}

$data1 = array(
    "method" => "ASSIGNEDPATIENTS",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "USERID"=>$_SESSION['myMM_Userid']
);
//print_r(json_encode($data1));
$get_clientlist1 = APICall($data1);
$get_client_json = json_decode($get_clientlist1, "true");

//print_r($get_client_json);
?>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                           <div class="iq-header-title">
                              <h4 class="card-title">Patient List</h4>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php if($get_client_json['STATUSCODE']=="000"){
                     
                     foreach($get_client_json['RESULTS'] AS $patients){?>
                  <div class="col-sm-6 col-md-3">
                     <div class="iq-card">
                        <div class="iq-card-body text-center">
                           <div class="doc-profile">
                              <img class="rounded-circle img-fluid avatar-80" src="assets/images/user/09.jpg" alt="profile">
                           </div>
                           <div class="iq-doc-info mt-3">
                              <h4> <?php echo $patients['surname']." ".$patients['firstnames']." ".$patients['othernames']; ?></h4>
                              <p class="mb-0" ><?php echo $patients['card_number']; ?></p>
                              <a href="javascript:void();"><?php echo $patients['email'];?></a>
                           </div>
                           <!-- <div class="iq-doc-description mt-2">
                              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam auctor non erat non gravida. In id ipsum consequat</p>
                           </div> -->
                           <!-- <div class="iq-doc-social-info mt-3 mb-3">
                              <ul class="m-0 p-0 list-inline">
                                 <li><a href="#"><i class="ri-facebook-fill"></i></a></li>
                                 <li><a href="#"><i class="ri-twitter-fill"></i></a> </li>
                                 <li><a href="#"><i class="ri-google-fill"></i></a></li>
                              </ul>
                           </div>    -->
                           <a onclick="routeTrigger('viewPatient','<?php echo $patients['patientID']; ?>')" class="btn btn-primary">View Profile</a>
                        </div>
                     </div>
                  </div>
                <?php } }?>
                  
               </div>
           