<?php
include_once("./includes/includes.php");

$data = array(
    "method" => "LISTAGENCY",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD
);
$get_agencyList = APICall($data);
$get_agencyList_json = json_decode($get_agencyList, "true");
//print_r($get_client_json);
?>

<link href="<?php echo BASE_URL; ?>/assets/vendors/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

<div class="iq-card">
    <div class="iq-card-header d-flex justify-content-between">
        <div class="iq-header-title">
            <h4 class="card-title"> List of Agencies</h4>
        </div>
        <div class="iq-header-toolbar">
            <button type="button" class="btn btn-outline-success mb-3" onclick="routeTrigger('<?php echo $action ?>')" id="ref">
                <i class="fa fa-refresh"></i>Refresh</button> &nbsp; &nbsp;
            <button type="button" class="btn btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('CreateAgency','', 'test_wrapper_results2')">
                <i class="fa fa-plus"></i>Add Agency</button>
        </div>
    </div>
    <div class="iq-card-body">
        <table class="table table-responsive" id="mytable" style="width: 100%;">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">Email</th>
                    <th scope="col">Tel</th>
                    <th scope="col">Country</th>
                    <th scope="col">Date Added</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($get_agencyList_json['STATUSCODE'] == "000") {
                    $count = 0;
                    foreach ($get_agencyList_json['RESULTS'] as $clientData) {
                        $count = $count + 1;
                ?>
                        <tr>
                            <th scope="row"><?php echo $count; ?></th>
                            <td><?php echo ucwords(strtolower($clientData['agency_name'])); ?></td>
                            <td><?php echo $clientData['agency_address']. " ".$clientData['agency_location'] ?></td>
                            <td><?php echo $clientData['email']; ?></td>
                            <td><?php echo $clientData['tel']; ?></td>
                            <td><?php echo $clientData['country']; ?></td>

                            <td><?php echo date('j F Y g:i:s a', strtotime($clientData['date_added'])) ?></td>
                            <td style="cursor: pointer">
                                <?php
                                if ($clientData['status'] == "Active") { ?>
                                    <span class='btn mb-1 iq-bg-success'><?php echo  ucfirst("Active") ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                    <span class='btn mb-1 iq-bg-danger'><?php echo ucfirst('Inactive') ?></span>
                                <?php } ?>
                               
                            </td>
                            <td>                                      
                                <span data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg"  onclick="ShowDetails('editAgency','<?php echo $clientData['id'] ?>', 'test_wrapper_results2')"><i class="fa fa-edit" style="color: #81c91d; cursor: pointer"></i></span>
                                &nbsp; | &nbsp;
                                <span><i class="fa fa-trash" style="color: #FF416C; cursor: pointer" onclick="getstepDelete('deleteAgency','<?php echo $clientData['id'] ?>')"></i></span>

                            </td>
                        </tr>
                <?php  }
                } ?>

            </tbody>
        </table>

    </div>
</div>



<div class="modal fade bd-example-modal-lg" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="test_wrapper_results2">

        </div>
    </div>
</div>

<script src="<?php echo BASE_URL; ?>/assets/vendors/datatables/datatables.bundle.js" type="text/javascript"></script>

<script type="text/javascript">
      $(document).ready( function () {
    $('#mytable').DataTable();
} )
   </script>