<?php
include_once("./includes/includes.php");


$action = $_POST['act'];
$id = $_POST['id'];


if($action=="SaveAgency"){

    $name = $_POST['name'];
    $email = $_POST['email'];
    $tel = $_POST['tel'];
    $address = $_POST['address'];
    $location = $_POST['city'];
    $state = $_POST['state'];
    $country = $_POST['country'];


$data = array(
    "method" => "SAVEAGENCY",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "NAME"=>$name,
    "EMAIL"=>$email,
    "TEL"=>$tel,
    "ADDRESS"=>$address,
    "LOCATION"=>$location,
    "STATE"=>$state,
    "COUNTRY"=>$country
);
$get_agencyList = APICall($data);
$get_agencyList_json = json_decode($get_agencyList, "true");
//print_r($get_agencyList_json);


if($get_agencyList_json['STATUSCODE'] == '000'){
    $message = array(
        "code" => "000",
        "message" => "Agency successfuly added",
        "results"=>$get_agencyList_json['AGENCYID']
    );
}else{
   // echo "dddd";
    $message = array(
        "code" => $data['STATUSCODE'],
        "message" => $data['STATUSMSG'],
    );
}

echo json_encode($message);
die();
}



// update agency detaiils

if($action=="UpdateAgency"){

    $name = $_POST['name'];
    $email = $_POST['email'];
    $tel = $_POST['tel'];
    $address = $_POST['address'];
    $location = $_POST['city'];
    $state = $_POST['state'];
    $country = $_POST['country'];


$data = array(
    "method" => "UPDATEAGENCY",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "NAME"=>$name,
    "EMAIL"=>$email,
    "TEL"=>$tel,
    "ADDRESS"=>$address,
    "LOCATION"=>$location,
    "STATE"=>$state,
    "COUNTRY"=>$country,
    "AGENCYID"=>$id
);
$get_agencyList = APICall($data);
$get_agencyList_json = json_decode($get_agencyList, "true");
//print_r($get_agencyList_json);


if($get_agencyList_json['STATUSCODE'] == '000'){
    $message = array(
        "code" => "000",
        "message" => "Agency successfuly added",
        "results"=>$get_agencyList_json['AGENCYID']
    );
}else{
   // echo "dddd";
    $message = array(
        "code" => $data['STATUSCODE'],
        "message" => $data['STATUSMSG'],
    );
}

echo json_encode($message);
die();
}


if($action=="deleteAgency"){

    $data = array(
    "method" => "DELETEAGENCY",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "AGENCYID"=>$id
);
$get_agencyList = APICall($data);
$get_agencyList_json = json_decode($get_agencyList, "true");
//print_r($get_agencyList_json);


if($get_agencyList_json['STATUSCODE'] == '000'){
    $message = array(
        "code" => "000",
        "message" => "Agency successfuly added",
        "results"=>$get_agencyList_json['AGENCYID']
    );
}else{
   // echo "dddd";
    $message = array(
        "code" => $data['STATUSCODE'],
        "message" => $data['STATUSMSG'],
    );
}

echo json_encode($message);
die();
}


?>