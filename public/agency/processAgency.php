<?php
include_once("./includes/includes.php");

if(!isset($_SESSION)){
    session_start();
}

$action = $_POST['act'];
$id = $_POST['id'];
$id2 = $_POST['idd2'];
$userID = $_SESSION['myMM_Userid'];
$AgencyID = $_SESSION['agencyID'];

if($action=="assignPatient")
{
    
    $data = array(
        "method" => "ASSIGNPATIENT",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "EMPLOYEE"=>$id2,
        "PATIENT"=>$id
    );

    $get_agencyList = APICall($data);
    $get_agencyList_json = json_decode($get_agencyList, "true");
   // print_r($get_agencyList_json);

    if($get_agencyList_json['STATUSCODE'] == '000'){
        $message = array(
            "code" => "000",
            "message" => "Patient Assigned Successfully",
            
        );
    }else{
       // echo "dddd";
        $message = array(
            "code" => $data['STATUSCODE'],
            "message" => $data['STATUSMSG'],
        );
    }
    
    echo json_encode($message);
    die();

}

if($action=="patientAdd" || $action=="patientUpdate"){



$surnames = $_POST['surname'];
$othername = $_POST['othernames'];
$firstnames = $_POST['firstnames'];
$dateOfBirth = $_POST['dateOfBirth'];
$gender = $_POST['gender'];
$telephone = $_POST['tel'];
$email = $_POST['email'];
$residential_address = $_POST['residential_address'];
$nationality = $_POST['nationality'];

$relativename = $_POST['relative_name'];
$relative_tel = $_POST['relative_tel'];
$relative_relationship = $_POST['relative_relationship'];
$agency = $_POST['agency'];
$condition = $_POST['condition'];

if($action=="patientUpdate"){
    $method = "UPDATECLIENT";
    $patientID = $_POST['id'];

}

if($action=="patientAdd"){
    $method = "SAVECLIENT";
    $patientID=NULL;
}

$fields = array(
    'method'=>$method,
    "api_key"=> APIKEY,
    "user"=> USER,
    "passcode"=> PASSWORD,
    "SURNAME"=>$surnames,
    "OTHERNAMES"=>$othername,
    "FIRSTNAME"=>$firstnames,
    "DOB"=>$dateOfBirth,
    "GENDER"=>$gender,
    "TEL"=>$telephone,
    "EMAIL"=>$email,
    "NATIONALITY"=>$nationality,
    "RESIDENTIAL"=>$residential_address,
    "USERID"=>$userID,
    "RELATIVENAME"=>$relativename,
    "RELATIVETEL"=>$relative_tel,
    "RELATIVERELATIONSHIP"=>$relative_relationship,
    "AGENCY"=>$agency,
    "CONDITION"=>$condition,
    "PATIENTID"=>$patientID
    
    // "FILEPATH"=>$filepath
  );

 // print_r(json_encode($fields));
  //exit;
  $postData = APICall($fields);
  $get_PostData = json_decode($postData,"true");
  


if($get_PostData['STATUSCODE'] == '000'){
    $message = array(
        "code" => "000",
        "message" => "Patient Saved Successfully",
    );
}else{
   // echo "dddd";
    $message = array(
        "code" => $data['STATUSCODE'],
        "message" => $data['STATUSMSG'],
    );
}

echo json_encode($message);
die();

}

if($action=="updatePatientstatus"){
    
    $data = array(
        "method" => "UPDATEPATIENTSTATUS",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "STATUS"=>$id2,
        "PATIENTID"=>$id
    );

    $get_agencyList = APICall($data);
    $get_agencyList_json = json_decode($get_agencyList, "true");
   // print_r($get_agencyList_json);

    if($get_agencyList_json['STATUSCODE'] == '000'){
        $message = array(
            "code" => "000",
            "message" => "Patient Assigned Successfully",
            
        );
    }else{
       // echo "dddd";
        $message = array(
            "code" => $data['STATUSCODE'],
            "message" => $data['STATUSMSG'],
        );
    }
    
    echo json_encode($message);
    die();
}


if($action=="generateInvoice"){
   
    $data = array(
        "method" => "GENERATEVISITNO",
        "api_key" => APIKEY,
        "user" => USER,
        "passcode" => PASSWORD,
        "CLIENTID" => $id,
        "USER" => $userID,
        "AGENCYID" => $AgencyID
    );
    //print_r(json_encode($data));
    $get_encounterID = APICall($data);
    $get_visit_encounterID = json_decode($get_encounterID, "true");
    $visitData = $get_visit_encounterID['RESULTS'];

    print_r($get_visit_encounterID);
    
    if($get_visit_encounterID['STATUSCODE'] == '000'){
        $message = array(
            "code" => "000",
            "message" => "Invoice Generated Successfully",
            
        );
    }else{
       // echo "dddd";
        $message = array(
            "code" => $data['STATUSCODE'],
            "message" => $data['STATUSMSG'],
        );
    }
    
    echo json_encode($message);
    die();
}
?>