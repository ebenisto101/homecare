<?php
include_once("./includes/includes.php");

if(!isset($_SESSION)){
    session_start();
}
$action = $_POST['act'];
$id = $_POST['id'];

//GET EMPLOYEE DETAILS
$data = array(
    "method" => "GETEMPLOYEE",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
	"USERID"=>$id
);
//print_r(json_encode($data));
$get_employee = APICall($data);
$get_employeeJson = json_decode($get_employee, "true");
$employee = $get_employeeJson ['RESULTS'];
//print_r($get_employeeJson);
$assigned = $get_employeeJson['ASSIGNED'];

//END EMPLOYEE DETAILS

//GET LIST OF AGENCY EMPLOYEES
$data1 = array(
    "method" => "AGENCYUNASSIGNEDPATIENTS",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "AGENCYID"=>$_SESSION['agencyID'],
	"USERID"=>$id
);
$get_unassigned = APICall($data1);
$get_unassigned_json = json_decode($get_unassigned, "true");

//print_r($get_unassigned_json);

?>
<link href="<?php echo BASE_URL; ?>/assets/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>/assets/custom/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />


								<!--begin::Content container-->
							
									<!--begin::Layout-->
									<div class="d-flex flex-column flex-xl-row">
										<!--begin::Sidebar-->
										<div class="flex-column flex-lg-row-auto w-100 w-xl-350px mb-10">
											<!--begin::Card-->
											<div class="card mb-5 mb-xl-8">
												<!--begin::Card body-->
												<div class="card-body pt-15">
													<!--begin::Summary-->
													<div class="d-flex flex-center flex-column mb-5">
														<!--begin::Avatar-->
														<div class="symbol symbol-100px symbol-circle mb-7">
															<img src="assets/media/avatars/300-1.jpg" alt="image" />
														</div>
														<!--end::Avatar-->
														<!--begin::Name-->
														<a href="#" class="fs-3 text-gray-800 text-hover-primary fw-bold mb-1"><?php
														echo ucwords(strtolower($employee['firstnames']. " ".$employee['surname']));

														?></a>
														<!--end::Name-->
														<!--begin::Position-->
														<div class="fs-5 fw-semibold text-muted mb-6"><?php echo $employee['security'];?></div>
														<!--end::Position-->
														<!--begin::Info-->
														<div class="d-flex flex-wrap flex-center">
															<!--begin::Stats-->
															<div class="border border-gray-300 border-dashed rounded py-3 px-3 mb-3">
																<div class="fs-4 fw-bold text-gray-700">
																	<span class="w-75px"><?php echo $assigned['TOTALCOUNT'] ?></span>
																	<i class="ki-duotone ki-arrow-up fs-3 text-success">
																		<span class="path1"></span>
																		<span class="path2"></span>
																	</i>
																</div>
																<div class="fw-semibold text-muted">Patient(s)</div>
															</div>
															<!--end::Stats-->
															<!--begin::Stats-->
															<div class="border border-gray-300 border-dashed rounded py-3 px-3 mx-4 mb-3">
																<div class="fs-4 fw-bold text-gray-700">
																	<span class="w-50px">130</span>
																	<i class="ki-duotone ki-arrow-down fs-3 text-danger">
																		<span class="path1"></span>
																		<span class="path2"></span>
																	</i>
																</div>
																<div class="fw-semibold text-muted">Tasks</div>
															</div>
															<!--end::Stats-->
															<!--begin::Stats-->
															<div class="border border-gray-300 border-dashed rounded py-3 px-3 mb-3">
																<div class="fs-4 fw-bold text-gray-700">
																	<span class="w-50px">500</span>
																	<i class="ki-duotone ki-arrow-up fs-3 text-success">
																		<span class="path1"></span>
																		<span class="path2"></span>
																	</i>
																</div>
																<div class="fw-semibold text-muted">Hours</div>
															</div>
															<!--end::Stats-->
														</div>
														<!--end::Info-->
													</div>
													<!--end::Summary-->
													<!--begin::Details toggle-->
													<div class="d-flex flex-stack fs-4 py-3">
														<div class="fw-bold rotate collapsible" data-bs-toggle="collapse" href="#kt_customer_view_details" role="button" aria-expanded="false" aria-controls="kt_customer_view_details">Details
														<span class="ms-2 rotate-180">
															<i class="ki-duotone ki-down fs-3"></i>
														</span></div>
														<span data-bs-toggle="tooltip" data-bs-trigger="hover" title="Edit customer details">
															<!-- <a href="#" class="btn btn-sm btn-light-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_update_customer">Edit</a> -->
														</span>
													</div>
													<!--end::Details toggle-->
													<div class="separator separator-dashed my-3"></div>
													<!--begin::Details content-->
													<div id="kt_customer_view_details" class="collapse show">
														<div class="py-5 fs-6">
															<!--begin::Badge-->
															<!-- <div class="badge badge-light-info d-inline">Premium user</div> -->
															<!--begin::Badge-->
															<!--begin::Details item-->
															<div class="fw-bold mt-5">Account ID</div>
															<div class="text-gray-600">ID-<?php echo $id;?></div>
															<!--begin::Details item-->
															<!--begin::Details item-->
															<div class="fw-bold mt-5">Email</div>
															<div class="text-gray-600">
																<a href="#" class="text-gray-600 text-hover-primary"><?php echo $employee['email'] ?></a>
															</div>
															<div class="fw-bold mt-5">Tel</div>
															<div class="text-gray-600"><?php echo $employee['tel'];?></div>
															
															<div class="fw-bold mt-5">Active Since</div>
															<div class="text-gray-600"><?php echo date('j F Y g:i:s a', strtotime($employee['date_added']))?></div>
															
															<div class="fw-bold mt-5">Agency</div>
															<div class="text-gray-600"><?php echo $get_employeeJson['AGENCY']; ?></div>
															<!--begin::Details item-->
														</div>
													</div>
													<!--end::Details content-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Card-->
											
										</div>
										<!--end::Sidebar-->
										<!--begin::Content-->
										<div class="flex-lg-row-fluid ms-lg-15">
											<!--begin:::Tabs-->
											<ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-semibold mb-8">
												<!--begin:::Tab item-->
												<li class="nav-item">
													<a class="nav-link text-active-primary pb-4 active" data-bs-toggle="tab" href="#kt_customer_view_overview_tab">Assigned Clients</a>
												</li>
												<!--end:::Tab item-->
												<!--begin:::Tab item-->
												<li class="nav-item">
													<a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#kt_customer_view_overview_events_and_logs_tab">Assign Patient</a>
												</li>
												<!--end:::Tab item-->
												<!--begin:::Tab item-->
												
												<!--end:::Tab item-->
												<!--begin:::Tab item-->
												<li class="nav-item ms-auto">
													<!--begin::Action menu-->
													<a class="btn btn-outline btn-outline-dashed btn-outline-success btn-active-light-success" onclick="routeTrigger('<?php echo $action; ?>','<?php echo $id ?>')">Refresh</a>
													
												</li>
												<!--end:::Tab item-->
											</ul>
											<!--end:::Tabs-->
											<!--begin:::Tab content-->
											<div class="tab-content" id="myTabContent">
												<!--begin:::Tab pane-->
												<div class="tab-pane fade show active" id="kt_customer_view_overview_tab" role="tabpanel">
													<!--begin::Card-->
													<div class="card pt-4 mb-6 mb-xl-9">
														<!--begin::Card header-->
														<div class="card-header border-0">
															<!--begin::Card title-->
															<div class="card-title">
																<h2>List of Clients</h2>
															</div>
															<!--end::Card title-->
															<!--begin::Card toolbar-->
															
															<!--end::Card toolbar-->
														</div>
														<!--end::Card header-->
														<!--begin::Card body-->
														<div class="card-body pt-0 pb-5">
															<!--begin::Table-->
															<div class="table-responsive">
                <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="kt_table_1">
                    <thead>
                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                            <th class="w-10px pe-2">#</th>
                            <th class="min-w-125px">Client</th>
                            <th class="min-w-125px">Tel</th>
                            <th class="min-w-125px">Home Address</th>
                            <th class="min-w-125px">Status</th>
                            <!-- <th class="text-end min-w-100px">Actions</th> -->
                        </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-semibold">
                        <?php
                        if ($assigned['TOTALCOUNT']>0) {
                            $count = 0;
                            foreach ($assigned['ASSIGNEDCLIENTS'] as $clientData) {
                                $count = $count + 1;
                        ?>
                                <tr>
                                    <td>
                                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                                            <?php echo $count; ?>
                                        </div>
                                    </td>
                                    <td class="d-flex align-items-center">
                                        <!--begin:: Avatar -->
                                        <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                            <?php
                                            $arraylist = array("bg-light-danger text-danger", "bg-light-success text-success", "bg-light-warning text-warning","bg-light-info text-info");

                                            shuffle($arraylist);
                                            $classID = implode("", array_slice($arraylist, 0, 1));
                                            ?>

                                            <div class="symbol-label">
                                                <div class="symbol-label fs-3 <?php echo $classID; ?>"><?php echo substr($clientData['surname'], 0, 1); ?></div>
                                            </div>

                                        </div>
                                        <!--end::Avatar-->
                                        <!--begin::User details-->
                                        <div class="d-flex flex-column">
                                            <span onclick="routeTrigger('viewClient','<?php echo $clientData['id']; ?>')" class="text-gray-800 text-hover-primary mb-1"><?php echo ucwords(strtolower($clientData['surname'] . " " .$clientData['firstname']." ". $clientData['othernames'])); ?></span>
                                            <span><?php echo $clientData['email']; ?></span>
                                        </div>
                                        <!--begin::User details-->
                                    </td>
                                    <td><?php echo $clientData['tel'] ?></td>
                                   

                                    <td><?php echo $clientData['home_address']; ?></td>
                                    <td style="cursor: pointer">
                                        <?php
                                        if ($clientData['status'] == "1") { ?>
                                            <span class='btn btn-sm btn-light-success'><?php echo  ucfirst("Active") ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <?php } else { ?>
                                            <span class='btn btn-sm btn-light-danger'><?php echo ucfirst('Inactive') ?></span>
                                        <?php } ?>
                                    
                                    </td>
                                    <!-- <td>

                                        <span data-bs-toggle="modal" data-bs-target="#kt_modal_add_customer" onclick="ShowDetails('editEmployee','<?php echo $clientData['id'] ?>', 'test_wrapper_results2')"><i class="fa fa-edit" style="color: #81c91d; cursor: pointer"></i></span>
                                        &nbsp; | &nbsp;
                                        <span><i class="fa fa-trash" style="color: #FF416C; cursor: pointer" onclick="getstepDelete('deleteUser','<?php echo $clientData['id'] ?>')"></i></span>

                                    </td> -->
                                </tr>
                        <?php  }
                        }

                        ?>



                    </tbody>
                </table>
            </div>
															<!--end::Table-->
														</div>
														<!--end::Card body-->
													</div>
													<!--end::Card-->
													<!--begin::Card-->
													
													<!--end::Card-->
													<!--begin::Card-->
													
													<!--end::Card-->
													<!--begin::Card-->
													
													<!--end::Card-->
												</div>
												<!--end:::Tab pane-->
												<!--begin:::Tab pane-->
												<div class="tab-pane fade" id="kt_customer_view_overview_events_and_logs_tab" role="tabpanel">
													<!--begin::Card-->
													<div class="card pt-4 mb-6 mb-xl-9">
														<!--begin::Card header-->
														<div class="card-header border-0">
															<!--begin::Card title-->
															<div class="card-title">
																<h2>Patients</h2>
															</div>
															
														</div>
														
														<div class="card-body py-0">
														<div id="kt_table_users_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
															<div class="table-responsive">
																
															<table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="kt_table_2">
																	<tbody>

																	<?php 
																	$count= 0;
																	foreach($get_unassigned_json['RESULTS'] AS $patient){
																		$count  = $count + 1;
																		?>
																		<tr>
																			<td class="min-w-70px">
																				<div class="badge badge-light-success"><?php echo $count; ?></div>
																			</td>
																			<td><?php echo $patient['surname']." ".$patient['firstname']." ".$patient['othernames'];?></td>
																			<td class="pe-0 text-end min-w-200px"><span class="btn btn-active-light-success" onclick="saveInfo('assignPatient','<?php echo $patient['id']; ?>','<?php echo $id; ?>')">Assign</span></td>
																		</tr>
																		
																		<?php } ?>
																		
																		
																	</tbody>
																</table>
															</div>
															<!--end::Table wrapper-->
														</div>
														</div>
														<!--end::Card body-->
													</div>
													
												</div>
												
											</div>
											<!--end:::Tab content-->
										</div>
										<!--end::Content-->
									</div>
								
							
								<!--end::Content container-->
<script src="<?php echo BASE_URL; ?>/assets/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL; ?>/assets/custom/basic/scrollable.js" type="text/javascript"></script>