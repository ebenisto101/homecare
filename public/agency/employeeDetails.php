<?php
include_once("./includes/includes.php");

if (!isset($_SESSION)) {
	session_start();
}
$action = $_POST['act'];
$id = $_POST['id'];

//GET EMPLOYEE DETAILS
$data = array(
	"method" => "GETEMPLOYEE",
	"api_key" => APIKEY,
	"user" => USER,
	"passcode" => PASSWORD,
	"USERID" => $id
);
//print_r(json_encode($data));
$get_employee = APICall($data);
$get_employeeJson = json_decode($get_employee, "true");
$employee = $get_employeeJson['RESULTS'];
//print_r($get_employeeJson);
$assigned = $get_employeeJson['ASSIGNED'];

//END EMPLOYEE DETAILS

//GET LIST OF AGENCY EMPLOYEES
$data1 = array(
	"method" => "AGENCYUNASSIGNEDPATIENTS",
	"api_key" => APIKEY,
	"user" => USER,
	"passcode" => PASSWORD,
	"AGENCYID" => $_SESSION['agencyID'],
	"USERID" => $id
);
$get_unassigned = APICall($data1);
$get_unassigned_json = json_decode($get_unassigned, "true");

//print_r($get_unassigned_json);

?>
<link href="<?php echo BASE_URL; ?>/assets/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>/assets/custom/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />


<!--begin::Content container-->

<div class="row" style="overflow:scroll;">
	<div class="col-lg-3">
		<div class="iq-card">
			<div class="iq-card-body ps-0 pe-0 pt-0">
				<div class="docter-details-block">
					<div class="doc-profile-bg bg-primary" style="height:150px;">
					</div>
					<div class="docter-profile text-center">
						<img src="assets/images/user/11.png" alt="profile-img" class="avatar-130 img-fluid">
					</div>
					<div class="text-center mt-3 ps-3 pe-3">
						<h4><b><?php echo ucwords(strtolower($employee['firstnames'] . " " . $employee['surname'])); ?></b></h4>
						<p><?php echo $employee['security']; ?></p>
						<hr>
						<p class="mb-0"><h4 class="counter"><?php echo $assigned['TOTALCOUNT']." Patients" ?></h4></p>
					</div>
					
					<!-- <ul class="doctoe-sedual d-flex align-items-center justify-content-between p-0 m-0">
						<li class="text-center">
							<h3 class="counter"><?php echo $assigned['TOTALCOUNT'] ?></h3>
							<span>Patients</span>
						</li>
						<li class="text-center">
							<h3 class="counter">100</h3>
							<span>Hospital</span>
						</li>
						<li class="text-center">
							<h3 class="counter">10000</h3>
							<span>Patients</span>
						</li>
					</ul> -->
				</div>
			</div>
		</div>
		<div class="iq-card">
			<div class="iq-card-header d-flex justify-content-between">
				<div class="iq-header-title">
					<h4 class="card-title">Personal Information</h4>
				</div>
			</div>
			<div class="iq-card-body">
				<div class="about-info m-0 p-0">
					<div class="row">
						<div class="col-4">Name:</div>
						<div class="col-8"><?php echo $employee['surname']." ".$employee['firstnames']; ?></div>
						<div class="col-4">Position:</div>
						<div class="col-8"><?php echo $employee['security']; ?></div>
						<div class="col-4">Email:</div>
						<div class="col-8"><a href="mailto:<?php echo $employee['email']; ?>"> <?php echo $employee['email']; ?> </a></div>
						<div class="col-4">Phone:</div>
						<div class="col-8"><a href="tel:<?php echo $employee['tel']; ?>"><?php echo $employee['tel']; ?></a></div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="col-lg-9">

		<div class="row">
			<div class="col-md-12">
				<div class="iq-card">
					<div class="iq-card-header d-flex justify-content-between">
						<div class="iq-header-title">
							<h4 class="card-title">Activities</h4>
						</div>
						<div class="iq-header-toolbar">

							<!--begin::Action menu-->
							<a class="btn btn-outline btn-outline-dashed btn-outline-success btn-active-light-success" onclick="routeTrigger('<?php echo $action; ?>','<?php echo $id ?>')">Refresh</a>


						</div>
					</div>
					<div class="iq-card-body">
						<ul class="nav nav-pills mb-3 nav-fill" id="pills-tab-1" role="tablist">
							<li class="nav-item" role="presentation">
								<a class="nav-link active" id="pills-home-tab-fill" data-bs-toggle="pill" href="#pills-home-fill" role="tab" aria-controls="pills-home" aria-selected="true">Assigned Patients</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link" id="pills-profile-tab-fill" data-bs-toggle="pill" href="#pills-profile-fill" role="tab" aria-controls="pills-profile" aria-selected="false" tabindex="-1">Assign Patient</a>
							</li>
							<li class="nav-item" role="presentation">
                                 <a class="nav-link" id="pills-contact-tab-fill" data-bs-toggle="pill" href="#pills-contact-fill" role="tab" aria-controls="pills-contact" aria-selected="false" tabindex="-1">Work Sessions</a>
                              </li>
						</ul>
						<div class="tab-content" id="pills-tabContent-1">
							<div class="tab-pane fade active show" id="pills-home-fill" role="tabpanel" aria-labelledby="pills-home-tab-fill">
								<table class="table table-responsive" id="kt_table_1" style="width: 100%;">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Name</th>
											<!-- <th scope="col">Email</th> -->
											<th scope="col">Tel</th>
											<th scope="col">Home Address</th>
											<th scope="col">Status</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ($assigned['TOTALCOUNT'] > 0) {
											$count = 0;
											foreach ($assigned['ASSIGNEDCLIENTS'] as $clientData) {
												$count = $count + 1;
										?>
												<tr>
													<th scope="row"><?php echo $count; ?></th>
													<td><?php echo ucwords(strtolower($clientData['surname'] . " " . $clientData['firstnames']." ".$clientData['othernames'])); ?></td>
													<!-- <td><?php //echo $clientData['email']; ?></td> -->
													<td><?php echo $clientData['tel'] ?></td>

													<td><?php echo $clientData['home_address']; ?></td>
													<td style="cursor: pointer">
														<?php
														if ($clientData['status'] == "1") { ?>
															<span class='btn mb-1 iq-bg-success'><?php echo  ucfirst("Active") ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
														<?php } else { ?>
															<span class='btn mb-1 iq-bg-danger'><?php echo ucfirst('Inactive') ?></span>
														<?php } ?>
													</td>
													<td>
														<span onclick="routeTrigger('viewClient','<?php echo $clientData['id'] ?>', '');return false;"><i class="fa fa-eye" style="color: #81c91d; cursor: pointer"></i></span>

													</td>
												</tr>
										<?php  }
										} ?>

									</tbody>
								</table>
							</div>
							<div class="tab-pane fade" id="pills-profile-fill" role="tabpanel" aria-labelledby="pills-profile-tab-fill">
							<table class="table table-responsive" id="kt_table_1" style="width: 100%;">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Name</th>
											<th scope="col">Tel</th>
											<th scope="col">Home Address</th>
											<th scope="col">Status</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php

										if($get_unassigned_json['STATUSCODE']=="000"){
										$count= 0;
										
										foreach($get_unassigned_json['RESULTS'] AS $patient){
											$count  = $count + 1;
											?>
												<tr>
													<th scope="row"><?php echo $count; ?></th>
													<td><?php echo ucwords(strtolower($patient['surname'] . " " . $patient['firstname']." ".$patient['othernames'])); ?></td>
													<td><?php echo $patient['tel'] ?></td>

													<td><?php echo $patient['home_address']; ?></td>
													<td style="cursor: pointer">
														<?php
														if ($patient['status'] == "1") { ?>
															<span class='btn mb-1 iq-bg-success'><?php echo  ucfirst("Active") ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
														<?php } else { ?>
															<span class='btn mb-1 iq-bg-danger'><?php echo ucfirst('Inactive') ?></span>
														<?php } ?>
													</td>
													<td>
														<span class="btn btn-outline btn-outline-success btn-active-light-success" onclick="saveInfo('assignPatient','<?php echo $patient['id'] ?>', '<?php echo $id; ?>')">Assign</span>

													</td>
												</tr>
										<?php  } }else{?>
											<tr>
												<td colspan="6"><?php echo $get_unassigned_json['STATUSMSG']; ?></td>
											</tr>
										<?php }
										 ?>

									</tbody>
								</table>
							</div>
							<div class="tab-pane fade" id="pills-contact-fill" role="tabpanel" aria-labelledby="pills-contact-tab-fill">
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!--end::Content container-->
<script src="<?php echo BASE_URL; ?>/assets/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL; ?>/assets/custom/basic/scrollable.js" type="text/javascript"></script>