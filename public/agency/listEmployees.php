<?php
include_once("./includes/includes.php");

if(!isset($_SESSION)){
    session_start();
}

$data1 = array(
    "method" => "LISTEMPLOYEES",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "AGENCYID"=>$_SESSION['agencyID']
);
$get_clientlist1 = APICall($data1);
$get_client_json = json_decode($get_clientlist1, "true");
//print_r($get_client_json);
//print_r($data1);
?>
<link href="<?php echo BASE_URL; ?>/assets/vendors/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>/assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />


<div class="iq-card">
    <div class="iq-card-header d-flex justify-content-between">
        <div class="iq-header-title">
            <h4 class="card-title"> List of Employees</h4>
        </div>
        <div class="iq-header-toolbar">
            <button type="button" class="btn btn-outline-success mb-3" onclick="routeTrigger('<?php echo $action ?>')" id="ref">
                <i class="fa fa-refresh"></i>Refresh</button> &nbsp; &nbsp;
            <button type="button" class="btn btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('addEmployee','', 'test_wrapper_results2')">
                <i class="fa fa-plus"></i>Add Employee</button>
        </div>
    </div>
    <div class="iq-card-body">
        <table class="table table-responsive" id="kt_table_1" style="width: 100%;">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">User</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Last login</th>
                    <th scope="col">Joined Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($get_client_json['STATUSCODE'] == "000") {
                    $count = 0;
                    foreach ($get_client_json['RESULTS'] as $clientData) {
                        $count = $count + 1;
                ?>
                        <tr>
                            <th scope="row"><?php echo $count; ?></th>
                            <td><?php echo ucwords(strtolower($clientData['surname'] . " " . $clientData['firstnames'])); ?></td>
                            <td><?php echo $clientData['email']; ?></td>
                            <td><?php echo $clientData['security'] ?></td>
                            <td>
                                <div class="btn mb-1 iq-bg-primary"><?php echo timeAgo($clientData['lastlogindate']); ?></div>
                            </td>

                            <td><?php echo date('j F Y g:i:s a', strtotime($clientData['date_added'])) ?></td>
                            <td style="cursor: pointer">
                                <?php
                                if ($clientData['status'] == "1") { ?>
                                    <span class='btn mb-1 iq-bg-success'><?php echo  ucfirst("Active") ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                    <span class='btn mb-1 iq-bg-danger'><?php echo ucfirst('Inactive') ?></span>
                                <?php } ?>
                                &nbsp;||&nbsp; <span class="btn btn-outline-info mb-3" onclick="getstep('userPermssion','<?php echo $clientData['id'] ?>')">Permissions</span>
                            </td>
                            <td>

                                <span data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg"  onclick="ShowDetails('editEmployee','<?php echo $clientData['id'] ?>', 'test_wrapper_results2')"><i class="fa fa-edit" style="color: #6777ef; cursor: pointer"></i></span>
                                &nbsp; | &nbsp;
                                <span><i class="fa fa-trash" style="color: red; cursor: pointer" onclick="getstepDelete('deleteUser','<?php echo $clientData['id'] ?>')"></i></span> &nbsp; | &nbsp;<span><i style="color: #6777ef; cursor: pointer" class="fa fa-eye" onclick="routeTrigger('viewEmployee','<?php echo $clientData['id']; ?>')"></i></span>

                            </td>
                        </tr>
                <?php  }
                } ?>

            </tbody>
        </table>

    </div>
</div>


<div class="modal fade bd-example-modal-lg" id="" tabindex="-1" style="display: none;" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-lg">
        <!--begin::Modal content-->
        <div class="modal-content" id="test_wrapper_results2">

        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>


<script src="<?php echo BASE_URL; ?>/assets/vendors/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL; ?>/assets/vendors/basic/scrollable.js" type="text/javascript"></script>