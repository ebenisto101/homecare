<?php
include_once("./includes/includes.php");

if (!isset($_SESSION)) {
    session_start();
}

$data1 = array(
    "method" => "LISTAGENCYPATIENTS",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "AGENCYID" => $_SESSION['agencyID']
);
$get_clientlist1 = APICall($data1);
$get_client_json = json_decode($get_clientlist1, "true");
//print_r($get_client_json);
//print_r($data1);
?>

<style type="text/css">
    .switch {
        position: relative;
        display: block;
        vertical-align: top;
        width: 70px;
        height: 30px;
        padding: 3px;
        margin: 0 10px 10px 0;
        background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
        background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
        border-radius: 18px;
        box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
        cursor: pointer;
    }

    .switch-input {
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
    }

    .switch-label {
        position: relative;
        display: block;
        height: inherit;
        font-size: 10px;
        text-transform: uppercase;
        background: #eceeef;
        border-radius: inherit;
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
    }

    .switch-label:before,
    .switch-label:after {
        position: absolute;
        top: 50%;
        margin-top: -.5em;
        line-height: 1;
        -webkit-transition: inherit;
        -moz-transition: inherit;
        -o-transition: inherit;
        transition: inherit;
    }

    .switch-label:before {
        content: attr(data-off);
        right: 11px;
        color: #aaaaaa;
        text-shadow: 0 1px rgba(255, 255, 255, 0.5);
    }

    .switch-label:after {
        content: attr(data-on);
        left: 11px;
        color: #FFFFFF;
        text-shadow: 0 1px rgba(0, 0, 0, 0.2);
        opacity: 0;
    }

    .switch-input:checked~.switch-label {
        background: #00ca00;
        border-color: #00ca00;
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
    }

    .switch-input:checked~.switch-label:before {
        opacity: 0;
    }

    .switch-input:checked~.switch-label:after {
        opacity: 1;
    }

    .switch-handle {
        position: absolute;
        top: 4px;
        left: 4px;
        width: 28px;
        height: 28px;
        background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
        background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
        border-radius: 100%;
        box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
    }

    .switch-handle:before {
        content: "";
        position: absolute;
        top: 50%;
        left: 50%;
        margin: -6px 0 0 -6px;
        width: 12px;
        height: 12px;
        background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
        background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
        border-radius: 6px;
        box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
    }

    .switch-input:checked~.switch-handle {
        left: 44px;
        box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
    }

    /* Transition
========================== */
    .switch-label,
    .switch-handle {
        transition: All 0.3s ease;
        -webkit-transition: All 0.3s ease;
        -moz-transition: All 0.3s ease;
        -o-transition: All 0.3s ease;
    }
</style>
<link href="<?php echo BASE_URL; ?>/assets/vendors/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>/assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />


<div class="iq-card">
    <div class="iq-card-header d-flex justify-content-between">
        <div class="iq-header-title">
            <h4 class="card-title"> List of Patients</h4>
        </div>
        <div class="iq-header-toolbar">
            <button type="button" class="btn btn-outline-success mb-3" onclick="routeTrigger('<?php echo $action ?>')" id="ref">
                <i class="fa fa-refresh"></i>Refresh</button> &nbsp; &nbsp;
            <button type="button" class="btn btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('addPatient','', 'test_wrapper_results2')">
                <i class="fa fa-plus"></i>Add User</button>
        </div>
    </div>
    <div class="iq-card-body">
        <table class="table table-responsive" id="mypatienttable" style="width: 100%;">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Tel</th>
                    <th scope="col">Home Addres</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($get_client_json['STATUSCODE'] == "000") {
                    $count = 0;
                    foreach ($get_client_json['RESULTS'] as $clientData) {
                        $count = $count + 1;
                ?>
                        <tr>
                            <th scope="row"><?php echo $count; ?></th>
                            <td><?php echo ucwords(strtolower($clientData['surname'] . " " . $clientData['firstname'] . " " . $clientData['othernames'])); ?></td>
                            <td><?php echo $clientData['email']; ?></td>
                            <td><?php echo $clientData['tel'] ?></td>
                            <td><?php echo $clientData['home_address'] ?></td>
                            <td style="cursor: pointer">

                                <label class="switch">
                                    <input value="<?php if ($clientData['patient_status'] == "Active") { ?>On<?php } else { ?>Off<?php } ?>" <?php if ($clientData['patient_status'] == "Active") { ?>checked="checked" <?php } ?> onchange="saveInfo('updatePatientstatus','<?php echo $clientData['id']; ?>',this.value)" name="status" class="switch-input" type="checkbox" />
                                    <span class="switch-label" data-on="Active" data-off="Inactive"></span>
                                    <span class="switch-handle"></span>
                    </label>                               
                            </td>
                            <td>

                                <span onclick="routeTrigger('viewClient','<?php echo $clientData['id']; ?>')"><i class="fa fa-eye" style="color: #81c91d; cursor: pointer"></i></span>&nbsp;| &nbsp; <span data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('editPatient','<?php echo $clientData['id']; ?>', 'test_wrapper_results2')"><i class="fa fa-edit" style="color: #81c91d; cursor: pointer"></i></span>&nbsp;
    </div>

    </td>
    </tr>
<?php  }
                } ?>

</tbody>
</table>

</div>
</div>



<div class="modal fade bd-example-modal-xl" tabindex="-1" style="display: none;" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-xl">
        <!--begin::Modal content-->
        <div class="modal-content" id="test_wrapper_results2">

        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>


<script src="<?php echo BASE_URL; ?>/assets/vendors/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL; ?>/assets/vendors/basic/scrollable.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#mypatienttable').DataTable();
    })
</script>