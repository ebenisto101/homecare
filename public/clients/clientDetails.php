<?php
include_once("./includes/includes.php");

if (!isset($_SESSION)) {
    session_start();
}
$action = $_POST['act'];
$id = $_POST['id'];

//GET COUNTRYLIST
$data = array(
    "method" => "GETPATIENT",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "PATIENTID" => $id
);
//print_r(json_encode($data));
$get_employee = APICall($data);
$get_employeeJson = json_decode($get_employee, "true");
$employee = $get_employeeJson['RESULTS'];
//print_r($get_employeeJson);
$assigned = $get_employeeJson['ASSIGNED'];
?>
<link href="<?php echo BASE_URL; ?>/assets/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>/assets/custom/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />


<!--begin::Content container-->

<div class="row" style="overflow:scroll;">
    <div class="col-lg-3">
        <div class="iq-card">
            <div class="iq-card-body ps-0 pe-0 pt-0">
                <div class="docter-details-block">
                    <div class="doc-profile-bg bg-primary" style="height:150px;">
                    </div>
                    <div class="docter-profile text-center">
                        <img src="assets/images/user/11.png" alt="profile-img" class="avatar-130 img-fluid">
                    </div>
                    <div class="text-center mt-3 ps-3 pe-3">
                        <h4><b><?php echo ucwords(strtolower($employee['firstname'] . " " . $employee['surname'])); ?></b></h4>
                        <p><?php echo $employee['security']; ?></p>
                        <hr>
                        <p class="mb-0">
                        <h6 class="counter"><?php echo $employee['condition']; ?></h6>
                        </p>
                    </div>

                    <ul class="doctoe-sedual d-flex align-items-center justify-content-between p-0 m-0">
                        <li class="text-center">
                            <h3 class="counter"><?php echo $assigned['TOTALCOUNT'] ?></h3>
                            <span>Care Giver(s)</span>
                        </li>
                        <!-- <li class="text-center">
							<h3 class="counter">100</h3>
							<span>Hospital</span>
						</li>
						<li class="text-center">
							<h3 class="counter">10000</h3>
							<span>Patients</span>
						</li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
                <div class="iq-header-title">
                    <h4 class="card-title">Personal Information</h4>
                </div>
            </div>
            <div class="iq-card-body">
                <div class="about-info m-0 p-0">
                    <div class="row">
                        <div class="col-4">Name:</div>
                        <div class="col-8"><?php echo $employee['surname'] . " " . $employee['firstname']; ?></div>
                        <div class="col-4">Agency:</div>
                        <div class="col-8"><?php echo $get_employeeJson['AGENCY']; ?></div>
                        <div class="col-4">Email:</div>
                        <div class="col-8"><a href="mailto:<?php echo $employee['email']; ?>"> <?php echo $employee['email']; ?> </a></div>
                        <div class="col-4">Phone:</div>
                        <div class="col-8"><a href="tel:<?php echo $employee['tel']; ?>"><?php echo $employee['tel']; ?></a></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-lg-9">

        <div class="row">
            <div class="col-md-12">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Activities</h4>
                        </div>
                        <div class="iq-header-toolbar">

                            <!--begin::Action menu-->
                            <a class="btn btn-outline btn-outline-dashed btn-outline-success btn-active-light-success" onclick="routeTrigger('<?php echo $action; ?>','<?php echo $id ?>')">Refresh</a>


                        </div>
                    </div>
                    <div class="iq-card-body">
                        <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab-1" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="pills-home-tab-fill" data-bs-toggle="pill" href="#pills-home-fill" role="tab" aria-controls="pills-home" aria-selected="true">Assigned Carers</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-profile-tab-fill" data-bs-toggle="pill" href="#pills-profile-fill" role="tab" aria-controls="pills-profile" aria-selected="false" tabindex="-1">Medical Record</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-bills-tab-fill" data-bs-toggle="pill" href="#pills-bills-fill" role="tab" aria-controls="pills-bills" aria-selected="false" tabindex="-1">Patient Bills</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="pills-tabContent-1">
                            <div class="tab-pane fade active show" id="pills-home-fill" role="tabpanel" aria-labelledby="pills-home-tab-fill">
                                <table class="table table-responsive" id="kt_table_1" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <!-- <th scope="col">Email</th> -->
                                            <th scope="col">Tel</th>
                                            <th scope="col">Home Address</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($assigned['TOTALCOUNT'] > 0) {
                                            $count = 0;
                                            foreach ($assigned['ASSIGNEDCLIENTS'] as $clientData) {
                                                $count = $count + 1;
                                        ?>
                                                <tr>
                                                    <th scope="row"><?php echo $count; ?></th>
                                                    <td><?php echo ucwords(strtolower($clientData['surname'] . " " . $clientData['firstnames'] . " " . $clientData['othernames'])); ?></td>
                                                    <!-- <td><?php //echo $clientData['email']; 
                                                                ?></td> -->
                                                    <td><?php echo $clientData['tel'] ?></td>

                                                    <td><?php echo $clientData['home_address']; ?></td>
                                                    <td style="cursor: pointer">
                                                        <?php
                                                        if ($clientData['status'] == "1") { ?>
                                                            <span class='btn mb-1 iq-bg-success'><?php echo  ucfirst("Active") ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <?php } else { ?>
                                                            <span class='btn mb-1 iq-bg-danger'><?php echo ucfirst('Inactive') ?></span>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <span onclick="routeTrigger('viewClient','<?php echo $clientData['id'] ?>', '');return false;"><i class="fa fa-eye" style="color: #81c91d; cursor: pointer"></i></span>

                                                    </td>
                                                </tr>
                                        <?php  }
                                        } ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="pills-profile-fill" role="tabpanel" aria-labelledby="pills-profile-tab-fill">
                                <div class="row">
                                    <div class="iq-card">
                                        <!-- <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">Nursing Care</h4>
                    </div>
                </div> -->
                                        <div class="iq-card-body">

                                            <div class="row">
                                                <div class="col-sm-3" style="border-style: 15px solid red;">
                                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                        <a class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-home" aria-selected="true">Patient Vitals</a>
                                                        <a class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-profile" aria-selected="false" tabindex="-1">Nursing Notes</a>
                                                        <a class="nav-link" id="v-pills-messages-tab" data-bs-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-messages" aria-selected="false" tabindex="-1">Treatment Sheet</a>
                                                        <a class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-settings" aria-selected="false" tabindex="-1">Glucose Monitoring</a>
                                                        <a class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-settings" aria-selected="false" tabindex="-1">Urine Monitoring</a>
                                                        <a class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" href="#v-pills-6" role="tab" aria-controls="v-pills-settings" aria-selected="false" tabindex="-1">Patient Drugs</a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="tab-content mt-0" id="v-pills-tabContent">
                                                        <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
                                                            <!-- LIST VITALS --->
                                                            <?php

                                                            $vitalsData = array(
                                                                "method" => "GETPATIENTVITALS",
                                                                "api_key" => APIKEY,
                                                                "user" => USER,
                                                                "passcode" => PASSWORD,
                                                                "PATIENTID" => $id,
                                                                "OFFSET" => "5"
                                                            );

                                                            //print_r(json_encode($vitalsData));
                                                            $get_vitalsData = APICall($vitalsData);
                                                            $get_VitalsJson = json_decode($get_vitalsData, "true");
                                                            ?>
                                                            <div class="iq-card">
                                                                <div class="iq-card-header d-flex justify-content-between">
                                                                    <div class="iq-header-title">
                                                                        <h4 class="card-title">Patient Vitals</h4>
                                                                    </div>
                                                                    <div class="iq-header-toolbar">
                                                                        <!-- <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewVital','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>&nbsp; | &nbsp; -->
                                                                        <button type="button" class="btn btn-sm me-2 btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewallvitals','<?php echo $id; ?>', 'test_wrapper_results3')">
                                                                            <i class="fa fa-eye"></i>View All</button>
                                                                    </div>
                                                                </div>
                                                                <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                                                    <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">#</th>
                                                                                <th scope="col">Date Added</th>
                                                                                <th scope="col">User</th>
                                                                                <th scope="col">Weight</th>
                                                                                <th scope="col">Height</th>
                                                                                <th scope="col">Temp</th>
                                                                                <th scope="col">Pulse</th>
                                                                                <th scope="col">Resp</th>
                                                                                <th scope="col">Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            if ($get_VitalsJson['STATUSCODE'] == "000") {
                                                                                $count = 0;
                                                                                foreach ($get_VitalsJson['RESULTS'] as $vitals) {
                                                                                    $count = $count + 1;
                                                                            ?>
                                                                                    <tr>
                                                                                        <th scope="row"><?php echo $count; ?></th>
                                                                                        <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($vitals['date'])) ?></span></td>
                                                                                        <td><?php echo  ucwords(strtolower(getUser($vitals['user_id']))); ?></td>
                                                                                        <td><?php echo $vitals['weight'] ?></td>
                                                                                        <td><?php echo $vitals['height'] ?></td>
                                                                                        <td><?php echo $vitals['temperature'] ?></td>
                                                                                        <td><?php echo $vitals['pulse'] ?></td>
                                                                                        <td><?php echo $vitals['resp'] ?></td>
                                                                                        <td style="cursor: pointer"><span data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewVitals','<?php echo $vitals['id']; ?>','test_wrapper_results3')"><i class="fa fa-eye" style="color: #81c91d; cursor: pointer"></i></span>

                                                                                        </td>
                                                                                    </tr>
                                                                            <?php  }
                                                                            } ?>

                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
                                                            <?php

                                                            $notesData = array(
                                                                "method" => "GETPATIENTNOTES",
                                                                "api_key" => APIKEY,
                                                                "user" => USER,
                                                                "passcode" => PASSWORD,
                                                                "PATIENTID" => $id,
                                                                "OFFSET" => "5"
                                                            );

                                                            //print_r(json_encode($vitalsData));
                                                            $get_notesData = APICall($notesData);
                                                            $get_notesJson = json_decode($get_notesData, "true");
                                                            ?>
                                                            <div class="iq-card">
                                                                <div class="iq-card-header d-flex justify-content-between">
                                                                    <div class="iq-header-title">
                                                                        <h4 class="card-title">Nurses Notes</h4>
                                                                    </div>
                                                                    <div class="iq-header-toolbar">
                                                                        <!-- <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewNote','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>&nbsp; | &nbsp; -->
                                                                        <button type="button" class="btn btn-sm me-2 btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewallnotess','<?php echo $id; ?>', 'test_wrapper_results3')">
                                                                            <i class="fa fa-eye"></i>View All</button>
                                                                    </div>
                                                                </div>
                                                                <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                                                    <table class="table table-responsive" id="mypatienttable" style="width: 100%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">#</th>
                                                                                <th scope="col">Date Added</th>
                                                                                <th scope="col">Note</th>
                                                                                <th scope="col">Status</th>
                                                                                <!-- <th scope="col">Action</th> -->

                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            if ($get_notesJson['STATUSCODE'] == "000") {
                                                                                $count = 0;
                                                                                foreach ($get_notesJson['RESULTS'] as $notes) {
                                                                                    $count = $count + 1;
                                                                            ?>
                                                                                    <tr>
                                                                                        <th scope="row"><?php echo $count; ?></th>
                                                                                        <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($notes['date_added'])) ?></span></td>
                                                                                        <td><?php echo substr_replace($notes['note'], "...", 50); ?> <a style="color:blue;cursor:pointer" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('viewNote','<?php echo $notes['id']; ?>','test_wrapper_results2')">read more</a></td>
                                                                                        <td><small><span><i>Owner</i>:<?php echo getUser($notes['user_id']) . "(" . date('jS F Y g:i a', strtotime($notes['date_added'])) . ")"; ?></span><br /><span><i>Last update:</i><?php
                                                                                                                                                                                                                                                                            $valueDate = getUser($notes['updated_user']) . "(" . date('jS F Y g:i a', strtotime($notes['last_updated'])) . ")";
                                                                                                                                                                                                                                                                            echo ($notes['last_updated'] == '' || $notes['last_updated'] == "00-00-00" || is_null($notes['last_updated'])) ? 'N/A' : $valueDate;
                                                                                                                                                                                                                                                                            //echo $valueDate12;
                                                                                                                                                                                                                                                                            ?></span></small></td>
                                                                                        <!-- <td><?php if ($notes['user_id'] == $_SESSION['myMM_Userid'] && date('Y-m-d', strtotime($notes['date_added'])) == date("Y-m-d")) { ?><span data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('editNote','<?php echo $notes['id']; ?>','test_wrapper_results2')"><i class="fa fa-edit"></i></span><?php } ?></td> -->

                                                                                    </tr>
                                                                                <?php  }
                                                                            } else { ?>
                                                                                <tr>
                                                                                    <td colspan="5">No Records Found</td>
                                                                                </tr>
                                                                            <?php } ?>

                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
                                                            <?php

                                                            $treatmentData = array(
                                                                "method" => "GETPATIENTTREATMENTS",
                                                                "api_key" => APIKEY,
                                                                "user" => USER,
                                                                "passcode" => PASSWORD,
                                                                "PATIENTID" => $id,
                                                                "OFFSET" => "5"
                                                            );

                                                            //print_r(json_encode($vitalsData));
                                                            $get_treatmentData = APICall($treatmentData);
                                                            $get_treatmentJson = json_decode($get_treatmentData, "true");
                                                            //print_r($get_treatmentJson);
                                                            ?>
                                                            <div class="iq-card">
                                                                <div class="iq-card-header d-flex justify-content-between">
                                                                    <div class="iq-header-title">
                                                                        <h4 class="card-title">Treatment Chart</h4>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                                                    <table class="table table-responsive" id="mypatienttable" style="width: 100%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">#</th>
                                                                                <th scope="col">Date</th>
                                                                                <th scope="col">Time</th>
                                                                                <th scope="col">Status</th>
                                                                                <th scope="col">Action</th>

                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            if ($get_treatmentJson['STATUSCODE'] == "000") {
                                                                                $count = 0;
                                                                                foreach ($get_treatmentJson['RESULTS'] as $treatments) {
                                                                                    $count = $count + 1;
                                                                            ?>
                                                                                    <tr>
                                                                                        <th scope="row"><?php echo $count; ?></th>
                                                                                        <td><span class='badge border border-success text-success'><?php echo date('j F Y', strtotime($treatments['date_recorded'])) ?></span></td>
                                                                                        <td><?php echo $treatments['item']; ?></td>
                                                                                        <td><?php echo $treatments['timesRecorded'] ?></td>
                                                                                        <!-- <td><?php if ($notes['user_id'] == $_SESSION['myMM_Userid'] && date('Y-m-d', strtotime($notes['date_added'])) == date("Y-m-d")) { ?><span data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('editNote','<?php echo $notes['id']; ?>','test_wrapper_results2')"><i class="fa fa-edit"></i></span><?php } ?></td> -->

                                                                                    </tr>
                                                                                <?php  }
                                                                            } else { ?>
                                                                                <tr>
                                                                                    <td colspan="5">No Records Found</td>
                                                                                </tr>
                                                                            <?php } ?>

                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
                                                            <!-- LIST VITALS --->
                                                            <?php

                                                            $glucoseData = array(
                                                                "method" => "GETPATIENTGLUCOSE",
                                                                "api_key" => APIKEY,
                                                                "user" => USER,
                                                                "passcode" => PASSWORD,
                                                                "PATIENTID" => $id,
                                                                "OFFSET" => "ALL"
                                                            );

                                                            //print_r(json_encode($vitalsData));
                                                            $get_glucoseData = APICall($glucoseData);
                                                            $get_glucoseJson = json_decode($get_glucoseData, "true");
                                                            //print_r($get_glucoseJson);
                                                            ?>
                                                            <div class="iq-card">
                                                                <div class="iq-card-header d-flex justify-content-between">
                                                                    <div class="iq-header-title">
                                                                        <h4 class="card-title">Glucose Monitoring</h4>
                                                                    </div>
                                                                    <div class="iq-header-toolbar">
                                                                        <!-- <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewGlucose','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>&nbsp; | &nbsp; -->
                                                                        <!-- <button type="button" class="btn btn-sm me-2 btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-xl" onclick="ShowDetails('viewallvitals','<?php echo $id; ?>', 'test_wrapper_results3')">
                                                    <i class="fa fa-eye"></i>View All</button> -->
                                                                    </div>
                                                                </div>
                                                                <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                                                    <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">#</th>
                                                                                <th scope="col">Date Time</th>
                                                                                <th scope="col">FBS</th>
                                                                                <th scope="col">RBS</th>
                                                                                <th scope="col">Remark</th>
                                                                                <th scope="col">User</th>

                                                                                <!-- <th scope="col">Action</th> -->
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            if ($get_glucoseJson['STATUSCODE'] == "000") {
                                                                                $count = 0;
                                                                                foreach ($get_glucoseJson['RESULTS'] as $glucose) {
                                                                                    $count = $count + 1;
                                                                            ?>
                                                                                    <tr>
                                                                                        <th scope="row"><?php echo $count; ?></th>
                                                                                        <td><?php echo date('j F Y g:i:a', strtotime($glucose['date_recorded'])); ?><span class='badge border border-success text-success'><?php echo $glucose['time_recorded'] ?></span></td>

                                                                                        <td><?php echo $glucose['fbs'] ?></td>
                                                                                        <td><?php echo $glucose['rbs'] ?></td>
                                                                                        <td><?php echo $glucose['remarks'] ?></td>
                                                                                        <td><?php echo  ucwords(strtolower(getUser($glucose['user_id']))); ?></td>


                                                                                        </td>
                                                                                    </tr>
                                                                            <?php  }
                                                                            } ?>

                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5-tab">
                                                            <?php

                                                            $urineData = array(
                                                                "method" => "GETPATIENTURINE",
                                                                "api_key" => APIKEY,
                                                                "user" => USER,
                                                                "passcode" => PASSWORD,
                                                                "PATIENTID" => $id,
                                                                "OFFSET" => "ALL"
                                                            );

                                                            //print_r(json_encode($vitalsData));
                                                            $get_urineData = APICall($urineData);
                                                            $get_urineJson = json_decode($get_urineData, "true");
                                                            //print_r($get_glucoseJson);
                                                            ?>
                                                            <div class="iq-card">
                                                                <div class="iq-card-header d-flex justify-content-between">
                                                                    <div class="iq-header-title">
                                                                        <h4 class="card-title">Urine Monitoring</h4>
                                                                    </div>
                                                                    <div class="iq-header-toolbar">
                                                                       
                                                                    </div>
                                                                </div>
                                                                <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                                                    <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">#</th>
                                                                                <th scope="col">Date Time</th>
                                                                                <th scope="col">Protein</th>
                                                                                <th scope="col">Sugar</th>
                                                                                <th scope="col">Weight</th>
                                                                                <th scope="col">Kerotones</th>
                                                                                <th scope="col">Other</th>
                                                                                <th scope="col">User</th>

                                                                                <!-- <th scope="col">Action</th> -->
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            if ($get_urineJson['STATUSCODE'] == "000") {
                                                                                $count = 0;
                                                                                foreach ($get_urineJson['RESULTS'] as $urine) {
                                                                                    $count = $count + 1;
                                                                            ?>
                                                                                    <tr>
                                                                                        <th scope="row"><?php echo $count; ?></th>
                                                                                        <td><?php echo date('j F Y g:i:a', strtotime($urine['date_recorded'])); ?><span class='badge border border-success text-success'><?php echo $urine['time_recorded'] ?></span></td>

                                                                                        <td><?php echo $urine['protein'] ?></td>
                                                                                        <td><?php echo $urine['sugar'] ?></td>
                                                                                        <td><?php echo $urine['weight'] ?></td>
                                                                                        <td><?php echo $urine['kerotones'] ?></td>
                                                                                        <td><?php echo $urine['other'] ?></td>
                                                                                        <td><?php echo  ucwords(strtolower(getUser($urine['user_id']))); ?></td>


                                                                                        </td>
                                                                                    </tr>
                                                                            <?php  }
                                                                            } ?>

                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-6-tab">
                                                            <?php

                                                            $DrugsData = array(
                                                                "method" => "GETPATIENTDRUGS",
                                                                "api_key" => APIKEY,
                                                                "user" => USER,
                                                                "passcode" => PASSWORD,
                                                                "PATIENTID" => $id,
                                                                "OFFSET" => "ALL"
                                                            );

                                                            //print_r(json_encode($vitalsData));
                                                            $get_drugsData = APICall($DrugsData);
                                                            $get_DrugsJson = json_decode($get_drugsData, "true");
                                                            ?>
                                                            <div class="iq-card">
                                                                <div class="iq-card-header d-flex justify-content-between">
                                                                    <div class="iq-header-title">
                                                                        <h4 class="card-title">Patient Vitals</h4>
                                                                    </div>
                                                                    <div class="iq-header-toolbar">
                                                                        <!-- <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('AddNewDrug','<?php echo $id; ?>', 'test_wrapper_results2')">
                                                    <i class="fa fa-plus"></i>Add NEW</button> -->
                                                                    </div>
                                                                </div>
                                                                <div class="iq-card-body" style="max-height: 350px; overflow:scroll">
                                                                    <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">#</th>
                                                                                <th scope="col">Date Added</th>
                                                                                <th scope="col">User</th>
                                                                                <th scope="col">Prescrition</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            if ($get_DrugsJson['STATUSCODE'] == "000") {
                                                                                $count = 0;
                                                                                foreach ($get_DrugsJson['RESULTS'] as $drugs) {
                                                                                    $count = $count + 1;
                                                                            ?>
                                                                                    <tr>
                                                                                        <th scope="row"><?php echo $count; ?></th>
                                                                                        <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($drugs['date_added'])) ?></span></td>
                                                                                        <td><?php echo  ucwords(strtolower(getUser($drugs['user_id']))); ?></td>
                                                                                        <td><small>
                                                                                                <div class="alert alert-primary" role="alert">
                                                                                                    <div class="iq-alert-text"><?php echo $drugs['item'] . ":" . $drugs['dose'] . " " . $drugs['dosage_form'] . " " . $drugs['frequency'] . " " . $drugs['no_of_days'] . " days"; ?></div>
                                                                                                </div>
                                                                                            </small></td>

                                                                                    </tr>
                                                                            <?php  }
                                                                            } ?>

                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-bills-fill" role="tabpanel" aria-labelledby="pills-bills-tab-fill">
                                <div class="row">
                                    <div class="iq-card">
                                        <div class="iq-card-header d-flex justify-content-between">
                                            <div class="iq-header-title">
                                            <!-- <h4 class="card-title"> Bills</h4> -->
                                            </div>
                                            <div class="iq-header-toolbar">
                                                <button type="button" class="btn btn-sm me-2 btn-outline-success mb-3"  onclick="saveInfo('generateInvoice','<?php echo $id; ?>', '')">
                                                    <i class="fa fa-plus"></i>Add NEW</button>
                                            </div>
                                        </div>
                                        <?php
                                    $InvoiceData = array(
                                        "method" => "GETPATIENTINVOICES",
                                        "api_key" => APIKEY,
                                        "user" => USER,
                                        "passcode" => PASSWORD,
                                        "PATIENTID" => $id,
                                    );

                                   // print_r(json_encode($InvoiceData));
                                    $get_InvoiceData = APICall($InvoiceData);
                                    $get_InvoiceJson = json_decode($get_InvoiceData, "true");
                                    //print_r($get_InvoiceJson);
                                
                                        ?>
                                    <table class="table table-responsive-sm" id="mypatienttable" style="width: 100%;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">#</th>
                                                                                <th scope="col">Date Added</th>
                                                                                <th scope="col">Visit No</th>
                                                                                <th scope="col">Status</th>
                                                                                <th scope="col">User</th>
                                                                                <th scope="col">Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            if ($get_InvoiceJson['STATUSCODE'] == "000") {
                                                                                $count = 0;
                                                                                foreach ($get_InvoiceJson['RESULTS'] as $invoice) {
                                                                                    $count = $count + 1;
                                                                            ?>
                                                                                    <tr>
                                                                                        <th scope="row"><?php echo $count; ?></th>
                                                                                        <td><span class='badge border border-success text-success'><?php echo date('j F Y g:i:a', strtotime($invoice['datetimeadded'])) ?></span></td>
                                                                                        <td><?php echo $invoice['process_claim_no']; ?></td>
                                                                                        <td><?php echo $invoice['status'] ?></td>
                                                                                        <td><?php echo  ucwords(strtolower(getUser($invoice['user_id']))); ?></td>
                                                                                        <td><div class="btn-group" role="group" aria-label="Button group with nested dropdown">

                                 <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle show" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Dropdown
                                    </button>
                                    <div class="dropdown-menu show" aria-labelledby="btnGroupDrop1" data-popper-placement="bottom-start" style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate3d(0px, 36.8px, 0px);">
                                       <a class="dropdown-item" href="#">Dropdown link</a>
                                       <a class="dropdown-item" href="#">Dropdown link</a>
                                    </div>
                                 </div>
                              </div></td>

                                                                                    </tr>
                                                                            <?php  }
                                                                            } ?>

                                                                        </tbody>
                                                                    </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" style="display: none;" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-lg">
        <!--begin::Modal content-->
        <div class="modal-content" id="test_wrapper_results2">

        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>

<div class="modal fade bd-example-modal-xl" tabindex="-1" style="display: none;" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-xl">
        <!--begin::Modal content-->
        <div class="modal-content" id="test_wrapper_results3">

        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>

<!--end::Content container-->
<script src="<?php echo BASE_URL; ?>/assets/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL; ?>/assets/custom/basic/scrollable.js" type="text/javascript"></script>