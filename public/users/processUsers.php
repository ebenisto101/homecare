<?php
include_once("./includes/includes.php");

if(!isset($_SESSION)){
    session_start();
}


$action = $_POST['act'];
$id = $_POST['id'];

if($action=="UserAdd"){
    $surname = $_POST['surname'];
    $firstnames = $_POST['firstnames'];
    $tel = $_POST['tel'];
    $email = $_POST['user_email'];
    $agency = $_POST['agency'];
    $user_role = $_POST['user_role'];


    
$data = array(
    "method" => "ADDUSER",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "SURNAME"=>$surname,
    "FIRSTNAME"=>$firstnames,
    "EMAIL"=>$email,
    "TEL"=>$tel,
    "AGENCY"=>$agency,
    "USERROLE"=>$user_role

);

$addUser = APICall($data);
$get_addUser_json = json_decode($addUser, "true");
//print_r($get_agencyList_json);


if($get_addUser_json['STATUSCODE'] == '000'){
    $message = array(
        "code" => "000",
        "message" => "User successfuly added",
        "results"=>$get_addUser_json['USERID']
    );
}else{
   // echo "dddd";
    $message = array(
        "code" => $data['STATUSCODE'],
        "message" => $data['STATUSMSG'],
    );
}

echo json_encode($message);
die();
}


if($action=="SaveUserEdit"){
    $surname = $_POST['surname'];
    $firstnames = $_POST['firstnames'];
    $tel = $_POST['tel'];
    $email = $_POST['user_email'];
    $agency = $_POST['agency'];
    $user_role = $_POST['user_role'];


    
$data = array(
    "method" => "UPDATEUSER",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD,
    "SURNAME"=>$surname,
    "FIRSTNAME"=>$firstnames,
    "EMAIL"=>$email,
    "TEL"=>$tel,
    "AGENCY"=>$agency,
    "USERROLE"=>$user_role,
    "USERID"=>$id

);

$addUser = APICall($data);
$get_addUser_json = json_decode($addUser, "true");
//print_r($get_agencyList_json);


if($get_addUser_json['STATUSCODE'] == '000'){
    $message = array(
        "code" => "000",
        "message" => "User successfuly added",
        "results"=>$get_addUser_json['USERID']
    );
}else{
   // echo "dddd";
    $message = array(
        "code" => $data['STATUSCODE'],
        "message" => $data['STATUSMSG'],
    );
}

echo json_encode($message);
die();
}


if ($_POST['act'] == "change_exp_pass")  {

    if($_POST['new_exp_pass'] == $_POST['confirm_exp_pass']){

        $fields = array(
            "method" => "CHANGEPASSWORD",
            "api_key" => APIKEY,
            "user" => USER,
            "passcode" => PASSWORD,
            'USERID' => $_SESSION['myMM_Userid'],
            'OLDPASSWORD' => $_POST['exp_pass'],
            'NEWPASSWORD' => $_POST['confirm_exp_pass']
        );
    
        $change_pass =APICall($fields);
        $pass_changed = json_decode($change_pass, "true");

        //print_r($pass_changed);
    
        $stat_code = $pass_changed['STATUSCODE'];
        $stat_msg = $pass_changed['STATUSMSG'];
    
        echo $message = json_encode(
            array(
                "code" => $stat_code,
                "message" => $stat_msg
            )
        );

    } else {
        echo $message = json_encode(
            array(
                "code" => '002',
                "message" => 'Passwords do not match'
            )
        );
    }
}
?>