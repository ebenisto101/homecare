<?php 

//include_once('accesscheck2.php'); 
require_once('includes/includes.php'); 
if (!isset($_SESSION)){
    session_start();
  }
?>
<?php

//mysql_select_db($database_courier, $courier);
$id=$_POST['id'];
	


//getPermission Groups
$data1= array(
    "method"=>"SECURITYLEVELS",
    "api_key"=> APIKEY,
    "user"=> USER,
    "passcode"=> PASSWORD
);
$get_clientlist1 = APICall($data1);
$get_client_json = json_decode($get_clientlist1,"true");

//print_r($get_client_json);

$userpermid=$id;

?>

<form name="form" id="form" method="post" onsubmit="javascript:getstep('insertuserpermissions'); return false;">
<div class="iq-card">
    <div class="iq-card-header d-flex justify-content-between">
        <div class="iq-header-title">
        User Permission  for <?php echo strtoupper(getUser($id));?>
                <input name="id4" type="hidden" id="id4" value="<?php echo $id; ?>" />
        </div>
        <div class="iq-header-toolbar">
            <button type="button" class="btn btn-outline-success mb-3" onclick="routeTrigger('<?php echo $action ?>','<?php echo $id; ?>')" id="ref">
                <i class="fa fa-refresh"></i>Refresh</button> &nbsp; &nbsp;
            <button type="button" class="btn btn-info mb-3" onclick="routeTrigger('listUsers')">
                <i class="fa fa-plus"></i>List Of Users</button>
        </div>
    </div>
</div>
    <div class="row">
        	<?php
			foreach($get_client_json['RESULTS'] AS $row_rsodest){?>
            <div class="col-md-4">
                <div class="iq-card iq-card--height-fluid">
                    <div class="iq-card-header d-flex justify-content-between">
                            <h4 class="iq-header-title">
                               <?php echo $row_rsodest['menu_name']; ?>
                            </h4>
                    </div>
                    
                    <div class="iq-card-body">
                        <!--begin::k-widget4-->
                        <?php
							  $menuplace=$row_rsodest['menu_place'];
                                //getsubmenus
                                $field= array(
                                    "method"=>"SUBSECURITYLEVELS",
                                    "api_key"=> APIKEY,
                                    "user"=> USER,
                                    "passcode"=> PASSWORD,
                                    "MENUPLACE"=>$menuplace
                                );
                                $get_submenu = APICall($field);
                                $get_submenu_json = json_decode($get_submenu,"true");


							foreach($get_submenu_json['RESULTS'] AS $row_rsodest_sub) {  ?>
                            <div class="form-group">
                                 <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="item<?php echo $row_rsodest_sub['id'];?>" id="item<?php echo $row_rsodest_sub['id'];?>" value="<?php echo $row_rsodest_sub['id'];?>" <?php if(getUserPermid($row_rsodest_sub['id'],$id)=='yes'){?>checked="checked" <?php }?>>
                                    <label class="custom-control-label" for="customCheck1"><?php echo $row_rsodest_sub['menu_name']; ?></label>
                                 </div>
                                 
                              </div>
						<?php }?>
                        <!--end::Widget 9-->
                    </div>
                </div>
                
            </div>

            <?php } ?>
            <div class="text-center pt-15">
                <input type="reset" class="btn btn-light me-3" value="Cancel" data-bs-dismiss="modal">
                <input type="button" class="btn btn-primary" value="Save" onclick="saveInfo('saveUrine','<?php echo $id ?>','');return false;">
            </div>
        </div>
</form>

