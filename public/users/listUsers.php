<?php
include_once("./includes/includes.php");

$data1 = array(
    "method" => "LISTUSERS",
    "api_key" => APIKEY,
    "user" => USER,
    "passcode" => PASSWORD
);
$get_clientlist1 = APICall($data1);
$get_client_json = json_decode($get_clientlist1, "true");
//print_r($get_client_json);
?>
<link href="<?php echo BASE_URL; ?>/assets/vendors/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>/assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

<div class="iq-card">
    <div class="iq-card-header d-flex justify-content-between">
        <div class="iq-header-title">
            <h4 class="card-title"> List of Users</h4>
        </div>
        <div class="iq-header-toolbar">
            <button type="button" class="btn btn-outline-success mb-3" onclick="routeTrigger('<?php echo $action ?>')" id="ref">
                <i class="fa fa-refresh"></i>Refresh</button> &nbsp; &nbsp;
            <button type="button" class="btn btn-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('addUser','', 'test_wrapper_results2')">
                <i class="fa fa-plus"></i>Add User</button>
        </div>
    </div>
    <div class="iq-card-body">
        <table class="table table-responsive" id="kt_table_1" style="width: 100%;">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">User</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Last login</th>
                    <!-- <th scope="col">Joined Date</th> -->
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($get_client_json['STATUSCODE'] == "000") {
                    $count = 0;
                    foreach ($get_client_json['RESULTS'] as $clientData) {
                        $count = $count + 1;
                ?>
                        <tr>
                            <th scope="row"><?php echo $count; ?></th>
                            <td><?php echo ucwords(strtolower($clientData['surname'] . " " . $clientData['firstnames'])); ?></td>
                            <td><?php echo $clientData['email']; ?></td>
                            <td><?php echo $clientData['security'] ?></td>
                            <td>
                                <div class="btn mb-1 iq-bg-primary"><?php echo timeAgo($clientData['lastlogindate'])." ago"; ?></div>
                            </td>

                            <!-- <td><?php //echo date('j F Y g:i:s a', strtotime($clientData['date_added'])) ?></td> -->
                            <td style="cursor: pointer">
                                <?php
                                if ($clientData['status'] == "1") { ?>
                                    <span class='btn mb-1 iq-bg-success'><?php echo  ucfirst("Active") ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <?php } else { ?>
                                    <span class='btn mb-1 iq-bg-danger'><?php echo ucfirst('Inactive') ?></span>
                                <?php } ?>
                                &nbsp;||&nbsp; 
                                <!-- <span class="btn btn-outline-info mb-3" data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg" onclick="ShowDetails('userPermssion','<?php //echo $clientData['id'] ?>','test_wrapper_results2')">Permissions</span> -->
                            </td>
                            <td>

                                <span data-bs-toggle="modal" data-bs-target=".bd-example-modal-lg"  onclick="ShowDetails('editUser','<?php echo $clientData['id'] ?>', 'test_wrapper_results2')"><i class="fa fa-edit" style="color: #81c91d; cursor: pointer"></i></span>
                                &nbsp; | &nbsp;
                                <span><i class="fa fa-trash" style="color: #FF416C; cursor: pointer" onclick="getstepDelete('deleteUser','<?php echo $clientData['id'] ?>')"></i></span>

                            </td>
                        </tr>
                <?php  }
                } ?>

            </tbody>
        </table>

    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="test_wrapper_results2">



        </div>
    </div>
</div>

<script src="<?php echo BASE_URL; ?>/assets/vendors/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL; ?>/assets/vendors/basic/scrollable.js" type="text/javascript"></script>