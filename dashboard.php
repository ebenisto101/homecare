<div class="row">
                  <div class="col-sm-12">
                     <div class="row">
                        <div class="col-md-6 col-lg-3">
                           <div class="iq-card">
                              <div class="iq-card-body">
                                 <div class="iq-progress-bar progress-bar-vertical iq-bg-primary">
                                       <span class="bg-primary" data-percent="70"></span>
                                   </div>
                                 <span class="line-height-4"><?php echo date("j F Y");?></span>
                                 <h4 class="mb-2 mt-2">No of Patients</h4>
                                 <p class="mb-0 text-secondary line-height">2</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                           <div class="iq-card iq-card-block iq-card-height">
                              <div class="iq-card-body">
                                 <div class="iq-progress-bar progress-bar-vertical iq-bg-danger">
                                       <span class="bg-danger" data-percent="50"></span>
                                   </div>
                                 <span class="line-height-4"><?php echo date("j F Y");?></span>
                                 <h4 class="mb-2 mt-2">No. of Carers</h4>
                                 <p class="mb-0 text-secondary line-height">5</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                           <div class="iq-card">
                              <div class="iq-card-body">
                                 <div class="iq-progress-bar progress-bar-vertical iq-bg-warning">
                                       <span class="bg-warning" data-percent="80"></span>
                                   </div>
                                 <span class="line-height-4"><?php echo date("j F Y");?></span>
                                 <h4 class="mb-2 mt-2">Hypertensive Crisis</h4>
                                 <p class="mb-0 text-secondary line-height">Examination</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                           <div class="iq-card iq-card-block iq-card-height">
                              <div class="iq-card-body P-0 rounded" style="background: url(assets/images/page-img/38.jpg) no-repeat scroll center center; background-size: contain; min-height: 146px;">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>            
               </div>