<?php 
$id = $_POST["id"];
$action = $_POST["act"];

//-- begin users

if($action=="listUsers"){
	include_once("public/users/listUsers.php");
}

if($action=="addUser"){
	include_once("public/modals/modalContents.php");
}

if($action=="UserAdd"){
	include_once("public/users/processUsers.php");

}

if($action=="editUser"){
	include_once("public/modals/modalContents.php");
}

if($action=="SaveUserEdit"){
	include_once("public/users/processUsers.php");
}

if($action=="change_exp_pass"){
	include_once("public/users/processUsers.php");
}

if($action=="userPermssion"){
	include_once("public/modals/modalContents.php");
}
// -- end users --- 

// ---- begin config ------
if($action=="listAgency"){
	include_once("public/config/listAgency.php");
}

if($action=="CreateAgency"){
	include_once("public/modals/modalContents.php");
}

if($action=="SaveAgency" || $action=="UpdateAgency" || $action=="deleteAgency"){
	include_once("public/config/processConfig.php");
}


if($action=="editAgency"){
	include_once("public/modals/modalContents.php");
}

if($action=="deleteAgency"){
	include_once("public/config/processConfig.php");
}

//--- end config ------

// ---- begin agency -----

if($action=="listEmployees"){
	include_once("public/agency/listEmployees.php");
}

if($action=="addEmployee" || $action=="editEmployee"){
	include_once("public/modals/modalContents.php");
}

if($action=="viewEmployee"){
	include_once("public/agency/employeeDetails.php");
}

if($action=="listPatients"){
	include_once("public/agency/listPatients.php");
}

if($action=="viewClient"){
	include_once("public/clients/clientDetails.php");
}

if($action=="assignPatient"){
	include_once("public/agency/processAgency.php");
}

if($action=="addPatient" || $action=="editPatient"){
	include_once("public/modals/modalContents.php");
}

if($action=="patientAdd" || $action=="patientUpdate" || $action=="updatePatientstatus"){
	include_once("public/agency/processAgency.php");
}

if($action=="generateInvoice"){
	include_once("public/agency/processAgency.php");
}
// end agency //

// --- nursing --- //
if($action=="mypatients"){
	include_once("public/nursing/mypatients.php");
}

if($action=="viewPatient"){
	include_once("public/nursing/patientDetails.php");
}

if($action=="addAllergy"){
	include_once("public/modals/modalContents.php");
}

if($action=="savePatientAllergy"){
	include_once("public/nursing/processPatients.php");
}

if($action=="viewVitals" || $action=="viewallvitals"){
	include_once("public/modals/modalContents.php");
}

if($action=="AddNewVital"){
	include_once("public/modals/modalContents.php");
}

if($action=="saveVital"){
	include_once("public/nursing/processPatients.php");
}

if($action=="AddNewNote"){
	include_once("public/modals/modalContents.php");	
}

if($action=="saveNote" || $action=="UpdateNote"){
	include_once("public/nursing/processPatients.php");

}

if($action=="viewNote" || $action=="editNote"){
	include_once("public/modals/modalContents.php");

}

if($action=="viewallnotess"){
	include_once("public/modals/modalContents.php");
}

if($action=="AddNewDrug"){
	include_once("public/modals/modalContents.php");
}

if($action=="savePrescription" || $action=="saveGlucose"){
	include_once("public/nursing/processPatients.php");
}

if($action=="AddNewTreatment" || $action=="AddNewGlucose" || $action=="AddNewUrine"){
	include_once("public/modals/modalContents.php");
}

if($action=="saveTreatment" || $action=="saveUrine"){
	include_once("public/nursing/processPatients.php");
}

if($action=="addDiagnosis"){
	include_once("public/modals/modalContents.php");
}

if($action=="saveDiagnosis"){
	include_once("public/nursing/processPatients.php");
}
