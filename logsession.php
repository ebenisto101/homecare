<?php
 require_once('config/dbconnections.php'); 
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  
 //mysql_select_db($database_courier, $courier); 
  $machineip = getenv("REMOTE_ADDR");
  
 $user_id=  $_SESSION['myMM_Userid'];
 $loginUsername =  $_SESSION['myMM_Username'];
 $fullname=  $_SESSION['myMM_Fullname']; 
$shift_period=  $_SESSION['shift_period'];
$provider_id=  $_SESSION['provider_id'];
$provider_id_master=  $_SESSION['provider_id_master'];
 
	 
$query_rscarmodel11 = "INSERT INTO `shift_user_log` (`id`, `user_id`, `username`, `fullname`, `shift_period`, `logout_time`, `user_ip_address`, `provider_id`, `provider_id_master`) VALUES (NULL, '$user_id', '$loginUsername', '$fullname', '$shift_period', CURRENT_TIMESTAMP, '$machineip', '$provider_id', '$provider_id_master');";
$rscarmodel11 = $conn->query($query_rscarmodel11);
  
  $_SESSION['claimno'] = NULL;
  $_SESSION['myMM_Username'] = NULL;
  $_SESSION['myMM_UserGroup'] = NULL;
  $_SESSION['myMM_Fullname'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  $_SESSION['myMM_Userid']= NULL;
  $_SESSION['security']= NULL;
   $_SESSION['myMM_Programme']= NULL;
  $_SESSION['myMM_Year']= NULL;
  $_SESSION['myMM_UGPG']= NULL;
  	$_SESSION['myMM_Password'] = NULL;
	$_SESSION['myMM_Option'] = NULL;
     unset($_SESSION['claimno']);
   unset($_SESSION['myMM_Year']);
  unset($_SESSION['myMM_Programme']);
   unset($_SESSION['security']);
  unset($_SESSION['myMM_Userid']);
  unset($_SESSION['myMM_Fullname']);
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
 unset($_SESSION['myMM_Password']);
	unset($_SESSION['myMM_Option']);
	unset($_SESSION['access_token']);
	unset($_SESSION['email']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>