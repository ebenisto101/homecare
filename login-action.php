<?php require_once('includes/includes.php');

// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

//echo "meee";
$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

$MM_fldUserAuthorization = "";
$MM_redirectLoginSuccess = "index.php";
$MM_redirectLoginFailed = "index.php?error";
$MM_redirectLoginRecap = "index.php?errorCap";
$MM_redirecttoReferrer = false;

if (isset($_POST['email'])) {
  //&& !empty($_POST['g-recaptcha-response']
  $loginUsername = $_POST['email'];
  $password = md5(sha1($_POST['password']));
  $recaptcha = $_POST['g-recaptcha-response']; 
  //exit();

  $LoginRS__query = "SELECT * FROM users_accounts WHERE email=? AND password=? and status='1'";
  $LoginRS = $conn->prepare($LoginRS__query);
  $LoginRS->execute(array($loginUsername, $password));
  $row_LoginRS = $LoginRS->fetch(PDO::FETCH_ASSOC);
  //print_r($row_LoginRS);exit;
  $loginFoundUser = $LoginRS->rowCount();

  if ($loginFoundUser > 0) {
    $loginStrGroup = "";

    $fullname = $row_LoginRS['firstnames'] . ' ' . $row_LoginRS['surname'];
    $user_id = $row_LoginRS['id'];

    //create sessions
 
    $_SESSION['myMM_Username'] = $loginUsername;
    $_SESSION['myMM_Fullname'] = $row_LoginRS['firstnames'] . ' ' . $row_LoginRS['surname'];
    $_SESSION['myMM_Userid'] = $row_LoginRS['id'];
    $_SESSION['security'] = $row_LoginRS['security'];
    $_SESSION['userEmail'] = $row_LoginRS['email'];
    $_SESSION['chgpassword']=$row_LoginRS['req_pass_change'];

    $agency_id = $row_LoginRS['agency_id'];

    $query_rscarmodel11 = "SELECT * FROM agencies where agency_id=?";
    $rscarmodel11 = $conn->prepare($query_rscarmodel11);
    $rscarmodel11->execute(array($agency_id));
    $row_rscarmodel11 = $rscarmodel11->fetch(PDO::FETCH_ASSOC);


    $_SESSION['agencyID'] = $row_rscarmodel11['agency_id'];
    $_SESSION['agency_name'] = $row_rscarmodel11['agency_name'];
    $_SESSION['agency_type'] = $row_rscarmodel11['agency_type'];

    $machineip = getenv("REMOTE_ADDR");


    $query_rscarmodel11 = "INSERT INTO `shift_user_log` (`id`, `user_id`, `username`, `fullname`, `login_time`, `user_ip_address`, `agency_id`) VALUES (NULL, '$user_id', '$loginUsername', '$fullname', CURRENT_TIMESTAMP, '$machineip', '$agency_id');";

    $updateUsers = $conn->prepare("update users_accounts set lastlogindate=CURRENT_TIMESTAMP,lastloginip=? where id=?");
    $updateUsers->execute(array($machineip,$user_id));



    //echo $row_rscarmodel11['facility_name'];
    //exit;

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];
    }

    //  $verifyCaptcha = verifyRecaptcha($recaptcha);

    //  if($verifyCaptcha=="verified"){

    header("Location: " . $MM_redirectLoginSuccess );

    //  }else {
    //   header("Location: " . $MM_redirectLoginRecap);
    //  }
  } else {
    header("Location: " . $MM_redirectLoginFailed);
  }
}else {
  header("Location: " . $MM_redirectLoginFailed);
}
