<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}
  $_SESSION['loggedin'] = FALSE;
  $_SESSION['userEmail'] = NULL;
  $_SESSION['myMM_Userid'] = NULL;
  $_SESSION['myMM_Username']= NULL;
  unset($_SESSION['loggedin']);
  unset($_SESSION['userEmail']);
  unset($_SESSION['myMM_Userid']);
  unset($_SESSION['myMM_Username']);
  session_destroy();
      $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }

