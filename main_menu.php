<div class="iq-sidebar">
            <div class="iq-sidebar-logo d-flex justify-content-between">
               <a href="index.html">
               <!-- <img src="assets/images/logo.png" class="img-fluid" alt=""> -->
               <span><?php echo ucwords(strtolower($_SESSION['agency_name'])); ?></span>
               </a>
               <div class="iq-menu-bt-sidebar">
                     <div class="iq-menu-bt align-self-center">
                        <div class="wrapper-menu">
                           <div class="main-circle"><i class="ri-more-fill"></i></div>
                           <div class="hover-circle"><i class="ri-more-2-fill"></i></div>
                        </div>
                     </div>
                  </div>
            </div>
            <div id="sidebar-scrollbar">
               <nav class="iq-sidebar-menu">
                  <ul class="iq-menu">
                     <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Dashboard</span></li>
                     <li class="active">
                        <a href="index.html" class="iq-waves-effect"><i class="ri-hospital-fill"></i><span>Dashboard</span></a>
                     </li>                     
                     <!-- <li>
                        <a href="dashboard-1.html" class="iq-waves-effect"><i class="ri-home-8-fill"></i><span>Hospital Dashboard 1 </span></a>
                     </li>
                     <li>
                        <a href="dashboard-2.html" class="iq-waves-effect"><i class="ri-briefcase-4-fill"></i><span>Hospital Dashboard 2</span></a>
                     </li>
                     <li>
                        <a href="dashboard-3.html" class="iq-waves-effect"><i class="ri-group-fill"></i><span>Patient Dashboard</span></a>
                     </li>
                     <li>
                        <a href="dashboard-4.html" class="iq-waves-effect"><i class="lab la-mendeley"></i><span>Covid-19
                              Dashboard</span><span class="badge badge-danger">New</span></a>
                     </li> -->
                     <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Apps</span></li>
                     <!-- <li>
                        <a href="javascript:void(0);" class="iq-waves-effect"><i class="ri-mail-open-fill"></i><span>System Admin</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                        <ul class="iq-submenu" style="cursor: pointer;">
                           <li ><a onclick="routeTrigger('listUsers')"><i class="fa fa-user"></i>Users</a></li>
                           <li><a onclick="routeTrigger('listAgency')"><i class="fa fa-building"></i>Agencies</a></li>
                        </ul>
                     </li> -->
                     <li>
                        <a href="javascript:void(0);" class="iq-waves-effect"><i class="ri-mail-open-fill"></i><span>Agency</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                        <ul class="iq-submenu" style="cursor: pointer;">
                           <li ><a onclick="routeTrigger('listEmployees')"><i class="fa fa-user"></i>Employees</a></li>
                           <li><a onclick="routeTrigger('listPatients')"><i class="fa fa-user-pic"></i>Patients</a></li>
                        </ul>
                     </li>
                     
                     <li>
                        <a href="javascript:void(0);" class="iq-waves-effect"><i class="ri-user-3-fill"></i><span>Nursing</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                        <ul class="iq-submenu">
                           <li><a onclick="routeTrigger('mypatients','<?php echo $_SESSION['myMM_Userid'] ?>')"><i class="ri-file-list-fill"></i>My Patients</a></li>
                          
                        </ul>
                     </li>
                     <!-- <li><a href="calendar.html" class="iq-waves-effect"><i class="ri-calendar-event-fill"></i><span>Calendar</span></a></li>
                    
                    <li><a href="chat.html" class="iq-waves-effect"><i class="ri-message-fill"></i><span>Chat</span></a></li> -->
                     
                  </ul>
               </nav>
               <div class="p-3"></div>
            </div>
         </div>