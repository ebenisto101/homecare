/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 * 
 */
const base_url = window.location.origin + '/homecare/';

function current_show() {
    $(".fakeloader").css("visibility", "visible");
}

function current_opac() {
    $(".fakeloader").css("visibility", "hidden");
}

function routeTrigger(act, id, id2) {
    //alert(base_url);
    current_show();
    $("#loading_container").show();
    $.ajax({
        type: "POST",
        url: base_url + 'routes.php',
        data: $('form').serialize() + "&act=" + act + "&id=" + id + "&idd2=" + id2,
        success: function (msg) {
            $("#loading_container").hide();
            $("#kt_app_main").html(msg);
            //current_opac();
            // Materialize.toast('Success! Patient Information Saved.', 4000, 'success');


        }
    });

}


function ShowDetails(item, id, id2 = "test_result_wrapper2") {
    current_show();
    //$("#success_message").show();
    //alert ("ee");
    $.ajax({
        type: "POST",
        url: base_url + 'routes.php',
        data: "&act=" + item + "&id=" + id + "&idd2=" + id2,
        success: function (msg) {
            $("#" + id2).html(msg);
            current_opac();
            //$("#success_message").hide();

        }
    });

}

function saveInfo(act, id, id2) {

    // alert($('form').serialize());
    swal.fire({
        title: "Are you sure?",
        titleText: "You want to save Info",
        icon: 'warning',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes, Proceed!',
        cancelButtonText: "No, cancel!",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then((result) => {
        if (result.value) {


            errormsg(act);
            if ((errors == "<br>") || (!errors)) {

                current_show();
                $("#loading_container").show();
                $.ajax({
                    type: "POST",
                    url: base_url + 'routes.php',
                    data: $('form').serialize() + "&act=" + act + "&id=" + id + "&idd2=" + id2,
                    success: function (msg) {
                        
                        var message = JSON.parse(msg);
                           
                        if (message['code'] == "000" || message['code'] == "ok"){
                            Swal.fire(message['message'],'','success')
                        }else{
                            Swal.fire(message['message'],'','error')
                        }

                        if(act=="SaveAgency" || act=="UpdateAgency" || act=="UserAdd" || act=="SaveUserEdit" || act=="patientAdd" || act=="savePatientAllergy" || act=="saveVital" || act=="patientUpdate" || act=="updatePatientstatus"){
                        
                        //document.getElementById('kt_modal_add_customer').modal('hide');
                       // $('.modal').remove();
                        $('.modal-backdrop').remove();
                        $("#ref").trigger('click');

                        }
                        $("#loading_container").hide();
                        //$("#currentarea").html(msg);
                    }
                });
            } else {
                // if (act == "add_new_user"){
                //     $('#task_details_new_user').modal('show');
                // }
                errors = 'Please Correct the following errors\n --\n' + errors
                swal.fire("Error!", 'Fields marked with red are required', "error");
                errors = null;
                return false;
            }
        }else {
            Swal.fire(
                'Save!',
                'Data not saved',
                'error'
            )
        }

    });
}


function process_ckeditor(act, div, id, data,idd2) {
   // alert($('form').serialize());
    swal.fire({
        title: "Are you sure?",
        text: "You want to save Info",
        icon: 'warning',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes, Proceed!',
        cancelButtonText: "No, cancel!",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then((result) => {
        if (result.value) {


            errormsg(act);
            if ((errors == "<br>") || (!errors)) {
                const editorData = editor.getData();
                current_show();
                $("#loading_container").show();
                $.ajax({
                    type: "POST",
                    url: base_url + 'routes.php',
                    data: $('form').serialize() + "&act=" + act + "&id=" + id + "&data=" + encodeURIComponent(data) + "&div=" + div + "&idd2="+idd2 + "&editor2=" + encodeURIComponent(editorData),
                    success: function (msg) {
                        
                        var message = JSON.parse(msg);
                           
                        if (message['code'] == "000" || message['code'] == "ok"){
                            Swal.fire(message['message'],'','success')
                        }else{
                            Swal.fire(message['message'],'','error')
                        }

                        if(act=="SaveAgency" || act=="UpdateAgency" || act=="UserAdd" || act=="SaveUserEdit" || act=="patientAdd" || act=="savePatientAllergy" || act=="saveVital" || act=="saveNote" || act=="UpdateNote" || act=="savePrescription" || act=="saveTreatment" || act=="saveGlucose" || act=="saveUrine"){
                        
                        //document.getElementById('kt_modal_add_customer').modal('hide');
                        $('.modal-backdrop').remove();
                        //$("#ref").trigger('click');

                        }
                        $("#loading_container").hide();
                       $("#currentarea").html(msg);
                    }
                });
            } else {
                // if (act == "add_new_user"){
                //     $('#task_details_new_user').modal('show');
                // }
                errors = 'Please Correct the following errors\n --\n' + errors
                swal.fire("Error!", 'Fields marked with red are required', "error");
                errors = null;
                return false;
            }
        }else {
            Swal.fire(
                'Save!',
                'Data not saved',
                'error'
            )
        }

    });
}


function getstepDelete(act, id, id2) {
    Swal.fire({
        title: 'Are you sure?',
        text: "Agency will be made Inactive but not deleted",
        icon: 'warning',
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes, Continue!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: base_url + 'routes.php',
                data: $('form').serialize() + "&act=" + act + "&id=" + id + "&idd2=" + id2,
                success: function (msg) {
                    $("#loading_container").hide();
                    if (act == "deleteClient"){
                        $("#loaditems").trigger('click');
                    }if (act == "deleteItem" || act == "test_row_"){
                        $("#LoadProviderItemDetail").trigger('click');
                    }else if(act=="deleteAgency"){
                        $("#ref").trigger('click');
                    }
                    //$("#currentarea").html(msg);
                }
            });
            Swal.fire(
                'Deleted!',
                'Action Performed Successfully.',
                'success'
            )
        } else {
            Swal.fire(
                'Deleted!',
                'Data not Deleted',
                'error'
            )
        }
    })
}



function processInfo(act, id, id2,value) {
	$("#loading_container").show();
    	$.ajax({
		type: "POST",
		url: base_url + 'routes.php',
		data: $('form').serialize() + "&act=" + act + "&id=" + id + "&idd2=" + value,
		success: function (msg) {
			$("#loading_container").hide();
			
			var message = JSON.parse(msg);
           // alert(message);
			//$("#currentarea").html(message['message']);
			if (message['code'] == "001" || message['code'] == "002" || message['code'] == "failed") {
				Swal.fire(message['message'], '', 'error')
			} else if (message['code'] == "000" || message['code'] == "ok") {
				Swal.fire(message['message'], '', 'success')
                $("#CloseUserAdd").trigger('click');
                $('.modal-backdrop').remove();
                $("#" + id2).trigger("click");

                if(act=="change_exp_pass" || act=="change_pass"){
                    $('.modal').remove();
                }

                
			}
		}
	});
}







function errormsg(action) {
    errors = "";
    if (action == "SaveAgency" || action == "updateAgency") {
        if (!document.getElementById('name').value) {
            errors = errors + "Agency Name is Required\n";
            document.getElementById('name').style.border = "solid red 1px";
        } else {
            document.getElementById('name').style.border = "";
        }

        if (!document.getElementById('email').value) {
            errors = errors + "Email is Required\n";
            document.getElementById('email').style.border = "solid red 1px";
        } else {
            document.getElementById('email').style.border = "";
        }

        if (!document.getElementById('address').value) {
            errors = errors + "Address is Required\n";
            document.getElementById('address').style.border = "solid red 1px";
        } else {
            document.getElementById('address').style.border = "";
        }

        if (!document.getElementById('country').value) {
            errors = errors + "Country is Required\n";
            document.getElementById('country').style.border = "solid red 1px";
        } else {
            document.getElementById('country').style.border = "";
        }
        

       

       
    }

    if(action=="UserAdd"){
        if (!document.getElementById('surname').value) {
            errors = errors + "Surname is Required\n";
            document.getElementById('surname').style.border = "solid red 1px";
        } else {
            document.getElementById('surname').style.border = "";
        }

        if (!document.getElementById('user_email').value) {
            errors = errors + "Email is Required\n";
            document.getElementById('user_email').style.border = "solid red 1px";
        } else {
            document.getElementById('user_email').style.border = "";
        }

        if (!document.getElementById('firstnames').value) {
            errors = errors + "Firstnames is Required\n";
            document.getElementById('firstnames').style.border = "solid red 1px";
        } else {
            document.getElementById('firstnames').style.border = "";
        }

        if (!document.getElementById('tel').value) {
            errors = errors + "Telephone No. is Required\n";
            document.getElementById('tel').style.border = "solid red 1px";
        } else {
            document.getElementById('tel').style.border = "";
        }

        if (!document.getElementById('agency').value) {
            errors = errors + "Agency is Required\n";
            document.getElementById('agency').style.border = "solid red 1px";
        } else {
            document.getElementById('agency').style.border = "";
        }

        if (!document.getElementById('user_role').value) {
            errors = errors + "User Role is Required\n";
            document.getElementById('user_role').style.border = "solid red 1px";
        } else {
            document.getElementById('user_role').style.border = "";
        }
    }

}